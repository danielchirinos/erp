<?php 

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use app\models\Mediopago;
use app\models\Pedidoscab;
use yii\widgets\ActiveForm;
use app\models\Cierrecajacab;
use app\models\Cierrecajalin;

class CierreController extends Controller{


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init() {
        date_default_timezone_set("America/Santiago");
        parent::init();
        if (!isset(Yii::$app->session["nombre"])) {
            Yii::$app->session->setFlash("warning","Debe iniciar sessión para acceder a esta página");
            return $this->redirect(['/login']);
        }

        $this->layout = 'admin';
    }
    
    public function actionLista(){

        $cierreCajaCab = Cierrecajacab::find()->orderBy(['fecha_cierre'=>SORT_DESC])->all();
        return $this->render('lista', ['cierreCajaCab' => $cierreCajaCab]);

    }

    public function actionCrear(){

        $cierreCajaCab = new Cierrecajacab;        

		// if ((Yii::$app->request->isAjax) && ($cierreCajaCab->load(Yii::$app->request->post()))) {
			
		// 	Yii::$app->response->format = Response::FORMAT_JSON;
		// 	return ActiveForm::validate($cierreCajaCab);
        // }
        
        // se buscan los pedidos del dia que no esten cerrados
        $pedidosDelDia = Pedidoscab::find()->where(["between", "fecha_creacion", date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59")])->andWhere(["cerrado" => 0])->all();
		if ($_POST) {            
            $cierreCajaCab->id_usuario = $_SESSION["IdUsuario"];
            $cierreCajaCab->total_cierre = $_POST["Cierrecajacab"]["total_cierre"];
            $cierreCajaCab->nombre_persona = $_POST["Cierrecajacab"]["nombre_persona"];
            $cierreCajaCab->efectivo = $_POST["Cierrecajacab"]["efectivo"];
            $cierreCajaCab->debito = $_POST["Cierrecajacab"]["debito"];
            $cierreCajaCab->credito = $_POST["Cierrecajacab"]["credito"];
            $cierreCajaCab->transferencia = $_POST["Cierrecajacab"]["transferencia"];
            $cierreCajaCab->fecha_cierre = $_POST["txt_fecha"];

            if($cierreCajaCab->save()){
                // cunado se realiza el cierre, se recorren todos los pedidos del dia para cerrarlos
                foreach ($pedidosDelDia as $kp => $vp) {
                    $vp->cerrado = 1;
                    $vp->update();
                }
                Yii::$app->session->setFlash("success","Cierre de caja creado con exito");
                return $this->redirect(['lista']);
            }else{
                Yii::$app->session->setFlash("error","Error al crear el cierre de caja");
            }
        }  

        $totalCierre = 0;

        // se recorren todos los pedidos para tener el total y enviarlo a la vista
        if(count($pedidosDelDia) > 0){
            foreach ($pedidosDelDia as $key => $value) {
                $totalCierre += $value->precio_total;
            }
        }
           
        return $this->render('crear', ['model' => $cierreCajaCab, "totalCierre" => $totalCierre]);

    }

    public function actionEditar($id){

        $cierreCajaCab = Cierrecajacab::findOne($id);
        

		// if ((Yii::$app->request->isAjax) && ($cierreCajaCab->load(Yii::$app->request->post()))) {
			
		// 	Yii::$app->response->format = Response::FORMAT_JSON;
		// 	return ActiveForm::validate($cierreCajaCab);
        // }
        

        $cierreCajaLin = Cierrecajalin::find()->where(["id_cierrecajacab" => $id])->all();
        $cierreLinId = [];
        foreach ($cierreCajaLin as $key => $value) {
            $cierreLinId[] = $value["id_mediopago"];
        }
        $medioPago = Mediopago::find()->where(["activo" => 1])->andWhere(["not in", "id", $cierreLinId])->all();
		if ($_POST) {
            if ($cierreCajaCab) {
                
                if(count($cierreCajaLin) > 0){
                    foreach ($cierreCajaLin as $key => $vccl) {
                        $vccl->delete();   
                    }
                }

                $cierreCajaCab->id_usuario = $_SESSION["IdUsuario"];
                $cierreCajaCab->total_cierre = 0;
                $cierreCajaCab->fecha_cierre = $_POST["txt_fecha"];
                if($cierreCajaCab->save()){
                    $total = 0;
                    $medioPago = Mediopago::find()->where(["activo" => 1])->all();
                    foreach ($medioPago as $key => $value) {
                        $cierreCajaLin = new Cierrecajalin;
                        if(isset($_POST["TXT_".strtoupper($value->descripcion)])){
                            $cierreCajaLin->id_cierrecajacab = $cierreCajaCab->id;
                            $cierreCajaLin->id_mediopago = $value->id;
                            $cierreCajaLin->monto_mediopago = $_POST["TXT_".strtoupper($value->descripcion)];
                            $total = $total + (int)$_POST["TXT_".strtoupper($value->descripcion)];
                            $cierreCajaLin->save();
                        }
                    }

                    $cierreCajaCab->total_cierre = $total;
                    $cierreCajaCab->update();
                }

                if ($cierreCajaCab->save()) {
                    Yii::$app->session->setFlash("success","Cierre de caja editado con exito");
                    return $this->redirect(['lista']);
                }else{
                    Yii::$app->session->setFlash("error","Error al editar el cierre de caja");
                    // echo '<pre>';
                    // var_dump($cierreCajaCab->getErrors());
                    // exit;
                }

            }else{
                Yii::$app->session->setFlash("error","Error al buscar el cierre de caja");
            }
        }
        return $this->render('editar', ['model' => $cierreCajaCab, "medioPago" => $medioPago, "cierreCajaLin" => $cierreCajaLin]);

    }


    public function actionEliminar($id){


        $cierreCajaCab = Cierrecajacab::findOne($id);
        $cierreCajaCab->delete();
        Yii::$app->session->setFlash("success","Cierre de caja eliminado con exito");


        return $this->redirect(['lista']);

    }

	
}




