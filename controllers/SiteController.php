<?php

namespace app\controllers;

use Yii;
use stdClass;
use yii\helpers\Url;
use yii\web\Response;
use app\models\Carrito;
use app\models\Comunas;
use app\models\Cupones;
use yii\web\Controller;
use app\models\Clientes;
use yii\data\Pagination;
use app\models\LoginForm;
use app\models\Mediopago;
use app\models\Productos;
use yii\web\UploadedFile;
use app\models\_EnvioForm;
use app\models\Pedidoscab;
use app\models\Pedidoslin;
use app\models\ContactForm;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use app\models\LoginCliente;
use Transbank\Webpay\Webpay;
use yii\helpers\ArrayHelper;
use app\models\_CambiorutForm;
use app\models\Diasbloqueados;
use yii\filters\AccessControl;
use app\models\Pedidoscabpagos;
use app\models\_CambioclaveForm;
use app\models\_ReportarPagoForm;
use app\models\_CambiotelefonoForm;
use app\models\_RecuperarClaveForm;
use Transbank\Webpay\Configuration;
use app\models\_ActualizarClaveForm;
use app\models\_CambiodireccionForm;
use Transbank\Webpay\Options;
use app\models\_ActualizarFechaDespachoForm;
use Transbank\Webpay\WebpayPlus\Transaction;

class SiteController extends Controller{


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function beforeAction($action) {
        
        if ($this->action->id == "error"){
            return $this->render('error');
        }

        if ($action->actionMethod == "actionCallbacktransbank" || $action->actionMethod == "actionConfirmacion" || $action->actionMethod == "actionPagorechazado" || $action->actionMethod == "actionActualizarcarro" || $action->actionMethod == "actionAgregarcarro" || $action->actionMethod == "actionVaciarcarro" || $action->actionMethod == "actionEliminarproductocarrito" || $action->actionMethod == "actionBuscarcomuna" || $action->actionMethod == "actionBuscarproductoscarro" || $action->actionMethod == "actionError" || $action->actionMethod == "actionValidarcupon" || $action->actionMethod == "actionPregutarrutcliente" || $action->actionMethod == "actionActualizarturcliente" || $action->actionMethod == "confirmacionpedido" || $action->actionMethod == "actionCambiorut" ) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    //inicio
    public function actionIndex(){
        return $this->redirect('login');
    }


    //envio de email
    public function EnviarEmail($email, $asunto, $correo){

        $envio = Yii::$app->mailer->compose()
        ->setTo($email)
        ->setFrom(["info@mbgreen.cl" => "Mb Green"])
        ->setSubject(utf8_decode($asunto))
        ->setHtmlBody($correo)
        ->setCharset('iso-8859-1')
        // ->setCharset('UTF-8')
        ->send();
        
        return $envio;

    }

    public function Transbank($monto, $numeroOrden){

        $amount = round($monto);
        // Identificador que será retornado en el callback de resultado:
        $sessionId = "768542694mb";
        // Identificador único de orden de compra:
        $buyOrder = strval($numeroOrden);
        // "https://callback/resultado/de/transaccion";

        $returnUrl = Url::to(['site/callbacktransbank'],true);

        // para desarrollo se comentan las options
        // $transaction = new Transaction();


        $commerceCode = "597034641889";
        $apiKeySecret = "d94848e0fbdfa3bc0854f9eea5392f22";

        $options = Options::forProduction($commerceCode, $apiKeySecret);
        $transaction = new Transaction($options);

        $response = $transaction->create($buyOrder, $sessionId, $amount, $returnUrl);

        $formAction = $response->url;
        $tokenWs = $response->token;

        return [$formAction, $tokenWs];
    }

}

