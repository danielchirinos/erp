<?php 

namespace app\controllers;

use Yii;
use app\models\Posts;
use yii\web\Response;
use yii\web\Controller;
use app\models\Clientes;
use app\models\Pedidoscab;
use yii\widgets\ActiveForm;

class ClientesController extends Controller{


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init() {
        parent::init();
        if (!isset(Yii::$app->session["nombre"])) {
            Yii::$app->session->setFlash("warning","Debe iniciar sessión para acceder a esta página");
            return $this->redirect(['/login']);
        }

        $this->layout = 'admin';
    }
    
    public function actionLista(){

        $clientes = Clientes::find()->orderBy(['nombre'=>SORT_DESC])->all();
        return $this->render('lista', ['clientes' => $clientes]);

    }

    public function actionCrear(){

        $cliente = new Clientes;
        

		if ((Yii::$app->request->isAjax) && ($cliente->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($cliente);
        }
        

        
		if ($_POST) {
            $cliente->rut = $_POST["Clientes"]["rut"];
            $cliente->nombre = $_POST["Clientes"]["nombre"];
            $cliente->apellido = $_POST["Clientes"]["apellido"];
            $cliente->email = $_POST["Clientes"]["email"];
            $cliente->direccion = $_POST["Clientes"]["direccion"];
            $cliente->telefono = $_POST["Clientes"]["telefono"];
            $cliente->giro = $_POST["Clientes"]["giro"];
            $cliente->comuna = $_POST["Clientes"]["comuna"];
            $cliente->ciudad = $_POST["Clientes"]["ciudad"];

            if ($cliente->save()) {
                Yii::$app->session->setFlash("success","Cliente {$_POST['Clientes']['nombre']} agregado con exito");
                return $this->redirect(['lista']);
            }else{
                Yii::$app->session->setFlash("error","Error al agregar el cliente");
                // echo '<pre>';
                // var_dump($cliente->getErrors());
                // exit;
            }


        }
        return $this->render('crear', ['model' => $cliente]);

    }

    public function actionEditar($id){

        $cliente = Clientes::findOne($id);
        

		if ((Yii::$app->request->isAjax) && ($cliente->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($cliente);
        }
        

        
		if ($_POST) {
            if ($cliente) {
                $cliente->rut = $_POST["Clientes"]["rut"];
                $cliente->nombre = $_POST["Clientes"]["nombre"];
                $cliente->apellido = $_POST["Clientes"]["apellido"];
                $cliente->email = $_POST["Clientes"]["email"];
                $cliente->direccion = $_POST["Clientes"]["direccion"];
                $cliente->telefono = $_POST["Clientes"]["telefono"];
                $cliente->giro = $_POST["Clientes"]["giro"];
                $cliente->comuna = $_POST["Clientes"]["comuna"];
                $cliente->ciudad = $_POST["Clientes"]["ciudad"];

                if ($cliente->save()) {
                    Yii::$app->session->setFlash("success","Cliente {$_POST['Clientes']['nombre']} editado con exito");
                    return $this->redirect(['lista']);
                }else{
                    Yii::$app->session->setFlash("error","Error al editar el cliente");
                    // echo '<pre>';
                    // var_dump($cliente->getErrors());
                    // exit;
                }

            }else{
                Yii::$app->session->setFlash("error","Error al buscar el cliente");
            }
        }
        return $this->render('editar', ['model' => $cliente]);

    }

    public function actionCambioclave($id){

        $cliente = Clientes::findOne($id);
        
		if ((Yii::$app->request->isAjax) && ($cliente->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($cliente);
        }
        
		if ($_POST) {
            if ($cliente) {
                $cliente->clave = md5($_POST["Clientes"]["clave"]);
                $cliente->rut = md5($_POST["Clientes"]["rut"]);

                if ($cliente->save()) {
                    Yii::$app->session->setFlash("success","Clave cambiada para el cliente {$cliente->nombre}");
                    return $this->redirect(['lista']);
                }else{
                    Yii::$app->session->setFlash("error","Error al cambiar la clave");
                }
                
            }else{
                Yii::$app->session->setFlash("error","Error al buscar el cliente");
            }
        }

        return $this->render('cambioclave', ['model' => $cliente]);

    }

    public function actionEliminar($id){

        $pedidos = Pedidoscab::find()->where(["id_cliente" => $id])->all();

        if (count($pedidos) > 0) {
            Yii::$app->session->setFlash("warning","El Cliente no se puede eliminar porque tiene pedidos asociados");
        }else{
            $clientes = Clientes::findOne($id);
            $clientes->delete();
            Yii::$app->session->setFlash("success","Cliente eliminado con exito");
        }

        return $this->redirect(['lista']);

    }

    public function actionActivar($id){

        $cliente = Clientes::findOne($id);

        $cliente->activo = 1;
        if ($cliente->save()) {
            Yii::$app->session->setFlash("success","Cliente {$cliente->nombre} activado ");
        }else{
            Yii::$app->session->setFlash("error","Ha ocurrido un error activar el Cliente {$cliente->nombre} ");

        }
        
        return $this->redirect(['lista']);

    }

	
}




