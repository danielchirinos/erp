<?php 

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use app\models\Usuarios;
use app\models\Tipousuario;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

class UsuariosController extends Controller{


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init() {
        parent::init();
        if (!isset(Yii::$app->session["nombre"])) {
            Yii::$app->session->setFlash("warning","Debe iniciar sessión para acceder a esta página");
            return $this->redirect(['/login']);
        }

        $this->layout = 'admin';
    }
    
    public function actionLista(){

        $usuarios = Usuarios::find()->orderBy(['usuario' => SORT_DESC])->all();
        return $this->render('lista', ['usuarios' => $usuarios] );

    }

    public function actionCrear(){

        $usuario = new Usuarios();
        

		if ((Yii::$app->request->isAjax) && ($usuario->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($usuario);
        }
        

        
		if ($_POST) {
            if ($usuario) {
                $usuario->usuario = $_POST["Usuarios"]["usuario"];
                $usuario->clave = md5($_POST["Usuarios"]["clave"]);
                $usuario->id_tipo_usuario = $_POST["Usuarios"]["id_tipo_usuario"];
                $usuario->activo = $_POST["Usuarios"]["activo"];

                if ($usuario->save()) {
                    Yii::$app->session->setFlash("success","Usuario {$_POST['Usuarios']['usuario']} creado con exito");
                    return $this->redirect(['lista']);
                }else{
                    Yii::$app->session->setFlash("error","Error al crear el Usuario");
                    // echo '<pre>';
                    // var_dump($cliente->getErrors());
                    // exit;
                }

            }else{
                Yii::$app->session->setFlash("error","Error al buscar el Usuario");
            }
        }
        $tipoUsuario = ArrayHelper::map(Tipousuario::find()->all(), 'id', 'tipo');
        return $this->render('crear', ['model' => $usuario, 'tipoUsuario' => $tipoUsuario]);

    }

    public function actionEditar($id){

        $usuario = Usuarios::findOne($id);
        

		if ((Yii::$app->request->isAjax) && ($usuario->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($usuario);
        }
        

        
		if ($_POST) {
            if ($usuario) {
                $usuario->usuario = $_POST["Usuarios"]["usuario"];
                $usuario->clave = md5($_POST["password"]);
                $usuario->id_tipo_usuario = $_POST["Usuarios"]["id_tipo_usuario"];
                $usuario->activo = $_POST["Usuarios"]["activo"];

                if ($usuario->save()) {
                    Yii::$app->session->setFlash("success","Usuario {$_POST['Usuarios']['usuario']} editado con exito");
                    return $this->redirect(['lista']);
                }else{
                    Yii::$app->session->setFlash("error","Error al editar el Usuario");
                    // echo '<pre>';
                    // var_dump($cliente->getErrors());
                    // exit;
                }

            }else{
                Yii::$app->session->setFlash("error","Error al buscar el Usuario");
            }
        }

        $tipoUsuario = ArrayHelper::map(Tipousuario::find()->all(), 'id', 'tipo');
        return $this->render('editar', ['model' => $usuario, 'tipoUsuario' => $tipoUsuario]);

    }


    public function actionCambioclave($id){

        $cliente = Usuarios::findOne($id);
        
		if ((Yii::$app->request->isAjax) && ($cliente->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($cliente);
        }
        
		if ($_POST) {
            if ($cliente) {
                $cliente->clave = md5($_POST["Clientes"]["clave"]);
                $cliente->rut = md5($_POST["Clientes"]["rut"]);

                if ($cliente->save()) {
                    Yii::$app->session->setFlash("success","Clave cambiada para el cliente {$cliente->nombre}");
                    return $this->redirect(['lista']);
                }else{
                    Yii::$app->session->setFlash("error","Error al cambiar la clave");
                }
                
            }else{
                Yii::$app->session->setFlash("error","Error al buscar el cliente");
            }
        }

        return $this->render('cambioclave', ['model' => $cliente]);

    }

    public function actionBorrar($id){
        $session = Yii::$app->session;
        $IdUsuario = $session['IdUsuario']; 
        $usuarioAdmin = Usuarios::findOne($IdUsuario);


        // Se le colocó ID 1 (ADMINISTRADOR)

        if($usuarioAdmin->id_tipo_usuario == 1){
            $usuarios = Usuarios::findOne($id);
            $usuarios->delete();
            Yii::$app->session->setFlash("success","Usuario eliminado con exito");
        }else{
            Yii::$app->session->setFlash("warning","No puedes borrar un Usuario si no eres administrador");
        }

        return $this->redirect(['lista']);

    }

    public function actionActivar($id){

        $cliente = Usuarios::findOne($id);

        $cliente->activo = 1;
        if ($cliente->save()) {
            Yii::$app->session->setFlash("success","Cliente {$cliente->nombre} activado ");
        }else{
            Yii::$app->session->setFlash("error","Ha ocurrido un error activar el Cliente {$cliente->nombre} ");

        }
        
        return $this->redirect(['lista']);

    }

	
}




