<?php 

namespace app\controllers;

use Yii;
use app\models\Posts;
use yii\web\Response;
use yii\web\Controller;
use app\models\Pedidoscab;
use yii\widgets\ActiveForm;
use app\models\_ContactoForm;

class AdminController extends Controller{


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init() {
        parent::init();

        $this->layout = 'admin';
    }

	public function actionIndex(){
        
        $pedidosPagados = Pedidoscab::find()->where(["id_estadopedido" => 1])->all();
        $pedidosRechazados = Pedidoscab::find()->where(["id_estadopedido" => 3])->all();
        $pedidosGuardados = Pedidoscab::find()->where(["id_estadopedido" => 4])->all();
        $pedidosEntregados = Pedidoscab::find()->where(["id_estadopedido" => 2])->all();
        
        $pedidosPagados = count($pedidosPagados);
        $pedidosRechazados = count($pedidosRechazados);
        $pedidosGuardados = count($pedidosGuardados);
        $pedidosEntregados = count($pedidosEntregados);
		return $this->render('index', [
            "pedidosPagados" => $pedidosPagados,
            "pedidosRechazados" => $pedidosRechazados,
            "pedidosGuardados" => $pedidosGuardados,
            "pedidosEntregados" => $pedidosEntregados
        ]);
	}

    public function actionBuscartabla(){
        $pedidos = Pedidoscab::find()->all();

        $data = [];

        $tabla = "";
        $tabla .= '<table class="table table-striped" id="table">';
        $tabla .= '<thead class="table-dark">';
        $tabla .= '<tr>';
        $tabla .= '<th scope="col">ID</th>';
        $tabla .= '<th scope="col">Cliente</th>';
        $tabla .= '<th scope="col">Tipo Pedido</th>';
        $tabla .= '<th scope="col">Pago</th>';
        $tabla .= '</tr>';
        $tabla .= '</thead>';
        $tabla .= '<tbodyid="tbody_table">';
        
        foreach($pedidos as $key => $pedido){
            $tabla .= '<tr>';
            $tabla .= '<td>'.$pedido->id.'</td>';
            $tabla .= '<td>'.$pedido->cliente->nombre . ' ' . $pedido->cliente->apellido .'</td>';
            $tipo_pedido = ($pedido->tipo_pedido === 0 ? "WEB" : "MANUAL");
            $tabla .= '<td>'.$tipo_pedido.'</td>';
            // COLOCAR AQUÍ EL MEDIO DE PAGO DEL PEDIDOCAB
            $tabla .= '<td>WEBPAY</td>';
            $tabla .= '</tr>';
        }

        $tabla .= '</tbody>';
        $tabla .= '</table>';

        $data['data'] = $tabla;

        return json_encode($data);
    }

	
}




