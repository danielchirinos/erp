<?php 

namespace app\controllers;

use Yii;
use app\models\Posts;
use yii\web\Response;
use yii\web\Controller;
use app\models\Clientes;
use yii\data\Pagination;
use app\models\Productos;
use yii\web\UploadedFile;
use app\models\Categorias;
use app\models\Pedidoscab;
use app\models\Pedidoslin;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\_ContactoForm;
use app\models\Estadospedidos;
use app\models\Pedidoscabpagos;
use app\models\ProductosVendidos;
use app\models\Usuarios;

class PedidosController extends Controller{


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action) {
        if ($action->actionMethod == "actionCambiarestadopedido" ) { //|| $action->actionMethod == "confirmacionpedido"
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function init() {
        parent::init();
        date_default_timezone_set('America/Santiago');
        if (!isset(Yii::$app->session["nombre"])) {
            Yii::$app->session->setFlash("warning","Debe iniciar sessión para acceder a esta página");
            return $this->redirect(['/login']);
        }

        $this->layout = 'admin';
    }

    
    public function actionLista(){
        $fechaInicio = date("Y-m-d H:i:s", strtotime("-60 days"));
        $fechaFin = date("Y-m-d H:i:s", strtotime("+14 days"));
        $pedidos = Pedidoscab::find()->andWhere(['between', 'fecha_creacion', $fechaInicio, $fechaFin ])->all();
        $estadoPedidos = Estadospedidos::find()->all();

        $vendedores = ArrayHelper::map(Usuarios::find()->where(['id_tipo_usuario' => 3])->all(), 'id', 'usuario');

        $clientes = (new \yii\db\Query())->select('id, nombre, apellido')->from('clientes')->all();

        

        // $pedidos = Pedidoscab::find()->all();

        $campo = isset($_POST["txt_buscar"]) ? $_POST["txt_buscar"] : "";
        return $this->render('lista',
        [
            'pedidos' => $pedidos,
            'estadoPedidos' => $estadoPedidos,
            'vendedores' => $vendedores,
            'clientes' => $clientes
        ]);

    }

    public function actionDetalle(){

        $pedidoscab = Pedidoscab::find()->where(["id" => $_GET["id"]])->all();
        $pedidoslin = Pedidoslin::find()->where(["id_pedidoscab" => $_GET["id"]])->all();

        return $this->render('detalle', ['pedidoscab' => $pedidoscab, "pedidoslin" => $pedidoslin]);

    }

    public function actionCompra(){
        $fechaInicio = date("Y-m-d H:i:s", strtotime("-60 days"));
        $fechaFin = date("Y-m-d H:i:s", strtotime("+14 days"));
        $pedidos = Pedidoscab::find()->andWhere(['between', 'fecha_despacho', $fechaInicio, $fechaFin ])->all();
        // $pedidos = Pedidoscab::find()->all();

        $campo = isset($_POST["txt_buscar"]) ? $_POST["txt_buscar"] : "";
        return $this->render('compra', ['pedidos' => $pedidos]);

    }

    public function actionCambiarestadopedido(){

        $pedidoscab = Pedidoscab::find()->where(["id" => $_POST["id"]])->one();

        $pedidocabPago = Pedidoscabpagos::find()->where(["id_pedidoscab" => $_POST["id"]])->one();

        // si el pedido esta en estado guardado, pasa a pagado
        if ($pedidoscab->id_estadopedido == 4) {       
            $pedidoscab->id_estadopedido = 1;
        }else{
            // si el pedido esta en estado pagado, pasa a entregado 
            $pedidoscab->id_estadopedido = 2;
        }
        
        $pedidoscab->update();
        return true;

    }

    public function actionEnviarcorreo($id){

        // Yii::$app->cache->flush();
        $pedidoCabConfirmado = PedidosCab::find()->where(['id'=>$id])->one();

        $pedidoLinConfirmado = Pedidoslin::find()->where(['id_pedidoscab'=>$pedidoCabConfirmado->id])->all();

        $datosCliente = Clientes::find()->where(['id'=>$pedidoCabConfirmado->id_cliente])->one();

        //generacion de correo de confirmacion

        $correoClienteEmail = $pedidoCabConfirmado->cliente->email;
        $direccionDespacho = $pedidoCabConfirmado->cliente->direccion_despacho;
        $direccionDespacho = $direccionDespacho. " , nro dpto/casa: ". $pedidoCabConfirmado->cliente->nro_casa;
        $costoTotalEmail = $pedidoCabConfirmado->precio_total;
        $costoEnvioEmail = $pedidoCabConfirmado->comuna->precio;
        $clienteEmail = $pedidoCabConfirmado->cliente->nombre ." ".$pedidoCabConfirmado->cliente->apellido; //nombr total del cliente
        $numeroPedidoEmail = $pedidoCabConfirmado->id; //numero del pedido
        $fechaDespachoEmail = $pedidoCabConfirmado->fecha_despacho; //fecha del pedido
        $direccionEmail = $pedidoCabConfirmado->cliente->direccion_despacho; //direccion del pedido
        $direccionEmail = $direccionEmail. " , nro dpto/casa: ". $pedidoCabConfirmado->cliente->nro_casa;
        $telefonoEmail = $pedidoCabConfirmado->cliente->telefono; //telefono cliente
        $comunaEmail = $pedidoCabConfirmado->comuna->nombre; //comuna del cliente

        // si el pedido tiene un cupon
        if ($pedidoCabConfirmado->id_cupon != null) {
            //se revisa si el descuento es monto o porcentaje
            $simboloPorcentaje = ($pedidoCabConfirmado->cupon->tipo_cupon == 0) ? "" : "%";
            //se arma la cadena para adjuntarla al email
            $cuponEmail = "Cupón usado: (". $pedidoCabConfirmado->cupon->cupon.") ". $pedidoCabConfirmado->cupon->descuento.$simboloPorcentaje."\r\n";

            //si el cupon es por monto se hacen los calculos para el monto final
            if ($pedidoCabConfirmado->cupon->tipo_cupon == 0) {
                $montoConDescuento = (floatval($costoTotalEmail) + floatval($costoEnvioEmail)) - floatval($pedidoCabConfirmado->cupon->descuento);
            }else{
                //  si es en procentaje
                $porcentajeDescuento = $pedidoCabConfirmado->cupon->descuento * 0.01;
                $montoConDescuento = (floatval($costoTotalEmail) + floatval($costoEnvioEmail)) - ((floatval($costoTotalEmail) + floatval($costoEnvioEmail)) * $porcentajeDescuento);
            }
        }else{
            $montoConDescuento = floatval($costoTotalEmail) + floatval($costoEnvioEmail);
            $cuponEmail = "";

            // si el pedido tiene comuna gratis
            if ($pedidoCabConfirmado->comuna->dias_envio_gratis != null) {
                $diasGratis = explode(",", $pedidoCabConfirmado->comuna->dias_envio_gratis);

                $fechaDespachoEmailSplit = explode("-", $pedidoCabConfirmado->fecha_despacho);
                $fechaDespachoElegida = date("N", mktime(00, 00, 00, $fechaDespachoEmailSplit[1], $fechaDespachoEmailSplit[2], $fechaDespachoEmailSplit[0]));

                foreach ($diasGratis as $key => $value) {
                    if ($fechaDespachoElegida == $value) {
                        $costoEnvioEmail = "Despacho gratis";
                        $montoConDescuento = floatval($montoConDescuento) - floatval($pedidoCabConfirmado->comuna->precio);
                    }
                }
            }
        }

        $detalleEmail = [];

        //se recorre para sacar todo el detalle de la compra
        foreach ($pedidoLinConfirmado as $key => $value) {
            
            $detalleEmail[$key]["descripcion"] = $value->producto->descripcion;
            $detalleEmail[$key]["cantidad"] = $value->cantidad;
            $detalleEmail[$key]["precio"] = $value->precio;
        }
        sort($detalleEmail);

        $productos = "<table style='border-collapse: collapse; border: 1px solid black'>";
        $productos .= "<tr style='border: 1px solid black'>";
        $productos .= "<th style='border: 1px solid black'><b>Producto</b></th>";
        $productos .= "<th style='border: 1px solid black'><b>Cantidad</b></th>";
        $productos .= "<th style='border: 1px solid black'><b>Precio Total</b></th>";
        $productos .= "</tr>";

        foreach ($detalleEmail as $key => $value) {
            $productos .= "<tr style='border: 1px solid black'>";
                $productos .= "<td style='border: 1px solid black'>".$value["descripcion"]."</td>";
                $productos .= "<td style='border: 1px solid black'>".$value["cantidad"]."</td>";
                $productos .= "<td style='border: 1px solid black'>".$value["precio"]." $</td>";
            $productos .= "</tr>";
        }

        $productos .= "</table>";

        $costoEnvioEmail = ($costoEnvioEmail == "Despacho gratis") ? "Despacho gratis" : $costoEnvioEmail."$";

        $emailCliente = $this->EnviarEmail($correoClienteEmail, 'Detalle del pedido MB Green', '
            <b>Hemos Confirmado tu pedido con exito</b><br><br>
            A continuaci&oacute;n te detallamos tu pedido:
            '.$productos.'
            <br>
            Direccion de despacho: '.htmlentities($direccionDespacho). ', '. htmlentities($comunaEmail). ' <br>
            Fecha de despacho: '.$fechaDespachoEmail.'<br>
            Subtotal del pedido: '.$costoTotalEmail.' $<br>
            Costo de envio: '.$costoEnvioEmail.'<br>
            '.htmlentities($cuponEmail). ''.(($cuponEmail!="") ? "$<br>" : "").' 
            Total del pedido: '.number_format(floatval($montoConDescuento),2,".","").' $<br>

            <br><br>
            Gracias por preferirnos<br><br>
            <img src="https://mbgreen.cl/web/images/mbgreen_logo.png" width="50">
        ' );
                        
        //envio a admin
        $emailAdmin = $this->EnviarEmail("mbgreenspa@gmail.com", 'Confirmación de pedido', '
            El cliente <b>'.htmlentities($clienteEmail).'</b> , nro telf: <b>'.$telefonoEmail.'</b> , direcci&oacute;n: <b>'.htmlentities($direccionEmail). ', '. htmlentities($comunaEmail) . '</b> , fecha de despacho <b>'. $fechaDespachoEmail.'</b>, ha realizado el pedido <b>'.$numeroPedidoEmail.'</b> con exito</b><br><br>
            Detalle del pedido:
            '.$productos.'
            <br>
            Subtotal del pedido: '.$costoTotalEmail.' $<br>
            Costo de envio: '.$costoEnvioEmail.' $<br>
            '.htmlentities($cuponEmail). ''.(($cuponEmail!="") ? "$<br>" : "").' 
            Total del pedido: '.number_format(floatval($montoConDescuento),2,".","").' $<br>

            <img src="https://mbgreen.cl/web/images/mbgreen_logo.png" width="50">
        ' );

        if ($emailCliente && $emailAdmin) {
            Yii::$app->session->setFlash("success","Envio de email exitoso");
        }else{
            Yii::$app->session->setFlash("error","Ocurrio un error al enviar algun email");

        }

        $pedidocabPago = Pedidoscabpagos::find()->where(["id_pedidoscab" => $id])->one();

        if ($pedidocabPago->id_medio_pago == 3 || $pedidocabPago->id_medio_pago == 4) { 
            return $this->redirect(['otrospagos']);
        }

        return $this->redirect(['lista']);
    }

    public function actionOtrospagos(){

        $db = Yii::$app->db;

        $sql = $db->createCommand("select pcab.id, pcabpagos.orden_compra, mp.descripcion as medio_pago, c.nombre as cliente, pcab.direccion_despacho, pcab.cantidad_productos, pcab.precio_total, pcab.fecha_despacho, pcab.id_estadopedido, pcabpagos.url_redireccion as imagen, e.estado
        from pedidoscab pcab 
        inner join pedidoscabpagos pcabpagos on pcab.id = pcabpagos.id_pedidoscab
        inner join clientes c on c.id = pcab.id_cliente
        inner join comunas co on co.id = pcab.id_comuna
        inner join estadospedidos e on e.id = pcab.id_estadopedido
        inner join mediopago mp on mp.id = pcabpagos.id_medio_pago
        where pcabpagos.id_medio_pago in (3,4)");

        $otrosMedios = $sql->queryAll();

        // $campo = isset($_POST["txt_buscar"]) ? $_POST["txt_buscar"] : "";

        return $this->render('otrospagos', ['otrosMedios' => $otrosMedios]);

    }

    public function EnviarEmail($email, $asunto, $correo){
        

        // ->attach(Yii::getAlias('@webroot/images/thum/producto20200821091110.jpg'))
        // ->setCharset('UTF-8')
        $envio = Yii::$app->mailer->compose()
        ->setTo($email)
        ->setFrom(["info@mbgreen.cl" => "Mb Green"])
        ->setSubject(utf8_decode($asunto))
        ->setHtmlBody($correo)
        ->setCharset('iso-8859-1')
        ->send();
        
        return $envio;

    }

    public function actionVenta(){
        if($_POST){
            if(isset($_POST['sku'])){
                $sku = $_POST['sku'];
                $productos = Productos::find()->where(['sku' => $sku])->one();

                return $this->asJson($productos);
            }

            return false;
        }
    }

    public function actionVender(){
        if($_POST){
            if(isset($_POST['ArrayCompleto'])){
                $cliente = Clientes::find()->where(['rut' => $_POST['rut']])->one();
                if(!$cliente){
                    if($_POST['nombre'] == ""){
                        return "nombre";
                    }else if(!isset($_POST['nombre'])){
                        return "nombre";
                    }

                    if( $_POST['apellido'] == ""){
                        return "apellido";
                    }else if(!isset($_POST['apellido'])){
                        return "apellido";
                    }

                    $cliente = new Clientes();
                    $cliente->rut = $_POST['rut'];
                    $cliente->nombre = $_POST['nombre'];
                    $cliente->apellido = $_POST['apellido'];
                    $cliente->activo = 1;
                    if($cliente->save()){
                    }
                }

                $pedidos = new Pedidoscab();
                $pedidos->id_cliente = $cliente->id;
                $pedidos->id_estadopedido = 1;
                $session = Yii::$app->session;
                $pedidos->id_usuario = $session['IdUsuario'];

                $cantidadProductos = 0;

                foreach($_POST['ArrayCompleto'] as $ArrayCompleto){
                    $cantidadProductos += intval($ArrayCompleto['cantidadProductos']);

                    $productoBuscar = Productos::find()->where(['sku' => $ArrayCompleto['sku']])->one();

                    if($productoBuscar->cantidad < $ArrayCompleto['cantidadProductos']){
                        Yii::$app->session->setFlash("warning","La cantidad de productos a vender es mayor a la que se encuentra en stock, producto: " . $productoBuscar->descripcion . " stock: " . $productoBuscar->cantidad);
                        return $this->redirect(['/pedidos/compra']);
                    }
                }
                
                $pedidos->cantidad_productos = $cantidadProductos;
                $pedidos->precio_total = intval($_POST['total']);
                $pedidos->fecha_creacion = date("Y-m-d H:i:s");
                $pedidos->tipo_pedido = 0;

                if($pedidos->save()){

                    foreach($_POST['ArrayCompleto'] as $ac){
                        $productoBuscar = Productos::find()->where(['sku' => $ac['sku']])->one();
                        $pedidosLin = new Pedidoslin();
                        $pedidosLin->id_pedidoscab = $pedidos->id;
                        $pedidosLin->id_producto = $productoBuscar->id;
                        $pedidosLin->cantidad = $ac['cantidadProductos'];
                        $pedidosLin->precio = $ac['precioUnitario'];

                        if($pedidosLin->save()){
                            $productoBuscar->cantidad -= $pedidosLin->cantidad;
                            if($productoBuscar->save()){
                                $buscarVentaProducto = ProductosVendidos::find()->where(['id_producto' => $productoBuscar->id])->one();
                                if($buscarVentaProducto){
                                    $buscarVentaProducto->cantidad += $pedidosLin->cantidad;
                                    $buscarVentaProducto->save();
                                }else{
                                    $ventaProducto = new ProductosVendidos();
                                    $ventaProducto->id_producto = $pedidosLin->id_producto;
                                    $ventaProducto->cantidad = $pedidosLin->cantidad;
                                    $ventaProducto->save();
                                }
                            }else{
                                echo "<pre>";
                                var_dump($productoBuscar->getErrors());
                                exit;
                            }
                        }else{
                            echo "<pre>";
                            var_dump($pedidosLin->getErrors());
                            exit;
                        }
                    }
                    return "success";
                }
            }

        }
    }

    public function actionBuscar(){
        if($_POST){
            $db = Yii::$app->db;
            $filtros = "";
            
            $estado = "p.fecha_creacion is not null";

            //buscar por id o nro_viaje

            if($_POST['nro_pedido'] != ""){
                $filtros .= " AND  p.id = " . $_POST['nro_pedido'];
            }
    
            //buscar por estado
            if ($_POST['estado'] != 0) {
                $filtros .= " AND  p.id_estadopedido = " . intval($_POST['estado']);
            }

            //buscar por cliente
            if ($_POST['cliente'] != 0) {
                $filtros .= " AND  p.id_cliente = " . $_POST['cliente'];
            }

            //buscar por vendedor
            if ($_POST['vendedor'] != 0) {
                $filtros .= " AND  p.id_usuario = " . $_POST['vendedor'];
            }

            //buscar por tipo
            if ($_POST['tipo'] != 0) {
                $filtros .= " AND  p.tipo_pedido = " . intval($_POST['tipo']) - 1;
            }
    
    

            $sql = "SELECT p.id, p.fecha_creacion, p.fecha_despacho, p.id_estadopedido, p.precio_total, u.usuario, c.nombre, c.apellido, c.telefono, p.tipo_pedido, ep.estado, p.direccion_despacho, p.cantidad_productos FROM pedidoscab p
                INNER JOIN clientes c ON c.id = p.id_cliente
                left join estadospedidos ep on ep.id = p.id_estadopedido
                left join usuarios u on u.id = p.id_usuario
                WHERE
                " . $estado . "
                " . $filtros . "
            
            ORDER BY
            p.id DESC";
    
            $sql = $db->createCommand($sql);
    
            $datos = $sql->queryAll();

            $tabla = "";
           
            foreach($datos as $value){
                $tabla .= "<tr>";
                $tabla .= "<td><a href='".Yii::getAlias('@web')."/pedidos/pdf/".$value['id']."'><i style='color: red;' class='fas fa-file-pdf'></i></a></td>";
                $tabla .= "<td>".$value['id']."</td>";
                $tabla .= "<td>".$value['fecha_creacion']."</td>";
                $condicion = $value['fecha_despacho'] != null ? $value['fecha_despacho'] : 'Sin Fecha de Despacho';
                $tabla .= "<td>".$condicion."</td>";
                $tabla .= "<td>";
                $estado = "";
                $value['id_estadopedido'] = intval($value['id_estadopedido']);
                switch ($value['id_estadopedido']) {
                    case 1://pagado
                        $estado = "primary";
                        break;
                    case 2://entregado
                        $estado = "success";
                        break;
                    case 3://rechazados
                        $estado = "warning";
                        break;
                    case 4://confirmados
                        $estado = "info";
                        break;
                    
                }
                if ($value['id_estadopedido'] == 1 || $value['id_estadopedido'] == 4) {
                    $tabla .= '<span class="badge badge-'.$estado.'" style="cursor:pointer" onclick="cambiarEstado('.$value["id"].')">'.$value["estado"].'</span>';
                }else{
                    $tabla .= '<span class="badge badge-'.$estado.'" style="cursor:pointer">'.$value["estado"].'</span>';
                }
                $tabla .= "</td>";
                $tabla .= "<td>$".number_format($value['precio_total'], 0, '', '.')."</td>";
                $tabla .= "<td>".$value['usuario']."</td>";
                $tabla .= "<td>".$value['nombre'] . ' ' . $value['apellido']."</td>";
                $condicion = $value['telefono'] ? $value['telefono'] : 'Sin Teléfono';
                $tabla .= "<td>".$condicion."</td>";
                $condicion = $value['tipo_pedido'] == 1 ? 'MANUAL' : 'WEB';
                $tabla .= "<td>".$condicion."</td>";
                $tabla .= "<td>";
                $tabla .= "<span data-toggle='tooltip' title='".$value['direccion_despacho']."'>";
                $tabla .= strlen($value['direccion_despacho'])  > 15 ? substr($value['direccion_despacho'], 0, 12)."..." : $value['direccion_despacho'];
                $tabla .= "</span>";
                $tabla .= "</td>";
                $tabla .= "<td>".$value['cantidad_productos']."</td>";
                /*$tabla .= "<td class='text-center'>";
                $web = Yii::getAlias('@web');
                $tabla .= "<a class='btn btn-secondary btn-sm' href='".$web.'/pedidos/detalle/'.$value['id']."'><i class='fas fa-bars'></i></a>";
                $tabla .= "</td>"; */
                $tabla .= "</tr>";
            }

            return $tabla;
        }
    }

    public function actionPdf($id){
        // Buscar datos
        $pedidos = Pedidoscab::find()->where(['id' => $id])->one();
        $productosPedidos = Pedidoslin::find()->where(['id_pedidoscab' => $pedidos->id])->all();
        $factura_numero = $id;
        $rut_cliente = $pedidos->cliente->rut;
        $html = $this->renderPartial('pdf', [
            'factura_numero' => $factura_numero,
            'rut_cliente' => $rut_cliente,
            'nombre_cliente' => $pedidos->cliente->nombre . ' ' . $pedidos->cliente->apellido,
            'vendedor' => $pedidos->usuario->usuario,
            'productos_vendidos' => $productosPedidos,
            'productos_cab' => $pedidos
        ]);
        $pdf = Yii::$app->pdf;
        $pdf->format = [200, 90];
        $pdf->marginLeft = '5px';
        $pdf->marginRight = '5px';
        $pdf->marginTop = '5px';
        $pdf->marginTop = '5px';
        $pdf->content = $html;
        return $pdf->render();
    }
}




