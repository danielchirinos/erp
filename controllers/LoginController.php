<?php 

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\_ContactoForm;
use yii\widgets\ActiveForm;
use yii\web\Response;
use app\models\Login;
use app\models\Usuarios;

class LoginController extends Controller{
	
	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public $layout = false;

	public function actionIndex(){
    
		$model = new Login();
		
		if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if ($model->load(Yii::$app->request->post())) {
			if ($model->validate()) {
				
				$usuario = Usuarios::findOne(["usuario" => $model->Usuario, "clave" => md5($model->Clave)]);

				if ($usuario != null) {
					if ($usuario->activo == 0) {
						Yii::$app->session->setFlash("error","Usuario inactivo, no puede acceder al sistema!! ");
						return $this->render('login', ['model' => $model]);
					}
					Yii::$app->session->setFlash("success","Accedio correctamente - Bienvenido");

					$session = Yii::$app->session;
					$session->open();
					$session['IdUsuario'] = $usuario->id;
					$session['nombre'] = $usuario->usuario;
					
					return $this->redirect(['admin/index']);
				}
				else{
					Yii::$app->session->setFlash("error", "Usuario o clave incorrecta");
					return $this->render('login', ['model' => $model]);
				}	
			} else{
				Yii::$app->session->setFlash("error","Ha ocurrido un error al iniciar sesion!! ");
				$model->getErrors();
			}
		}
		else{
			return $this->render('login', ['model' => $model]);
		}	
	}

	public function actionCambiarcontrasena($id){

		if (isset($_POST["contrasenaVieja"])) {

			$usuario = Usuarios::findOne(["IdUsuario" => $id, "Clave" => md5($_POST["contrasenaVieja"])]);

			if ($usuario) {
				
				if (isset($_POST["contrasenaNueva"])) {
					
					Yii::$app->session->setFlash("success","La contraseña se ha modificado, inicie sesion nuevamente!! ");
					$usuario->Clave = md5($_POST["contrasenaNueva"]);
					$usuario->save();
					return $this->goHome();
				}
			}
		}

		Yii::$app->session->setFlash("error","La contraseña anterior no coinciden!! ");
		return $this->redirect(['home/index']);

	}

	public function actionLogout(){
        $session = Yii::$app->session;

		// $session->remove("IdUsuario");
		foreach ($session as $name => $value){
			$session->remove($name);
		}
		$session->close();
		Yii::$app->session->setFlash("success","Ha salido del sistema!!");
		return $this->redirect('index');
	}




}

