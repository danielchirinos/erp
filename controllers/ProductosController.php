<?php 

namespace app\controllers;

use Yii;
use app\models\Posts;
use yii\web\Response;
use app\models\Carrito;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Productos;
use yii\web\UploadedFile;
use app\models\Categorias;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\_ContactoForm;
use app\models\Pedidoslin;

class ProductosController extends Controller{


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function init() {
        parent::init();
        if (!isset(Yii::$app->session["nombre"])) {
            Yii::$app->session->setFlash("warning","Debe iniciar sesión para acceder a esta página");
            return $this->redirect(['/login']);
        }

        $this->layout = 'admin';
    }

	public function actionCrear(){

        $model = new Productos();

        if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
        // Yii::$app->request->isPost
        if($_POST){
            
            if($_POST["Productos"]["precio_venta"] == ""){
                Yii::$app->session->setFlash("error","El Producto debe tener un precio de venta");
                return $this->redirect(['/productos/crear']);
            }
            if($_POST["Productos"]["precio_costo"] == ""){
                Yii::$app->session->setFlash("error","El Producto debe tener un precio de costo");
                return $this->redirect(['/productos/crear']);
            }
            if($_POST["Productos"]["cantidad"] == ""){
                Yii::$app->session->setFlash("error","El Producto debe tener una cantidad de stock");
                return $this->redirect(['/productos/crear']);
            }
            if($_FILES["Productos"]["name"]["imagen"] == ""){
                Yii::$app->session->setFlash("error","El Producto debe tener una imagen de venta");
                return $this->redirect(['/productos/crear']);
            }

            $model->sku = $_POST["Productos"]["sku"];
            $model->descripcion = $_POST["Productos"]["descripcion"];
            $model->precio_costo = $_POST["Productos"]["precio_costo"];
            $model->precio_venta = $_POST["Productos"]["precio_venta"];
            $model->cantidad = intval($_POST["Productos"]["cantidad"]);

            //si viene alguna imagen
            if ($_FILES["Productos"]["name"]["imagen"] != "") {
                $model->imagen = UploadedFile::getInstance($model, 'imagen');
                
                $variableDate = date("Ymdhis");
                
                if ($model->imagen) { 
                    //se guarda la imagen en la ruta especificada               
                    $model->imagen->saveAs('images/productos/producto' . $variableDate . '.' . $model->imagen->extension);
                }
                //se le asigna el nombre de la imagen al articulo
                $model->imagen = "producto" . $variableDate . '.' . $model->imagen->extension;  
            }

            if ($model->save()) {

                /* if ($model->imagen != null) {
                    //si se guarda el producto, se crea una minuatura de la imagen
                    if ($this->resizeImagen($model->imagen)) {
                        $model->update();
                    }
                } */
                
                return $this->redirect(['lista']);
            }else{
                var_dump($model->getErrors());exit;
            }
            
        }
        
		return $this->render('crear',['model' => $model]);
		
    }
    
    public function actionLista(){

        if ($_POST) {
            $consulta = Productos::find()->where(['like', 'descripcion', '%' . $_POST["txt_buscar"] . '%', false])->orderBy(['descripcion'=>SORT_DESC]);
        }else{
            $consulta = Productos::find()->orderBy(['descripcion'=>SORT_ASC]);
        }

        $pages = new Pagination([
            'defaultPageSize' => 12, 
            'totalCount' => $consulta->count()
        ]);

        $model = $consulta->offset($pages->offset)
                        ->limit($pages->limit)
                        ->all();

        $campo = isset($_POST["txt_buscar"]) ? $_POST["txt_buscar"] : "";
        return $this->render('lista', ['model' => $model,'pages' => $pages, 'campo' => $campo]);

    }

    public function actionEditar($id){

        $model = Productos::findOne($id);
        

		if ((Yii::$app->request->isAjax) && ($model->load(Yii::$app->request->post()))) {
			
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
        
		if ($_POST) {
			if ($model->validate()) {    
                
                $model->descripcion = $_POST["Productos"]["descripcion"];
                $model->sku = $_POST["Productos"]["sku"];
                $model->precio_costo = $_POST["Productos"]["precio_costo"];
                $model->precio_venta = $_POST["Productos"]["precio_venta"];
                $model->activo = isset($_POST["check_activo"]) ? 1 : 0;
                $model->cantidad = intval($_POST["Productos"]["cantidad"]);


                if ($_FILES["Productos"]["name"]["imagen"] != "") {
                    $model->imagen = UploadedFile::getInstance($model, 'imagen');
                    
                    $variableDate = date("Ymdhis");
                    
                    if ($model->imagen) {                
                        $model->imagen->saveAs('images/productos/'. $model->descripcion . $variableDate . '.' . $model->imagen->extension);
                    }
                    $model->imagen = $model->descripcion . $variableDate . '.' . $model->imagen->extension;

                    //si se edita el producto, se crea una minuatura de la imagen
                   /*  $thumbail = $this->resizeImagen($model->imagen); */

                }

                if ($model->save()) {
                    //al guardar el precio, si el producto esta en el carrito, se actualiza el precio

                    Yii::$app->session->setFlash("success","Se edito el articulo con exito");
                    return $this->redirect(['lista']);
                }else{
                    Yii::$app->session->setFlash("success","Ha ocurrido un error al actualizar el producto!! ");
                    // var_dump($model->getErrors());
                    // exit;
                    return $this->redirect(['lista']);
                }

               
                
	
			}
			else{
                Yii::$app->session->setFlash("success","Ha ocurrido un error al actualizar el producto!! ");
                return $this->redirect(['lista']);
                // var_dump($model->getErrors());
                // exit;
			}
		}
		else{;
			return $this->render('editar', ['model' => $model]);
		}

    }
    
    public function resizeImagen($imagen){

        $original = imagecreatefromjpeg(Yii::getAlias('@webroot/images/productos/').$imagen);


        // parametros de imagecopyresized (
        // 1 path dela nueva foto, 
        // 2 foto original, 
        // 3 coodenadas x donde empieza a crear la imagen, 
        // 4 coordenadas en y donde se creara la imagen, 
        // 5 coodenadas x donde empieza a copiar la imagen, 
        // 6 coordenadas en y donde se copiar la imagen, 
        // 7 coodenadas x donde termina la copiar de la imagen, 
        // 8 coordenadas en Y donde se termina de copiar la imagen, 
        // 9 coodenadas x donde termina la copiar de la imagen original, 
        // 10 coordenadas en Y donde se termina de copiar la imagen original)

        $cw  = 300; //ancho de la copia
        $ch  = 300; //alto de la copia

        $copia = imagecreatetruecolor($cw,$ch);

        $ow = 400; //ancho de la original
        $oh = 400; //alto de la original

        $owf = 0; //ancho desde donde se empieza la copiar en el original
        $owh = 0; //alto desde donde se empieza la copiar en el original


        $ancho = imagesx($original);
        $alto = imagesy($original);

        if ($ancho > $alto) {
            $ow = $alto;
            $oh = $alto;

            $owf = ($ancho - $alto) / 2;
            $ohf = 0;
        }else{
            $ow = $ancho;
            $oh = $ancho;

            $ohf = ($alto - $ancho) / 2;
            $owf = 0;
        }

        imagecopyresized($copia, $original, 0,0, $owf, $owh, $cw, $ch, $ow, $oh);

        return imagejpeg($copia, Yii::getAlias('@webroot/images/thum/').$imagen, 100);

    } 

	public function actionEliminar(){
        if($_POST){
            $id_producto = Productos::findOne($_POST['id']);

            $pedido = Pedidoslin::find()->where(['id_producto' => $id_producto->id])->one();

            if(!$pedido){
                $id_producto->delete();

                Yii::$app->session->setFlash("success","Producto borrado correctamente");
                return $this->redirect(['lista']);
            }

            Yii::$app->session->setFlash("warning","No se puede borrar un producto cuando está asociado a un pedido");
            return $this->redirect(['lista']);
        }
    }
}
