/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100422
 Source Host           : localhost:33070
 Source Schema         : erp

 Target Server Type    : MySQL
 Target Server Version : 100422
 File Encoding         : 65001

 Date: 06/06/2022 19:28:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cierrecajacab
-- ----------------------------
DROP TABLE IF EXISTS `cierrecajacab`;
CREATE TABLE `cierrecajacab`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `id_usuario` int NOT NULL,
  `total_cierre` bigint NOT NULL,
  `fecha_cierre` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `id_usuario`(`id_usuario`) USING BTREE,
  CONSTRAINT `cierrecajacab_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cierrecajacab
-- ----------------------------
INSERT INTO `cierrecajacab` VALUES (6, 1, 60, '2022-06-07 00:00:00');

-- ----------------------------
-- Table structure for cierrecajalin
-- ----------------------------
DROP TABLE IF EXISTS `cierrecajalin`;
CREATE TABLE `cierrecajalin`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `id_cierrecajacab` bigint NOT NULL,
  `id_mediopago` int NOT NULL,
  `monto_mediopago` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `id_cierrecajacab`(`id_cierrecajacab`) USING BTREE,
  INDEX `id_mediopago`(`id_mediopago`) USING BTREE,
  CONSTRAINT `cierrecajalin_ibfk_1` FOREIGN KEY (`id_cierrecajacab`) REFERENCES `cierrecajacab` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cierrecajalin_ibfk_2` FOREIGN KEY (`id_mediopago`) REFERENCES `mediopago` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cierrecajalin
-- ----------------------------
INSERT INTO `cierrecajalin` VALUES (13, 6, 1, 10);
INSERT INTO `cierrecajalin` VALUES (14, 6, 2, 20);
INSERT INTO `cierrecajalin` VALUES (15, 6, 3, 30);

-- ----------------------------
-- Table structure for clientes
-- ----------------------------
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `rut` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nombre` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `apellido` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `direccion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telefono` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `activo` int NOT NULL DEFAULT 1,
  `giro` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `comuna` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ciudad` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  UNIQUE INDEX `rut`(`rut`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clientes
-- ----------------------------
INSERT INTO `clientes` VALUES (1, '11.111.111-1', '1', '1', '1@1.com', '1', '1', 1, '1', '1', '12');

-- ----------------------------
-- Table structure for comunas
-- ----------------------------
DROP TABLE IF EXISTS `comunas`;
CREATE TABLE `comunas`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `activo` tinyint(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comunas
-- ----------------------------
INSERT INTO `comunas` VALUES (1, 'Conchalí', 1);
INSERT INTO `comunas` VALUES (2, 'Huechuraba', 1);
INSERT INTO `comunas` VALUES (3, 'Independencia', 1);
INSERT INTO `comunas` VALUES (4, 'Las Condes', 1);
INSERT INTO `comunas` VALUES (5, 'Macul', 1);
INSERT INTO `comunas` VALUES (6, 'Ñuñoa', 1);
INSERT INTO `comunas` VALUES (7, 'Providencia', 1);
INSERT INTO `comunas` VALUES (8, 'Pudahuel', 1);
INSERT INTO `comunas` VALUES (9, 'Quilicura', 1);
INSERT INTO `comunas` VALUES (10, 'Quinta Normal', 1);
INSERT INTO `comunas` VALUES (11, 'Recoleta', 1);
INSERT INTO `comunas` VALUES (12, 'San Miguel', 1);
INSERT INTO `comunas` VALUES (13, 'Santiago Centro', 1);
INSERT INTO `comunas` VALUES (14, 'Valle Grande', 1);
INSERT INTO `comunas` VALUES (15, 'Vitacura', 1);
INSERT INTO `comunas` VALUES (16, 'Lo Barnechea', 1);
INSERT INTO `comunas` VALUES (17, 'Chicureo', 1);
INSERT INTO `comunas` VALUES (18, 'Estación Central', 1);
INSERT INTO `comunas` VALUES (19, 'La Reina', 1);
INSERT INTO `comunas` VALUES (20, 'Independencia', 0);
INSERT INTO `comunas` VALUES (21, 'San Joaquin', 1);
INSERT INTO `comunas` VALUES (22, 'Renca ', 1);
INSERT INTO `comunas` VALUES (23, 'Cerro Navia', 1);
INSERT INTO `comunas` VALUES (24, 'Lo Prado', 1);
INSERT INTO `comunas` VALUES (25, 'PAC ', 1);
INSERT INTO `comunas` VALUES (26, 'Cerrillos', 1);
INSERT INTO `comunas` VALUES (27, 'Pedro Aguirre Cerda', 1);

-- ----------------------------
-- Table structure for estadospedidos
-- ----------------------------
DROP TABLE IF EXISTS `estadospedidos`;
CREATE TABLE `estadospedidos`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `estado` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of estadospedidos
-- ----------------------------
INSERT INTO `estadospedidos` VALUES (1, 'Pagado', 'pedido por entregar', 1);
INSERT INTO `estadospedidos` VALUES (2, 'Entregado', 'perido entregado a cliente', 1);
INSERT INTO `estadospedidos` VALUES (3, 'Rechazado', 'Estuvo guardado, el usuario fue a pagar y por algun motivo la pasarela no tomo el pago, puede pagar ir a pagar\r\n\r\n', 1);
INSERT INTO `estadospedidos` VALUES (4, 'Guardado', 'Pedido guardado, pedido ya ingresado en BD, si el usuario no paga el pedido, quedara guardado y no podra volver a recuperarlo', 1);

-- ----------------------------
-- Table structure for mediopago
-- ----------------------------
DROP TABLE IF EXISTS `mediopago`;
CREATE TABLE `mediopago`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `activo` tinyint NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mediopago
-- ----------------------------
INSERT INTO `mediopago` VALUES (1, 'Efectivo', 1);
INSERT INTO `mediopago` VALUES (2, 'Debito', 1);
INSERT INTO `mediopago` VALUES (3, 'Credito', 1);

-- ----------------------------
-- Table structure for movimientoinventario
-- ----------------------------
DROP TABLE IF EXISTS `movimientoinventario`;
CREATE TABLE `movimientoinventario`  (
  `id` bigint NOT NULL,
  `id_usuario` int NOT NULL,
  `cantidad` int NOT NULL,
  `id_producto` int NOT NULL,
  `fecha_movimiento` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of movimientoinventario
-- ----------------------------

-- ----------------------------
-- Table structure for pedidoscab
-- ----------------------------
DROP TABLE IF EXISTS `pedidoscab`;
CREATE TABLE `pedidoscab`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_cliente` int NOT NULL,
  `id_estadopedido` int NOT NULL,
  `id_comuna` int NULL DEFAULT NULL,
  `id_usuario` int NOT NULL COMMENT 'vendedor del pedido',
  `cantidad_productos` int NOT NULL,
  `precio_total` int NOT NULL,
  `direccion_despacho` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fecha_creacion` date NULL DEFAULT NULL,
  `fecha_despacho` date NULL DEFAULT NULL,
  `token_transbank` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_pedido` tinyint NULL DEFAULT NULL COMMENT '0 para web / 1 para manual',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `id_cliente`(`id_cliente`) USING BTREE,
  INDEX `id_estadopedido`(`id_estadopedido`) USING BTREE,
  INDEX `id_comuna`(`id_comuna`) USING BTREE,
  INDEX `id_usuario`(`id_usuario`) USING BTREE,
  CONSTRAINT `pedidoscab_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pedidoscab_ibfk_2` FOREIGN KEY (`id_estadopedido`) REFERENCES `estadospedidos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pedidoscab_ibfk_3` FOREIGN KEY (`id_comuna`) REFERENCES `comunas` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pedidoscab_ibfk_4` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pedidoscab
-- ----------------------------

-- ----------------------------
-- Table structure for pedidoscabpagos
-- ----------------------------
DROP TABLE IF EXISTS `pedidoscabpagos`;
CREATE TABLE `pedidoscabpagos`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_pedidoscab` int NOT NULL COMMENT 'id del pedido',
  `id_medio_pago` int NOT NULL,
  `orden_compra` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `fecha_transaccion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `monto_transaccion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `comprobante` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `pedidoscabpagos_pedidoscab`(`id_pedidoscab`) USING BTREE,
  INDEX `pedidoscabpagos_mediopago`(`id_medio_pago`) USING BTREE,
  CONSTRAINT `pedidoscabpagos_ibfk_1` FOREIGN KEY (`id_pedidoscab`) REFERENCES `pedidoscab` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pedidoscabpagos_ibfk_2` FOREIGN KEY (`id_medio_pago`) REFERENCES `mediopago` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pedidoscabpagos
-- ----------------------------

-- ----------------------------
-- Table structure for pedidoslin
-- ----------------------------
DROP TABLE IF EXISTS `pedidoslin`;
CREATE TABLE `pedidoslin`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_pedidoscab` int NOT NULL,
  `id_producto` int NOT NULL,
  `cantidad` int NOT NULL,
  `precio` int NOT NULL,
  `precio_adicional` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `id_pedidoscab`(`id_pedidoscab`) USING BTREE,
  INDEX `id_producto`(`id_producto`) USING BTREE,
  CONSTRAINT `pedidoslin_ibfk_1` FOREIGN KEY (`id_pedidoscab`) REFERENCES `pedidoscab` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `pedidoslin_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pedidoslin
-- ----------------------------

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `precio_costo` int NULL DEFAULT 0,
  `precio_venta` int NULL DEFAULT NULL,
  `imagen` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cantidad` int NOT NULL DEFAULT 0,
  `activo` int NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  UNIQUE INDEX `sku`(`sku`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productos
-- ----------------------------

-- ----------------------------
-- Table structure for tipousuario
-- ----------------------------
DROP TABLE IF EXISTS `tipousuario`;
CREATE TABLE `tipousuario`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tipousuario
-- ----------------------------
INSERT INTO `tipousuario` VALUES (1, 'Administrador');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_tipo_usuario` int NOT NULL,
  `usuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  INDEX `id_tipo_usuario`(`id_tipo_usuario`) USING BTREE,
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tipousuario` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES (1, 1, 'mbgreen', '3ef73fd6734408265108d9eaf108a7f3', 1);

SET FOREIGN_KEY_CHECKS = 1;
