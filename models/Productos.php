<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string $sku
 * @property string $descripcion
 * @property int $precio_costo
 * @property int $precio_venta
 * @property int $cantidad
 * @property string $imagen
 * @property int $activo
 *
 * @property Pedidoslin[] $pedidoslins
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'cantidad'], 'required'],
            [['precio_costo', 'precio_venta', 'cantidad', 'activo'], 'integer'],
            [['sku', 'descripcion', 'imagen'], 'string', 'max' => 255],
            [['sku'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'descripcion' => 'Descripcion',
            'precio_costo' => 'Precio Costo',
            'precio_venta' => 'Precio Venta',
            'cantidad' => 'Cantidad',
            'imagen' => 'Imagen',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoslins()
    {
        return $this->hasMany(Pedidoslin::className(), ['id_producto' => 'id']);
    }
}
