<?php

namespace app\models;

use Yii;
use yii\base\Model;

class _ActualizarClaveForm extends Model
{
    public $clave;
    public $claveRepetir;
    public $rut;

   
    public function rules()
    {
        return [
            [['clave', 'claveRepetir'], 'required'],
            [['clave', 'claveRepetir'], 'string', 'max' => 255],
            [['claveRepetir'], 'compare', 'compareAttribute' => 'clave', 'message' => 'Las claves no coinciden'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'clave' => 'Ingrese su nueva clave',
            'claveRepetir' => 'Repita la clave ingresada',
            'rut' => 'Rut',
        ];
    }


}