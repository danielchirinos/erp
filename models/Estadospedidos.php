<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estadospedidos".
 *
 * @property int $id
 * @property string $estado
 * @property string $descripcion
 * @property int $activo
 *
 * @property Pedidoscab[] $pedidoscabs
 */
class Estadospedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estadospedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado', 'descripcion'], 'required'],
            [['activo'], 'integer'],
            [['estado', 'descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estado' => 'Estado',
            'descripcion' => 'Descripcion',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscabs()
    {
        return $this->hasMany(Pedidoscab::className(), ['id_estadopedido' => 'id']);
    }
}
