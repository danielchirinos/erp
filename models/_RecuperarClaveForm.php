<?php

namespace app\models;

use Yii;
use yii\base\Model;

class _RecuperarClaveForm extends Model
{
    public $email;

   
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'string'],
            ['email', 'email', 'message' => 'Ingrese un email valido'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Ingrese su dirección de correo',
        ];
    }


}