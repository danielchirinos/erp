<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedidoslin".
 *
 * @property int $id
 * @property int $id_pedidoscab
 * @property int $id_producto
 * @property int $cantidad
 * @property int $precio
 * @property int $precio_adicional
 *
 * @property Pedidoscab $pedidoscab
 * @property Productos $producto
 */
class Pedidoslin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidoslin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pedidoscab', 'id_producto', 'cantidad', 'precio'], 'required'],
            [['id_pedidoscab', 'id_producto', 'cantidad', 'precio', 'precio_adicional'], 'integer'],
            [['id_pedidoscab'], 'exist', 'skipOnError' => true, 'targetClass' => Pedidoscab::className(), 'targetAttribute' => ['id_pedidoscab' => 'id']],
            [['id_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['id_producto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pedidoscab' => 'Id Pedidoscab',
            'id_producto' => 'Id Producto',
            'cantidad' => 'Cantidad',
            'precio' => 'Precio',
            'precio_adicional' => 'Precio Adicional',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscab()
    {
        return $this->hasOne(Pedidoscab::className(), ['id' => 'id_pedidoscab']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Productos::className(), ['id' => 'id_producto']);
    }
}
