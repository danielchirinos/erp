<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Clientes;

class _CambioclaveForm extends Model
{
    public $claveAnterior;
    public $clave;
    public $claveRepetir;

   
    public function rules()
    {
        return [
            [['claveAnterior', 'clave', 'claveRepetir'], 'required'],
            [['claveAnterior', 'clave', 'claveRepetir'], 'string'],

            [['claveAnterior'], 'validarClaveAnterior'],
            [['claveRepetir'], 'compare', 'compareAttribute' => 'clave', 'message' => 'Las claves no coinciden'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'claveAnterior' => 'Clave Anterior',
            'clave' => 'Nueva Clave',
            'claveRepetir' => 'Repetir Nueva Clave',
        ];
    }


    public function validarClaveAnterior(){
        
        $session = Yii::$app->session;
        $verificarClave = Clientes::findOne(["id"=> $session['IdCliente']]);
        if($verificarClave != null){
            if ($verificarClave->clave != md5($this->claveAnterior)) {
                $this->addError("claveAnterior", "Clave Anterior incorrecta");
            }  
        }else{
            $this->addError("claveAnterior", "Clave Anterior incorrecta");
        }
    }


}