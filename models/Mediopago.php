<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mediopago".
 *
 * @property int $id
 * @property string $descripcion
 * @property int $activo
 *
 * @property Cierrecajalin[] $cierrecajalins
 * @property Pedidoscabpagos[] $pedidoscabpagos
 */
class Mediopago extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mediopago';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'required'],
            [['activo'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCierrecajalins()
    {
        return $this->hasMany(Cierrecajalin::className(), ['id_mediopago' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscabpagos()
    {
        return $this->hasMany(Pedidoscabpagos::className(), ['id_medio_pago' => 'id']);
    }
}
