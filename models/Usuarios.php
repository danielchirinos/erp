<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property int $id_tipo_usuario
 * @property string $usuario
 * @property string $clave
 * @property int $activo
 *
 * @property Pedidoscab[] $pedidoscabs
 * @property Tipousuario $tipoUsuario
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_usuario', 'usuario', 'clave'], 'required'],
            [['id_tipo_usuario', 'activo'], 'integer'],
            [['usuario', 'clave'], 'string', 'max' => 100],
            [['usuario'], 'unique'],
            [['id_tipo_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Tipousuario::className(), 'targetAttribute' => ['id_tipo_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tipo_usuario' => 'Id Tipo Usuario',
            'usuario' => 'Usuario',
            'clave' => 'Clave',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscabs()
    {
        return $this->hasMany(Pedidoscab::className(), ['id_usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoUsuario()
    {
        return $this->hasOne(Tipousuario::className(), ['id' => 'id_tipo_usuario']);
    }
}
