<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedidoscabpagos".
 *
 * @property int $id
 * @property int $id_pedidoscab id del pedido
 * @property int $id_medio_pago
 * @property string $orden_compra
 * @property string $fecha_transaccion
 * @property string $monto_transaccion
 * @property string $comprobante
 *
 * @property Pedidoscab $pedidoscab
 * @property Mediopago $medioPago
 */
class Pedidoscabpagos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidoscabpagos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pedidoscab', 'id_medio_pago'], 'required'],
            [['id_pedidoscab', 'id_medio_pago'], 'integer'],
            [['comprobante'], 'string'],
            [['orden_compra', 'fecha_transaccion', 'monto_transaccion'], 'string', 'max' => 255],
            [['id_pedidoscab'], 'exist', 'skipOnError' => true, 'targetClass' => Pedidoscab::className(), 'targetAttribute' => ['id_pedidoscab' => 'id']],
            [['id_medio_pago'], 'exist', 'skipOnError' => true, 'targetClass' => Mediopago::className(), 'targetAttribute' => ['id_medio_pago' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pedidoscab' => 'Id Pedidoscab',
            'id_medio_pago' => 'Id Medio Pago',
            'orden_compra' => 'Orden Compra',
            'fecha_transaccion' => 'Fecha Transaccion',
            'monto_transaccion' => 'Monto Transaccion',
            'comprobante' => 'Comprobante',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscab()
    {
        return $this->hasOne(Pedidoscab::className(), ['id' => 'id_pedidoscab']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedioPago()
    {
        return $this->hasOne(Mediopago::className(), ['id' => 'id_medio_pago']);
    }
}
