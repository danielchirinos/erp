<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cierrecajacab".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $total_cierre
 * @property string $fecha_cierre
 * @property string $nombre_persona
 * @property int $efectivo
 * @property int $debito
 * @property int $credito
 * @property int $transferencia
 *
 * @property Usuarios $usuario
 */
class Cierrecajacab extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cierrecajacab';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'total_cierre', 'fecha_cierre', 'nombre_persona', 'efectivo', 'debito', 'credito', 'transferencia'], 'required'],
            [['id_usuario', 'total_cierre', 'efectivo', 'debito', 'credito', 'transferencia'], 'integer'],
            [['fecha_cierre'], 'safe'],
            [['nombre_persona'], 'string', 'max' => 255],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'total_cierre' => 'Total Cierre',
            'fecha_cierre' => 'Fecha Cierre',
            'nombre_persona' => 'Nombre Persona',
            'efectivo' => 'Efectivo',
            'debito' => 'Debito',
            'credito' => 'Credito',
            'transferencia' => 'Transferencia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'id_usuario']);
    }
}
