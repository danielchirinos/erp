<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comunas".
 *
 * @property int $id
 * @property string $nombre
 * @property int $activo
 *
 * @property Pedidoscab[] $pedidoscabs
 */
class Comunas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comunas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['activo'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscabs()
    {
        return $this->hasMany(Pedidoscab::className(), ['id_comuna' => 'id']);
    }
}
