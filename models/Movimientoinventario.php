<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movimientoinventario".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $cantidad
 * @property int $id_producto
 * @property string $fecha_movimiento
 */
class Movimientoinventario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movimientoinventario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_usuario', 'cantidad', 'id_producto', 'fecha_movimiento'], 'required'],
            [['id', 'id_usuario', 'cantidad', 'id_producto'], 'integer'],
            [['fecha_movimiento'], 'safe'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'cantidad' => 'Cantidad',
            'id_producto' => 'Id Producto',
            'fecha_movimiento' => 'Fecha Movimiento',
        ];
    }
}
