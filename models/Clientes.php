<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $id
 * @property string $rut
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property string $direccion
 * @property string $telefono
 * @property int $activo
 * @property string $giro
 * @property string $comuna
 * @property string $ciudad
 *
 * @property Pedidoscab[] $pedidoscabs
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido'], 'required'],
            [['activo'], 'integer'],
            [['rut'], 'string', 'max' => 15],
            [['nombre', 'apellido', 'email', 'direccion', 'telefono', 'giro', 'comuna', 'ciudad'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['rut'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rut' => 'Rut',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'activo' => 'Activo',
            'giro' => 'Giro',
            'comuna' => 'Comuna',
            'ciudad' => 'Ciudad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscabs()
    {
        return $this->hasMany(Pedidoscab::className(), ['id_cliente' => 'id']);
    }
}
