<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedidoscab".
 *
 * @property int $id
 * @property int $id_cliente
 * @property int $id_estadopedido
 * @property int $id_comuna
 * @property int $id_usuario vendedor del pedido
 * @property int $cantidad_productos
 * @property int $precio_total
 * @property string $direccion_despacho
 * @property string $fecha_creacion
 * @property string $fecha_despacho
 * @property string $token_transbank
 * @property int $tipo_pedido 0 para web / 1 para manual
 * @property int $cerrado 0 para no cerrado / 1 para cerrado
 *
 * @property Clientes $cliente
 * @property Estadospedidos $estadopedido
 * @property Comunas $comuna
 * @property Usuarios $usuario
 * @property Pedidoscabpagos[] $pedidoscabpagos
 * @property Pedidoslin[] $pedidoslins
 */
class Pedidoscab extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidoscab';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cliente', 'id_estadopedido', 'id_usuario', 'cantidad_productos', 'precio_total'], 'required'],
            [['id_cliente', 'id_estadopedido', 'id_comuna', 'id_usuario', 'cantidad_productos', 'precio_total', 'tipo_pedido', 'cerrado'], 'integer'],
            [['fecha_creacion', 'fecha_despacho'], 'safe'],
            [['direccion_despacho', 'token_transbank'], 'string', 'max' => 255],
            [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['id_cliente' => 'id']],
            [['id_estadopedido'], 'exist', 'skipOnError' => true, 'targetClass' => Estadospedidos::className(), 'targetAttribute' => ['id_estadopedido' => 'id']],
            [['id_comuna'], 'exist', 'skipOnError' => true, 'targetClass' => Comunas::className(), 'targetAttribute' => ['id_comuna' => 'id']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cliente' => 'Id Cliente',
            'id_estadopedido' => 'Id Estadopedido',
            'id_comuna' => 'Id Comuna',
            'id_usuario' => 'Id Usuario',
            'cantidad_productos' => 'Cantidad Productos',
            'precio_total' => 'Precio Total',
            'direccion_despacho' => 'Direccion Despacho',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_despacho' => 'Fecha Despacho',
            'token_transbank' => 'Token Transbank',
            'tipo_pedido' => 'Tipo Pedido',
            'cerrado' => 'Cerrado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Clientes::className(), ['id' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadopedido()
    {
        return $this->hasOne(Estadospedidos::className(), ['id' => 'id_estadopedido']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComuna()
    {
        return $this->hasOne(Comunas::className(), ['id' => 'id_comuna']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoscabpagos()
    {
        return $this->hasMany(Pedidoscabpagos::className(), ['id_pedidoscab' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidoslins()
    {
        return $this->hasMany(Pedidoslin::className(), ['id_pedidoscab' => 'id']);
    }
}
