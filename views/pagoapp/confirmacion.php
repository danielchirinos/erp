<?php

$this->title = 'Confirmación del pago';
$this->params['activeLink'] = "confirmacion-pago";

?>

<section class="banner-bottom-wthreelayouts py-lg-5 py-3">
	<div class="container">
		<div class="inner-sec-shop px-lg-0 px-md-0 px-sm-0 px-4">
			<h3 class="tittle-w3layouts my-lg-4 mt-3">Confirmación del pago </h3>
		</div>	
        <hr>
        <div class="row">

            <div class="col-md-12">
                <div class="px-lg-0 px-md-0 px-sm-0 px-2">
                    <div class="alert alert-success " role="alert">
                        <h4 class="alert-heading">Confirmación de pago de pedido</h4>
                        <p>Gracias por su compra test </p>
                        <p>Siga las siguientes instrucciones</p>
                        <hr>
                        <ul class="pl-3">
                            <li>Le hemos enviado un email a su correo con el detalle de su pedido, <b>si no lo recibe, revise en la carpeta de "no deseados" o "spam". </b></li>
                            <?php if ($envioEmailDTE) { ?>
                                <li>Adicionalmente hemos enviado un email con la boleta de la compra.</li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <button onclick="cerrarNavegador()">Volver a la aplicación</button>
            </div>
        </div>

	</div>
</section>

<script>
    function cerrarNavegador(){
        window.close();
    }
</script>
		