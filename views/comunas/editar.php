<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
    $this->title = 'Editar Comuna'; 
    $this->params['activeLink'] = "comuna-editar";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>


<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/admin/bootstrap-select-1.13.15/css/bootstrap-select.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/admin/bootstrap-select-1.13.15/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/admin/bootstrap-select-1.13.15/js/i18n/defaults-es_CL.js"></script>

<div class="container-fluid">


    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title .' '. $model->nombre ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/admin/index">Inicio</a></li>
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/comunas/lista">Lista comunas</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">

                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'EditarComuna', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ], 
                        
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'nombre', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Nombre comuna']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'precio', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Precio']) ?>
                            </div>


                            <div class="col-md-3">
								<?= $form->field($model, 'dias_envio_gratis', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger ']])->dropdownList($diasGratis, ['class' => 'form-control selectpicker', 'multiple' => 'multiple', 'title' => 'Seleccione días']) ?>
							</div>

                            <div class="col-md-3 d-flex align-items-center">
                                <div class="custom-control custom-checkbox">
                                    <div class="checkbox checkbox-success">
                                        <input id="check_activo" type="checkbox" name="check_activo" <?= $model->activo == 1 ? "checked" : "" ?>>
                                        <label for="check_activo"> Activa </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light']) ?>
                                    
                                </div>
                            </div>
                            
                        </div>


                    <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
    $("#comunas-precio").inputmask("[9]{1,4}.[9]{1,2}");
    cargarDias();
});


function cargarDias(){
        <?php if ($diasGratisSeleccionados != null){ ?>   
            <?php foreach ($diasGratisSeleccionados as $key => $value) { ?>
                $.each($("#comunas-dias_envio_gratis").children(), function( key, value ) {
                    if ('<?= $value ?>' == $(value).val()) {
                        $(this).attr("selected", true);
                        
                    }
                }); 
                
            <?php }  ?>
            $("#comunas-dias_envio_gratis").selectpicker('refresh');
        <?php }  ?>

}

</script>