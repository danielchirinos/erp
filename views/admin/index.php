<?php
use yii\helpers\Html;
    $this->title = 'Admin - Inicio'; 
    $this->params['activeLink'] = "inicio";
?>
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- datatables -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>


    <!-- esto es el index 2 de la plantilla -->
    <div class="container-fluid">

        <!-- Bread crumb and right sidebar toggle -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor mb-0 mt-0">Inicio</h3>
                <!-- <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol> -->
            </div>

        </div>
        

        <div class="row">
        <!-- col-sm-6 col-md-6 col-lg-2 col-xl-2 -->

            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div class="card card-inverse card-primary">
                    <div class="box text-center">
                        <h1 class="font-light text-white"><?= $pedidosPagados ?></h1>
                        <h4 class="text-white">Pedidos pagados</h4>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div class="card card-inverse card-info">
                    <div class="box text-center">
                        <h1 class="font-light text-white"><?= $pedidosGuardados ?></h1>
                        <h4 class="text-white">Pedidos guardados</h4>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div class="card card-inverse card-success">
                    <div class="box text-center">
                        <h1 class="font-light text-white"><?= $pedidosEntregados ?></h1>
                        <h4 class="text-white">Pedidos entregados</h4>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                <div class="card card-inverse card-danger">
                    <div class="box text-center">
                        <h1 class="font-light text-white"><?= $pedidosRechazados ?></h1>
                        <h4 class="text-white">Pedidos rechazados</h4>
                    </div>
                </div>
            </div>

        </div>
        <div id="table_div">

        </div>
    </div>


<script>
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "<?= Yii::getAlias('@web'); ?>/admin/buscartabla",
            data: {},
            success: function (response) {
                if(response){
                    respuesta = $.parseJSON(response);
                    $("#table_div").html(respuesta.data);
                    construirTabla();
                }
            }
        });
    });

    function construirTabla(){
        $('#table').DataTable({
            dom: 'Bfrtip',
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ]
        });
    }
</script>

    

