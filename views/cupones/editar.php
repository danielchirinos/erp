<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
    $this->title = 'Editar Cupón'; 
    $this->params['activeLink'] = "cupon-editar";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datetimepicker/tempusdominus-bootstrap-4.min.css" />
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datetimepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datetimepicker/tempusdominus-bootstrap-4.min.js"></script>

<style>
.bootstrap-datetimepicker-widget.dropdown-menu {
    width: unset;
}
</style>

<div class="container-fluid">


    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title .' '. $model->cupon ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/admin/index">Inicio</a></li>
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/cupones/lista">Lista cupones</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">

                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'EditarCupon', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ], 
                        
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'cupon', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Nombre comuna']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'cantidad_uso', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Precio', 'type' => 'number']) ?>
                            </div>

                            <div class="col-md-3">
                                <?= $form->field($model, 'tipo_cupon', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->dropDownList(["0" => "Monto", "1" => "Porcentaje"], ['class' => 'form-control']) ?>
                                
                            </div>

                            <div class="col-md-2">
                                <?= $form->field($model, 'descuento', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'Precio']) ?>
                                <!-- <label>Precio</label>
                                <input type="text" class="form-control" value=""> -->
                            </div>

                            <div class="col-md-3">
                                <?= $form->field($model, 'fecha_vencimiento', ['template' => '{label}
                                <div class="form-group mb-0">
                                    <div class="input-group date" id="fecha_vencimiento" data-target-input="nearest">
                                        {input}
                                        <div class="input-group-append" data-target="#fecha_vencimiento" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                {error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                ])->textInput(['class'=>'form-control', 'placeholder' => 'dia / mes / año', 'readonly'=>'true', 'value' => date("d/m/Y", strtotime($model->fecha_vencimiento))]) ?>

                            </div>
                        </div>

                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light']) ?>
                                    
                                </div>
                            </div>
                            
                        </div>


                    <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
    $("#cupones-descuento").inputmask("[9]{1,5}.[9]{1,2}");

    $('#fecha_vencimiento').datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: "",
        ignoreReadonly: true,
        // debug : true,
        // minDate: "< date("m/d/Y") ?>",
    });

});

$("#cupones-tipo_cupon").change(function(){
    if ($(this).val() == 0) {
        $("#cupones-descuento").inputmask("[9]{1,5}.[9]{1,2}");
    }else{
        $("#cupones-descuento").inputmask("[9]{1,2}.[9]{1,2}");
    }
    
    
})

</script>