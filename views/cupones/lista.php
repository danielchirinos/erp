<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de cupones'; 
    $this->params['activeLink'] = "cupon-lista";
?>

<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- datatables -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($cupones)?></span></span>
        </div>
    </div>

    <div class="row">
        <?php if (count($cupones) == 0) { ?>
            <span>Sin cupones</span>
        <?php }else { ?>
        
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Lista de Pedidos</h4>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Cupón</th>
                                        <th>Tipo Cupón</th>
                                        <th>Descuento</th>
                                        <th>Cantidad de uso</th>
                                        <th>Cantidad veces usado</th>
                                        <th>Fecha vencimiento</th>
                                        <th>Fecha creación</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($cupones as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value["cupon"] ?></td>
                                        <td><?= $value["tipo_cupon"] == 0 ? "Monto" : "Porcentaje" ?></td>
                                        <td><?= $value["descuento"] ?></td>
                                        <td><?= $value["cantidad_uso"] ?></td>
                                        <td><?= count($value["cantidad_usado"]) > 0 ? $value["cantidad_usado"][0]["cantidad"] : 0 ?></td>

                                        <td><?= date("Y-m-d", strtotime($value["fecha_vencimiento"]))  ?></td>
                                        <td><?= date("Y-m-d", strtotime($value["fecha_creacion"])) ?></td>
                                        <td class="text-center"><a class="btn btn-secondary btn-sm" href="<?= Yii::getAlias('@web'); ?>/cupones/editar/<?= $value["id"] ?>"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>




<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    });
});
</script>