<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de cupones por cliente y pedido'; 
    $this->params['activeLink'] = "cupon-lista-cliente-pedido";
?>

<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- datatables -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($cuponesUsados)?></span></span>
        </div>
    </div>

    <div class="row">
        <?php if (count($cuponesUsados) == 0) { ?>
            <span>Sin cupones usados</span>
        <?php }else { ?>
        
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Lista de Cupone usados por clientes en pedidos</h4>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Cupón</th>
                                        <th>Tipo Cupón</th>
                                        <th>Descuento</th>
                                        <th>Id Pedido</th>
                                        <th>Cliente</th>
                                        <th>Fecha despacho</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($cuponesUsados as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value->cupon->cupon ?></td>
                                        <td><?= $value->cupon->tipo_cupon == 0 ? "Monto" : "Porcentaje" ?></td>
                                        <td><?= $value->cupon->descuento ?><?= $value->cupon->tipo_cupon == 0 ? "" : "%" ?></td>
                                        <td><?= $value->id ?></td>
                                        <td><?= $value->cliente->nombre ?></td>
                                        <td><?= $value->fecha_despacho ?></td>
                                        
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>




<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "order": [[5, 'desc']]
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    });
});
</script>