<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
    $this->title = 'Editar Producto'; 
    $this->params['activeLink'] = "producto-editar";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<div class="container-fluid">


    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title .' '. $model->descripcion ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/admin/index">Inicio</a></li>
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/lista">Lista productos</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">

                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'EditarProducto', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ], 
                        
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'sku', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Código'])->label('Código de Barra') ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'descripcion', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Descripción']) ?>
                            </div>
                            <div class="col-md-4">
                                <label>Imagen</label>
                                <!-- <div class="custom-file mb-3">
                                    
                                    <input type="file" name="imagen" class="custom-file-input" id="customFileLang" lang="es">
                                    <label class="custom-file-label form-control" for="customFileLang">Seleccionar Archivo</label>
                                </div> -->

                                <?= $form->field($model, 'imagen')->fileInput(["accept"=>"image/jpeg"])->label(false) ?>

                            </div>
                            <div class="col-md-1" id="imagenNueva" style="display:none"></div>
                            <div class="col-md-1" id="imagenDefault">
                                <?php $imagen = ($model->imagen == null) ? 'producto.jpg' : $model->imagen ?>
                                <img  src="<?= Yii::getAlias('@web') .'/images/thum/'.$imagen ?>" class="img-thumbnail" width="100" alt="">
                            </div>
                        </div>

                        <div class="row">
                            

                            <div class="col-md-2">
                                <?= $form->field($model, 'precio_costo', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'Precio']) ?>
                                <!-- <label>Precio</label>
                                <input type="text" class="form-control" value=""> -->
                            </div>
                            <div class="col-md-2">
                                <?= $form->field($model, 'precio_venta', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'Precio']) ?>
                                <!-- <label>Precio</label>
                                <input type="text" class="form-control" value=""> -->
                            </div>
                            <div class="col-md-2">
                                <?= $form->field($model, 'cantidad', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'Cantidad (Stock)', 'type' => 'number']) ?>
                                <!-- <label>Precio</label>
                                <input type="text" class="form-control" value=""> -->
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-danger mt-4" type="button" id="btn_formDelete" onclick="borrar(<?= $model->id ?>)"> <i class="fas fa-trash-alt"></i> Borrar Producto</button>
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light']) ?>
                                    
                                </div>
                            </div>
                            
                        </div>
                    <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
    $("#productos-precio_costo, #productos-precio_venta").inputmask("[9]{1,6}");

});


$('#productos-imagen').change(function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagenDefault').fadeOut(1);
            $('#imagenNueva').fadeIn("slow").html(`<img src="${e.target.result}" class="img-thumbnail" width="100">`);
        };

        reader.readAsDataURL(this.files[0]);
    }else{
        $('#imagenNueva').fadeOut(1);
        $('#imagenDefault').fadeIn(1);
    }
});


async function borrar(idProducto){
    $.ajax({
        type: "post",
        url: "<?= Yii::getAlias('@web'); ?>/productos/eliminar",
        data: {
            id: idProducto
        },
        success: function (response) {
            console.log(response);
        }
    });
}



</script>