<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de Productos'; 
    $this->params['activeLink'] = "producto-lista";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <div class="input-group d-flex justify-content-end">
                <?php $form = ActiveForm::begin(['method' => 'post', 'id'=> 'formulario' ]); ?>
                    <div class="input-group mb-3 ">
                        <input type="text" name="txt_buscar" class="form-control" placeholder="Buscar productos" aria-describedby="basic-addon2" value="<?= $campo ?>">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit">Button</button>
                        </div>
                    </div>
                                        
                <?php $form->end(); ?>

            </div>
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($model)?></span></span>
        </div>
    </div>

    <div class="row el-element-overlay">
        <?php foreach ($model as $key => $value) { ?>
           
            <div class="col-lg-2 col-md-3 d-flex">
                <div class="card">
                    <div class="el-card-item">
                        <div class="el-card-avatar el-overlay-1"> 
                            <img height="100px" src="<?= Yii::getAlias('@web') ?>/images/<?= $value->imagen == null  ? "productos/producto.jpg" : ($value->imagen == "" ? "productos/producto.jpg" : "productos/".$value->imagen) ?>" alt="<?= $value->descripcion ?>">
                        <div class="el-overlay">
                            <ul class="el-info">
                                <li>
                                    <a class="btn default btn-outline image-popup-vertical-fit" 
                                    href="<?= Yii::getAlias('@web') .'/productos/editar/'.$value->id?>">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="el-card-content text-left px-3">
                        <h3 class="box-title"><?= $value->descripcion ?></h3> 
                        <h3 class="badge badge-warning">Cantidad: <?= $value->cantidad ?></h3>
                    </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="row d-flex justify-content-center">
        <nav aria-label="Page navigation example">
        <?= LinkPager::widget([
            'pagination' => $pages,
            'options'=>['class'=>'pagination'],
            'lastPageLabel'=>'Última', // Set the label for the "last" page button
            'firstPageLabel'=>'Primera', // Set the label for the "first" page button
            'prevPageLabel' => '<', // Set the label for the "previous" page button
            'nextPageLabel' => '>', // Set the label for the "next" page button
            'nextPageCssClass'=>'page-item',    // Set CSS class for the "next" page button
            'prevPageCssClass'=>'page-item',    // Set CSS class for the "previous" page button 
            'firstPageCssClass'=>'page-item',    // Set CSS class for the "first" page button
            'lastPageCssClass'=>'page-item',    // Set CSS class for the "last" page button
            'maxButtonCount'=>3,]);      
        ?>
        </nav>
    </div>

 
</div>



<script>
$(document).ready(function(){
    

});
</script>