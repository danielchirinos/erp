<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
    $this->title = 'Crear Producto'; 
    $this->params['activeLink'] = "producto-crear";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Crear Producto</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/crear">Inicio</a></li>
                <li class="breadcrumb-item active">Crear Producto</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">

                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'CrearProducto', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ], 
                        
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <div class="row">
                            <form id="enviarSKU">
                                <div class="col-md-3">
                                    <?= $form->field($model, 'sku', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                            ])->textInput(['class'=>'form-control', 'placeholder' => 'Código'])->label('Código de Barra') ?>
                                </div>
                            </form>
                            <div class="col-md-3">
                                <?= $form->field($model, 'descripcion', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Descripción']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'precio_costo', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Precio']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'precio_venta', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Precio']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'cantidad', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Cantidad (Stock)', 'type' => 'number']) ?>
                            </div>
                            <div class="col-md-4">
                                <label>Imagen</label>
                                <!-- <div class="custom-file mb-3">
                                    
                                    <input type="file" name="imagen" class="custom-file-input" id="customFileLang" lang="es">
                                    <label class="custom-file-label form-control" for="customFileLang">Seleccionar Archivo</label>
                                </div> -->

                                <?= $form->field($model, 'imagen')->fileInput(["accept"=>"image/jpeg"])->label(false) ?>

                            </div>
                            <div class="col-md-1" id="imagenNueva" style="display:none"></div>
                            <div class="col-md-1" id="imagenDefault">
                                <img  src="<?= Yii::getAlias('@web') ?>/images/admin/crear_producto.jpg" class="img-thumbnail" width="100" alt="">
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <button type="button" onClick="generarProducto()" class="btn btn-success waves-effect waves-light">
                                        <i class="fa fa-check"></i> Guardar
                                    </button>
                                    <button type="button" onClick="borrarCodigo()" class="btn btn-danger waves-effect waves-light">
                                        <i class="fal fa-trash"></i> Borrar código
                                    </button>
                                </div>
                            </div>
                            
                        </div>

                    <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
    $("#productos-precio_costo, #productos-precio_venta").inputmask("[9]{1,6}");
    $("#productos-sku").select();
});


$('#productos-imagen').change(function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imagenDefault').fadeOut(1);
            $('#imagenNueva').fadeIn("slow").html(`<img src="${e.target.result}" class="img-thumbnail" width="100">`);
        };

        reader.readAsDataURL(this.files[0]);

    }else{
        $('#imagenNueva').fadeOut(1);
        $('#imagenDefault').fadeIn(1);
    }
});


function generarProducto(){ 
    $("#CrearProducto").submit();
}


function borrarCodigo(){
    $("#productos-sku").val('');
    $("#productos-sku").select();
}

</script>