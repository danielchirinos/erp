<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
    $this->title = 'Editar Dia bloqueado'; 
    $this->params['activeLink'] = "diasbloqueados-editar";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datetimepicker/tempusdominus-bootstrap-4.min.css" />
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datetimepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datetimepicker/tempusdominus-bootstrap-4.min.js"></script>

<style>
.bootstrap-datetimepicker-widget.dropdown-menu {
    width: unset;
}
</style>

<div class="container-fluid">


    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title .' '. $model->fecha ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/admin/index">Inicio</a></li>
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/diasbloqueados/lista">Lista dias bloqueados</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">

                <?php $form = ActiveForm::begin([
                    'method' => 'post', 
                    'id'=> 'CrearDiasBloqueados', 
                    'options'=> [
                        'class' => 'form-horizontal',
                    ], 
                    
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true, 
                    ]); ?>

                    <div class="row">
                        <div class="col-md-3">
                                <?= $form->field($model, 'fecha', ['template' => '{label}
                                <div class="form-group mb-0">
                                    <div class="input-group date" id="fecha_bloquear" data-target-input="nearest">
                                        {input}
                                        <div class="input-group-append" data-target="#fecha_bloquear" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                {error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                ])->textInput(['class'=>'form-control', 'placeholder' => 'dia / mes / año', 'readonly'=>'true']) ?>
                            </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'cantidad_pedidos', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                    ])->textInput(['class'=>'form-control', 'placeholder' => '5']) ?>
                        </div>

                        <div class="col-md-3 d-flex align-items-center">
                            <div class="custom-control custom-checkbox">
                                <div class="checkbox checkbox-success">
                                    <input id="check_bloqueo" type="checkbox" name="check_bloqueo" <?= ($model->bloqueo_inmediato == 1) ? 'checked' : '' ?>> 
                                    <label for="check_bloqueo"> Bloqueo Inmediato </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-actions">
                            <div class="card-body">
                                <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light']) ?>
                                
                            </div>
                        </div>
                        
                    </div>


                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>




<script>
$(document).ready(function(){

    $('#fecha_bloquear').datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: "",
        ignoreReadonly: true,
        // debug : true,
        disabledDates: [
            "<?= $diasbloqueados ?>",
        ],
        minDate: "<?= date("m/d/Y") ?>",
    });

});

</script>