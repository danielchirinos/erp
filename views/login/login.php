<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>



<?php $this->beginPage() ?>
<?php $this->beginBody() ?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="">
    <title></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= Yii::getAlias('@web') ?>/content/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= Yii::getAlias('@web') ?>/content/admin/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?= Yii::getAlias('@web') ?>/content/admin/css/colors/blue.css" id="theme" rel="stylesheet">

      <!-- Alertify -->
	<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/alertifyjs/css/alertify.min.css" />
	<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/alertifyjs/css/themes/bootstrap.css" />
	<script src="<?= Yii::getAlias('@web') ?>/content/alertifyjs/alertify.min.js"></script>
	<script>
  		alertify.set('notifier','position', 'top-right');
	</script>

</head>
<body>

<!-- begin alertify alerts -->

<?php if (Yii::$app->session->hasFlash('success')) { ?>
		<script>
			alertify.success('<?= Yii::$app->session->getFlash('success'); ?>');
		</script>
	<?php } ?>
  <?php if (Yii::$app->session->hasFlash('warning')) { ?>
    <script>alertify.warning('<?= Yii::$app->session->getFlash('warning'); ?>');</script>
  <?php } ?>
	<?php if (Yii::$app->session->hasFlash('error')) { ?>
		<script>alertify.error('<?= Yii::$app->session->getFlash('error'); ?>');</script>
	<?php } ?>
<!-- end alertify alerts -->


    <!-- <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div> -->
    
    <!-- Main wrapper - style you can find in pages.scss -->
    
    <section id="wrapper">
        <!-- <div class="login-register" style="background-image:url(../assets/images/background/login-register.jpg);">         -->
        <div class="login-register" style="">        
            <div class="login-box card">
                <div class="card-body">
                    

                    <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'InicioSesion', 
                        'options'=> [
                            'class' => 'form-horizontal form-material'
                        ], 
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <h3 class="box-title mb-3">Inicio de Sesion</h3>

                        <?= $form->field($model, 'Usuario', [
                            'template' => '<div class="form-group mb-0">
                                                <div class="col-xs-12">
                                                    {input}
                                                </div>
                                            </div>
                                            {error}{hint}',
                            'errorOptions'=>['class'=>'badge badge-danger']
                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Usuario'])
                        ->label(false) ?>
                        <?= $form->field($model, 'Clave', [
                            'template' => '<div class="form-group mb-0">
                                                <div class="col-xs-12">
                                                    {input}
                                                </div>
                                            </div>
                                            {error}{hint}',
                            'errorOptions'=>['class'=>'badge badge-danger']
                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Clave', 'type' => 'password'])
                        ->label(false) ?>

                        <div class="form-group text-center mt-3">
                            <div class="col-xs-12 text-center">
                                <?= Html::submitButton(Yii::t('app', 'Entrar'), ['class' => 'btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light']) ?>
                            </div>
                        </div>

                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        
    </section>



<?php $this->endBody() ?>
<?php $this->endPage() ?>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/bootstrap/js/popper.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/custom.min.js"></script>
    
    <!-- Style switcher -->
    
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/styleswitcher/jQuery.style.switcher.js"></script>



</body>
</html>

