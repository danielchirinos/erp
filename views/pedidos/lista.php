<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de Pedidos'; 
    $this->params['activeLink'] = "pedidos-lista";
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/responsive.bootstrap4.min.css">
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/responsive.bootstrap4.min.js"></script>
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/admin/bootstrap-select-1.13.15/css/bootstrap-select.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/admin/bootstrap-select-1.13.15/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/admin/bootstrap-select-1.13.15/js/i18n/defaults-es_CL.js"></script>

<style>
table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before {

    background-color: var(--verde);
}
</style>

<div class="container-fluid">
    <!-- Button trigger modal -->
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/admin/index">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
        
        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($pedidos) ?></span></span>
        </div>
    </div>
    
    <a href="<?= Yii::getAlias('@web') ?>/pedidos/compra">
        <button type="button" class="btn btn-success mb-3" id="crearPedido" style="font-size:18px;">
            <i class="fal fa-plus-circle"></i>
            Crear Pedido
        </button>
    </a>

    <div class="row card">
        <div class="card-body">
            <div class="row">
                <div class="col-2">
                    <div class="form-group w-100">
                        <label class="control-label" for="nro_pedido">Nro Pedido</label>
                        <input type="text" id="nro_pedido" placeholder="Número de Pedido" class="selectpicker form-control" name="nro_pedido">

                        <div class="help-block badge badge-danger"></div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="estado_select">Estado</label>
                        <select id="estado_select" class="selectpicker form-control"  name="estado_select">
                            <option value="0">Seleccionar</option>
                            <?php
                                if(isset($estadoPedidos)){
                                    foreach($estadoPedidos as $ep){
                                        ?>
                                            <option value="<?=$ep->id?>"><?=$ep->estado?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>

                        <div class="help-block badge badge-danger"></div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="tipo_select">Tipo</label>
                        <select id="tipo_select" class="selectpicker form-control" name="tipo_select">
                            <option value="0">Seleccionar</option>
                            <option value="1">WEB</option>
                            <option value="2">MANUAL</option>
                        </select>

                        <div class="help-block badge badge-danger"></div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="vendedor_select">Vendedor</label>
                        <select id="vendedor_select" class="selectpicker form-control"  name="vendedor_select">
                            <option value="0">Seleccionar</option>
                            <?php
                                if(isset($vendedores)){
                                    foreach($vendedores as $k => $v){
                                        ?>
                                            <option value="<?=$k?>"><?=$v?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>

                        <div class="help-block badge badge-danger"></div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="cliente_select">Cliente</label>
                        <select id="cliente_select" class="selectpicker form-control" name="cliente_select" >
                            <option value="0">Seleccionar</option>
                            <?php
                                if(isset($clientes)){
                                    foreach($clientes as $c){
                                        ?>
                                            <option value="<?= $c['id'] ?>"><?= $c["nombre"] . ' ' . $c['apellido'] ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>

                        <div class="help-block badge badge-danger"></div>
                    </div>
                </div>
                <div class="col-3">
                    <br>
                    <button type="button" onclick="buscar()" class="btn btn-success mt-1" id="crearPedido" style="font-size:18px;">
                        <i class="fal fa-search"></i>
                        Buscar
                    </button>
                </div>
            </div>
            
        </div>
    </div>
    <div class="row">
        <?php if (count($pedidos) == 0) { ?>
            <span>Sin pedidos</span>
        <?php }else { ?>

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="w-100" style="overflow-x:scroll">
                            <table id="example23" class="display table table-hover table-striped table-bordered nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Boleta PDF</th>
                                        <th>Nro Pedido</th>
                                        <th>Fecha Creación</th>
                                        <th>Fecha despacho</th>
                                        <th>Estado pedido</th>
                                        <th>Precio Total</th>
                                        <th>Vendedor</th>
                                        <th>Cliente</th>
                                        <th>Teléfono</th>
                                        <th>Tipo</th>
                                        <th>Dir. despacho</th>
                                        <th>Cant. de productos</th>
                                        <!-- <th>Detalle</th> -->
                                    </tr>
                                </thead>
                                <tbody id="tabla_pedidoscab">
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>
<!-- Modal -->




<script>
$(document).ready(function(){
    buscar();
});

function buscar(){
    $.ajax({
        type: "POST",
        url: "<?= Yii::getAlias('@web') ?>/pedidos/buscar",
        data: {
            nro_pedido: $("#nro_pedido").val(),
            estado: $("#estado_select").val(),
            tipo: $("#tipo_select").val(),
            vendedor: $("#vendedor_select").val(),
            cliente: $("#cliente_select").val()
        },
        success: function(res){
            $('#example23').DataTable().clear().destroy();
            $("#tabla_pedidoscab").html(res);
            construirTabla();
        }
    });
}

function construirTabla(){
    $('#example23').DataTable({
        dom: 'lBfrtip',
        "lengthMenu": [50, 100, 150, 500],
        "pageLength": 50,
        "language": {
                "url": "<?= Yii::getAlias('@web'); ?>/content/datatable/Spanish.json"
            },
        "order": [[ 0, "desc" ]],
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    });
    
    // agregar para poner tooltips de boostrap 4 en las tablas
    tabla.on( 'draw', function () {
        $('[data-toggle="tooltip"]').tooltip();
    } );
}

function cambiarEstado(id){
    alertify.confirm(`Cambien de estado`, '¿Desea cambiar el estado a entregado?',
    function(){ 
        $.ajax({
            type: "POST",
            url: "<?= Yii::getAlias('@web') ?>/pedidos/cambiarestadopedido",
            data: {id: id},
            success: function (){
                alertify.success("Se actualizó el pedido a entregado");
                buscar();
            }
        });
    }, 
    function(){ 
        alertify.error('Accion cancelada por cliente')
    });
};
</script>

