<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de Pedidos con otros medios de pago'; 
    $this->params['activeLink'] = "pedidos-otros-pagos";
?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/responsive.bootstrap4.min.css">

<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/responsive.bootstrap4.min.js"></script>


<!-- lightbox -->
<link href="<?= Yii::getAlias('@web') ?>/content/lightbox2-2.11.3/css/lightbox.css" rel="stylesheet" />
<script src="<?= Yii::getAlias('@web') ?>/content/lightbox2-2.11.3/js/lightbox.js"></script>

<style>
table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before {

    background-color: var(--verde);
}
</style>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($otrosMedios) ?></span></span>
        </div>
    </div>

    

    <div class="row">
        <?php if (count($otrosMedios) == 0) { ?>
            <span>Sin pedidos</span>
        <?php }else { ?>
        
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Lista de Pedidos con otros medios de pago</h4>
                        <div class="w-100" style="overflow-x:scroll">
                            <table id="example23" class="display table table-hover table-striped table-bordered nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nro Pedido</th>
                                        <th>Referencia</th>
                                        <th>Medio pago</th>
                                        <th>Cliente</th>
                                        <th>Dir. despacho</th>
                                        <th>Cant. de productos</th>
                                        <th>Precio Total</th>
                                        <th>Fecha despacho</th>
                                        <th>Estado pedido</th>
                                        <th>Imagen</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($otrosMedios as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value["id"] ?></td>
                                        <td><?= $value["orden_compra"] ?></td>
                                        <td><?= $value["medio_pago"] ?></td>
                                        <td><?= $value["cliente"]?></td>
                                        <td >
                                            <span data-toggle="tooltip"title="<?= $value["direccion_despacho"] ?>">
                                                <?= strlen($value["direccion_despacho"])  > 15 ? substr($value["direccion_despacho"], 0, 12)."..." : $value["direccion_despacho"] ?>
                                            </span>
                                            
                                        </td>
                                        <td><?= $value["cantidad_productos"] ?></td>
                                        <td><?= $value["precio_total"] ?></td>
                                        <td><?= $value["fecha_despacho"] ?></td>
                                        <td>
                                            <?php 
                                            $estado = "";
                                            switch ($value["id_estadopedido"]) {
                                                case 1://pagado
                                                    $estado = "primary";
                                                    break;
                                                case 2://entregado
                                                    $estado = "success";
                                                    break;
                                                case 3://rechazados
                                                    $estado = "warning";
                                                    break;
                                                case 4://confirmados
                                                    $estado = "info";
                                                    break;
                                                
                                            } ?>
                                            <?php if ($value["id_estadopedido"] == 1 || $value["id_estadopedido"] == 4) { ?>
                                                <span class='badge badge-<?=$estado?>' style='cursor:pointer' onclick='cambiarEstado(<?=$value["id"]?>, <?= $value["id_estadopedido"] ?>)'><?= $value["estado"] ?></span>
                                            <?php }else{ ?>
                                                <span class='badge badge-<?= $estado ?>' style='cursor:default'><?= $value["estado"] ?></span>
                                            <?php } ?>
                                            
                                        </td>

                                        <td>
                                            <a href="<?= Yii::getAlias('@web') ."/images/transferencias/". $value["imagen"] ?>" data-lightbox="<?= $value["id"]."-". $value["orden_compra"] ?>" data-title="<?= $value["id"]."-".$value["orden_compra"] ?>">
                                                <img src="<?= Yii::getAlias('@web') ."/images/transferencias/". $value["imagen"] ?>" alt="" width="50">
                                            </a>
                                            
                                        </td>


                                        <td class="text-center">
                                            <a class="btn btn-secondary btn-sm" href="<?= Yii::getAlias('@web'); ?>/pedidos/detalle/<?= $value["id"] ?>"><i class="fas fa-bars"></i></a>
                                                
                                            <?php if($value["id_estadopedido"] == 1) {?>
                                                <a class="btn btn-success btn-sm" href="<?= Yii::getAlias('@web'); ?>/pedidos/enviarcorreo/<?= $value["id"] ?>"><i class="fas fa-envelope"></i></a>
                                            <?php }?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>



<script>
$(document).ready(function(){
    tabla = $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
                "url": "<?= Yii::getAlias('@web'); ?>/content/datatable/Spanish.json"
            },
        "order": [[ 0, "desc" ]]
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    });
    
    // agregar para poner tooltips de boostrap 4 en las tablas
    tabla.on( 'draw', function () {
        $('[data-toggle="tooltip"]').tooltip();
    } );


    
});

function cambiarEstado(id, estado){
    titulo = estado == 4 ? "Pagado" : "Entregado"
    alertify.confirm(`Cambien de estado`, `¿Desea cambiar el estado a ${titulo}?`,
    function(){ 
        $.ajax({
            type: "POST",
            url: "<?= Yii::getAlias('@web') ?>/pedidos/cambiarestadopedido",
            dataType: "html",
            data: {id: id},
        });
    }, 
    function(){ 
        alertify.error('Accion cancelada por cliente')
    });
}
</script>

