<?php

$db = Yii::$app->db;
date_default_timezone_set('America/Santiago');
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-    rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/stylePdf.css">
</head>
<body class="w-100">
    <div class="text-center">
        <img src="<?= Yii::getAlias('@web') ?>/images/tienda.png" alt="" style="width:50px;">
        <h4 class="text-center" style="font-size: 12pt;">PUNTO DE VENTA POS</h4>
    </div>
    <div style="margin-top: 10px;font-size: 10pt;">
        <span>
            <b>BOLETA ELECTRÓNICA #<?= $factura_numero ?></b>
        </span>
        <br>
        <?php $date = date_create($productos_cab->fecha_creacion)?>
        FECHA DE EMISIÓN: <?= date_format($date, 'd/m/Y') ?>
        <br>
        HORA: <?= date('H:i:s')?>
    </div>
    <div style="margin-top: 10px;font-size: 10pt;">
        <span>
            <b>VENDEDOR: <?= strtoupper(substr($vendedor, 0, 20)) ?></b>
        </span>
        <br>
        RUT CLIENTE: <?= $rut_cliente ?>
        <br>
        NOMBRE CLIENTE: <?= strtoupper(substr($nombre_cliente, 0, 20)) ?>
    </div>
    <table style="margin-top: 30px;padding-bottom:10px;font-size: 9pt;border-bottom:2px solid black;width:100%;">
        <thead>
            <tr style="border-bottom: 1px solid black;">
                <th>DESCRIPCIÓN</th>
                <th>CANTIDAD</th>
                <th>VALOR</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $subtotal = 0;
                foreach($productos_vendidos as $pv):
                    $subtotal += ($pv->cantidad * $pv->precio);
            ?>
                <tr>
                    <td><?= $pv->producto->descripcion ?></td>
                    <td><?= $pv->cantidad ?></td>
                    <td><?= ($pv->cantidad * $pv->precio) ?></td>
                </tr>
            <?php
                endforeach;
            ?>
        </tbody>
    </table>
    <table style="display:inline-block;width:100%;margin-top:10px;">
        <tr >
            <td style="display:inline-block;">
                <div>
                    <p style="text-align:left;font-size:16px;"><b>Valor neto: </b></p>
                    <p style="text-align:left;font-size:16px;"><b>Subtotal:</b> </p>
                    <p style="text-align:left;font-size:16px;"><b>IVA (19%):</b> </p>
                    <p style="text-align:left;font-size:16px;"><b>Total: </b></p>
                </div>
            </td>
            <td style="display:inline-block;text-align:right;">
                <p style="text-align:right;font-size:16px;"><?= $subtotal ?></p>
                <p style="text-align:right;font-size:16px;"><?= $subtotal ?></p>
                <p style="text-align:right;font-size:16px;"><?= round($subtotal * 0.19) ?></p>
                <p style="text-align:right;font-size:16px;"><?= $productos_cab->precio_total ?></p>
            </td>
        </tr>
    </table>
    <div style="margin-top:50px;text-align:center;"><b>RECIBÍ CONFORME MERCADERÍA Y/O SERVICIOS</b></div>
</body>
</html>