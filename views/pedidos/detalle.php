<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Detalle de Pedido'; 
    $this->params['activeLink'] = "pedidos-detalle";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/admin/index">Inicio</a></li>
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/pedidos/lista">Lista de pedidos</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Productos en este pedido: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($pedidoslin) ?></span></span>
        </div>
    </div>

    <div class="row">
        <?php if (count($pedidoslin) == 0) { ?>
            <div class="col-12">
                <span>Nada que mostrar</span>
            </div>
        <?php }else { ?>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Lista de Pedidos</h4>
                            <span><b>Estado del pedido: </b>
                                <?= $pedidoscab[0]["id_estadopedido"] == 1 ? "<span class='badge badge-info'>".$pedidoscab[0]['estadopedido']['estado']."</span>" : "<span class='badge badge-success'>".$pedidoscab[0]['estadopedido']['estado']."</span>" ?>
                            </span>
                        </div>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Imagen</th>
                                        <th>Descripción</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <th>Precio Oferta</th>
                                        <th>Precio Total</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td style="border: none;"></td>
                                        <td style="border: none;"></td>
                                        <td class="text-right"><b>Cantidad total:</b> <?= $pedidoscab[0]["cantidad_productos"] ?></td>
                                        <td class="text-right"><b>Costo de envio:</b> <?= $pedidoscab[0]["comuna"]["precio"] ?> $</td>
                                        <td class="text-right"><b>Pecio total:</b> <?= $pedidoscab[0]["precio_total"] ?> $</td>
                                        <td class="text-right"><b>Total precio y envio:</b> <?= $pedidoscab[0]["comuna"]["precio"] + $pedidoscab[0]["precio_total"] ?> $</td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <?php foreach ($pedidoslin as $key => $value) { ?>
                                    <tr>
                                        <td class="text-center">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt="" class="img-fluid">
                                        </td>
                                        <td><?= $value->producto->descripcion ?></td>
                                        <td><?= $value->cantidad ?></td>
                                        <td><?= $value->producto->precio ?></td>
                                        <td><?= ($value->producto->es_oferta == 1) ? $value->producto->precio_oferta : 0 ?></td>
                                        <td><?= $value->precio  ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>



<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    });
});
</script>