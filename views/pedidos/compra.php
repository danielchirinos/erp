<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Crear Pedidos'; 
    $this->params['activeLink'] = "pedidos-crear";
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/responsive.bootstrap4.min.css">
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datetimepicker/moment.min.js"></script>
<style>
table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before {

    background-color: var(--verde);
}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="w-100">
                        <h3 class="mb-2">Fecha y Hora actual: <b id="b_moment"></b></h3>
                        <div class="row">
                            <div class="col-3">
                                <form id="form_lector">
                                    <label for="form_input_lector">Código</label>
                                    <br>
                                    <input class="form-control" type="text" id="form_input_lector" name="form_input_lector"  placeholder="Código">
                                </form>
                            </div>
                            <div class="col-3">
                                <label>RUT - Cliente</label>
                                <br>
                                <input class="form-control" type="text" id="form_rut_cliente" name="form_rut_cliente" placeholder="RUT - Cliente">
                            </div>
                            <div class="col-3">
                                <label>Nombre</label>
                                <br>
                                <input class="form-control" type="text" id="form_nombre_cliente" name="form_nombre_cliente" placeholder="Nombre">
                            </div>
                            <div class="col-3">
                                <label>Apellido</label>
                                <br>
                                <input class="form-control" type="text" id="form_apellido_cliente" name="form_apellido_cliente" placeholder="Apellido">
                            </div>
                        </div>
                        
                        <table class="table table-bordered mt-3">
                            <thead>
                                <tr>
                                <th scope="col">SKU</th>
                                <th scope="col">Producto</th>
                                <th scope="col">Imagen</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Precio Und.</th>
                                <th scope="col">Total</th>
                                </tr>
                            </thead>
                            <tbody id="table_body">
                            </tbody>
                        </table>
                        
                        <div>
                            <h3>SubTotal: <b id="subtotal"></b></h3>
                            <h3>IVA: <b id="iva"></b></h3>
                            <h3>Total: <b id="total"></b></h3>
                        </div>
                        <div style="text-align:right;">
                            <button class="btn btn-success btn-lg" type="button" id="button_click_crear" onClick="generarPedido()">Generar Pedido</button>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Modal -->

<script src="<?= Yii::getAlias('@web'); ?>/content/jsrut/jquery.rut.js"></script>

<script>
$(document).ready(function(){
    tabla = $('#example23').DataTable({
        dom: 'lBfrtip',
        "lengthMenu": [50, 100, 150, 500],
        "pageLength": 50,
        "language": {
                "url": "<?= Yii::getAlias('@web'); ?>/content/datatable/Spanish.json"
            },
            "scrollX": true,
        "order": [[ 0, "desc" ]]
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    });
    
    // agregar para poner tooltips de boostrap 4 en las tablas
    tabla.on('draw', function () {
        $('[data-toggle="tooltip"]').tooltip();
    } );
    
    $("input[name='form_input_lector']").select();
    
    $("#form_rut_cliente").rut({formatOn: 'keyup'});
});

setInterval(() => {
    momentos = moment().format("DD/MM/YYYY HH:mm:ss");
    $("#b_moment").html(momentos);
}, 1000)
ArrayCompleto = [];

$("#form_lector").submit((evento) => {
    evento.preventDefault();
    
    $.ajax({
        type: "POST",
        url: "<?= Yii::getAlias('@web') ?>/pedidos/venta",
        data: {
            sku: evento.target[0].value
        },
        success: function (response) {
            if(response){
                find = ArrayCompleto.find(x => x.sku === response.sku);
                if(find){
                    find.cantidadProductos = find.cantidadProductos + 1;
                    find.precioTotal = find.precioTotal + response.precio_venta;
                    $("#cantidadProductos_sku_" + find.sku).html(find.cantidadProductos);
                    $("#precioTotal_sku_" + find.sku).html(`
                        <b>
                            ${new Intl.NumberFormat('es-CL').format(find.precioTotal)}
                        </b>
                    `);
                }else{
                    objeto = {
                        sku: response.sku,
                        cantidadProductos: 1,
                        precioTotal: response.precio_venta,
                        precioUnitario: response.precio_venta,
                    }
                    ArrayCompleto.push(objeto)
                    $("#table_body").append(`
                        <tr id="sku_${response.sku}">
                            <th>${response.sku}</th>
                            <td>${response.descripcion}</td>
                            <td>
                                <img src="<?= Yii::getAlias('@web') ?>/images/${response.imagen == null ? 'productos/producto.jpg' : 'productos/' + response.imagen}" width="100">
                            </td>
                            <td id="cantidadProductos_sku_${response.sku}">${objeto.cantidadProductos}</td>
                            <td>
                                <b>
                                    ${new Intl.NumberFormat('es-CL').format(response.precio_venta)}
                                </b>
                            </td>
                            <td id="precioTotal_sku_${response.sku}">
                                <b>
                                    ${new Intl.NumberFormat('es-CL').format(response.precio_venta)}
                                </b>
                            </td>
                            <td>
                                <button class="btn btn-danger" onClick="quitarProducto('${response.sku}')">
                                    Quitar
                                </button>
                            </td>
                        </tr>
                    `);
                }

                if(ArrayCompleto.length > 0){
                    suma = 0;
                    $.each(ArrayCompleto, function (k, v) { 
                        suma += parseInt(v.precioTotal);
                    });

                    $("#subtotal").text(new Intl.NumberFormat('es-CL').format(parseInt(suma)));
                    
                   porcentajeIva = 19; 

                    iva =  parseInt(suma) * (porcentajeIva / 100);

                    $("#iva").text(new Intl.NumberFormat('es-CL').format(parseInt(iva)));

                    precioTotal = parseInt(suma) + parseInt(iva);

                    $("#total").text(new Intl.NumberFormat('es-CL').format(parseInt(precioTotal)));
                }
            }
        }
    }).always(function() {
        /* Seleccionamos el texto para que se pueda sobreescribir por la siguiente lectura */
        $("input[name='form_input_lector']").select();
    });
});

function generarPedido(){
    if(ArrayCompleto.length <= 0){
        alertify.warning('No se puede crear un pedido sin ningún producto a vender')
    }else{
        if($("#form_rut_cliente").val() != ""){
            rut = $("#form_rut_cliente").val()
            if (checkRut(rut)) {
                $.ajax({
                    type: "POST",
                    url: "<?= Yii::getAlias('@web') ?>/pedidos/vender",
                    data: {
                        ArrayCompleto: ArrayCompleto,
                        subtotal: parseInt($("#subtotal").text().replaceAll(".", "")),
                        total: parseInt($("#total").text().replaceAll(".", "")),
                        iva: parseInt($("#iva").text().replaceAll(".", "")),
                        rut: $("#form_rut_cliente").val(),
                        nombre: $("#form_nombre_cliente").val(),
                        apellido: $("#form_apellido_cliente").val()
                    },
                    success: function (response) {
                        if(response == "nombre" || response == "apellido"){
                            alertify.warning('Cliente no encontrado por RUT, ingrese Nombre y Apellido para registrar')
                        }

                        if(response == "success"){
                            $("#button_click_crear").addClass("d-none");
                            alertify.success('Pedido creado exitosamente');
                            setTimeout(() => {
                                window.location.href = "<?= Yii::getAlias('@web')?>/pedidos/lista";
                            }, 5000)
                        }
                    }
                });
            } else {
                alertify.notify('El rut es invalido', "warning");
            }
        }else{
            alertify.warning('Agregue un RUT del Cliente')
        }
    }
}

function quitarProducto(sku){
    find = ArrayCompleto.find(x => x.sku === sku);
    if(find){
        find.cantidadProductos = find.cantidadProductos - 1;
        find.precioTotal = find.precioTotal - find.precioUnitario;
        if(find.cantidadProductos == 0){
            $("#sku_"+find.sku).remove();
            ArrayCompleto.splice(ArrayCompleto.indexOf(find), 1);
        }else{
            $("#cantidadProductos_sku_" + find.sku).html(find.cantidadProductos);
            $("#precioTotal_sku_" + find.sku).html(`
                <b>
                    ${new Intl.NumberFormat('es-CL').format(find.precioTotal)}
                </b>
            `);
        }
    }

    if(ArrayCompleto.length > 0){
        suma = 0;
        $.each(ArrayCompleto, function (k, v) { 
            suma += parseInt(v.precioTotal);
        });

        $("#subtotal").text(new Intl.NumberFormat('es-CL').format(parseInt(suma)));
        
        porcentajeIva = 19; 

        iva =  parseInt(suma) * (porcentajeIva / 100);

        $("#iva").text(new Intl.NumberFormat('es-CL').format(parseInt(iva)));

        precioTotal = parseInt(suma) + parseInt(iva);

        $("#total").text(new Intl.NumberFormat('es-CL').format(parseInt(precioTotal)));
    }else{
        $("#iva").text('');
        $("#total").text('');
        $("#subtotal").text('');
    }
    
    $("input[name='form_input_lector']").select();
}

function checkRut(rut) {

    // Despejar Puntos
    var valor = rut.replaceAll('.', '');
    // Despejar Guión
    valor = valor.replaceAll('-', '');

    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();

    // Formatear RUN
    rut = cuerpo + '-' + dv

    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if (cuerpo.length < 7) {
        // rut.setCustomValidity("RUT Incompleto"); 
        return false;
    }

    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;

    // Para cada dígito del Cuerpo
    for (i = 1; i <= cuerpo.length; i++) {

        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if (multiplo < 7) {
            multiplo = multiplo + 1;
        } else {
            multiplo = 2;
        }

    }

    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;

    // Validar que el Cuerpo coincide con su Dígito Verificador
    if (dvEsperado != dv) {
        // rut.setCustomValidity("RUT Inválido"); 
        return false;
    } else {
        return true
    }

    // Si todo sale bien, eliminar errores (decretar que es válido)
    // rut.setCustomValidity('');
}

</script>

