<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $this->params['activeLink'] = ($tipo==null) ? "Productos" : $tipo;
$this->params['campo'] = isset($campo) ? $campo : "";


?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/rangeSlider/ion.rangeSlider.css"/>
<script src="<?= Yii::getAlias('@web') ?>/content/rangeSlider/ion.rangeSlider.js"></script>


<!-- productos -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">
        <div class="inner-sec-shop px-lg-4 px-3">

        <?php  if ($productos != null) { 

                if (count($productos) > 0) { ?>
                    <h3 class="tittle-mbgreen my-lg-4 mt-3">
                        <?= $tipo==null ? "Sin productos para mostrar" : ($tipo > 4 ) ? "Sin productos para mostrar" : $tipo ?>
                    </h3>
                <?php } ?>
                <?php if ($tipo != null || $tipo > 3) { ?>

                    <?php if (count($productos) > 0) { ?>
                
                
                        <div class="row">
                            <!-- product left -->
                            <!-- col-lg-3 -->
                            <div class="d-none pt-2">

                                <?php $form = ActiveForm::begin(['method' => 'post', 'id'=> 'formularioProductos','options'=> [],  ]); ?>
                                    <!-- price range -->
                                    <div class="range">
                                        <h3 class="agileits-sear-head">Rango de precio <span id="state"></span></h3>
                                        <!-- <input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> -->
                                        <input type="text" class="js-range-slider" name="rango_precio" value="" />
                                        <input type="hidden" name="txt_buscar" value="<?= isset($campo) ? $campo : "" ?>" />

                                        <br>   
                                    </div>

                                    <br>
                                    <div>
                                        <?= Html::submitButton(Yii::t('app', 'Filtrar por precios'), ['class' => 'btn btn-verde col-12' ]) ?>
                                    </div>
                                <?php $form->end(); ?>

                            </div>


                            <div class="left-ads-display col-lg-12">
                                <div class="row">


                                    <?php foreach ($productos as $key => $value) {  ?>

                                    
                                        <div class="col-md-4 col-lg-3 col-xl-2 women_two shop-gd py-2 d-flex">
                                            <div class="product-googles-info googles">
                                                <div class="men-pro-item">
                                                    <div class="men-thumb-item">
                                                        <?php if ($value->imagen == null) { ?>
                                                            <img src="<?= Yii::getAlias('@web') ?>/images/productos/producto.jpg" class="img-fluid" alt="">
                                                        <?php }else{ ?>
                                                            <img src="<?= Yii::getAlias('@web') .'/images/thum/'.$value->imagen?>" class="img-fluid" alt="">
                                                        <?php  } ?>

                                                        <?php if ($value->es_oferta == 1) { ?>
                                                            <span class="product-new-top">Oferta</span>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="item-info-product">
                                                        <div class="info-product-price">
                                                            <div class="grid_meta">
                                                                <div class="product_price">
                                                                    <h4>
                                                                        <a href="#"><?= $value->descripcion ?></a>
                                                                    </h4>
                                                                    


                                                                    <?php if ($value->es_oferta == 1) { ?>
                                                                        <del class="precioOferta"><span class="money "><?= $value->precio ?></span></del>
                                                                        <br>
                                                                        <div class="grid-price mt-2">
                                                                            <span class="money "><?= $value->precio_oferta ?></span>
                                                                            <?php if ($value->es_unidad == 1) { ?>

                                                                            <span class="badge badge-light ">Por Unidad</span>

                                                                            <?php }else{ ?>

                                                                            <span class="badge badge-light ">Por Kg</span>

                                                                            <?php } ?>
                                                                        </div>
                                                                        
                                                                    <?php }else{ ?>
                                                                        <div class="grid-price mt-2">
                                                                            <span class="money "><?= $value->precio ?></span>
                                                                            <?php if ($value->es_unidad == 1) { ?>

                                                                                    <span class="badge badge-light ">Por Unidad</span>

                                                                            <?php }else{ ?>

                                                                                    <span class="badge badge-light ">Por Kg</span>
                                  
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>

                                                                </div>
                                                            </div>
                                                            <div id="proudcto-no-hover" class="googles single-item hvr-outline-out " style="background-color: var(--verdeClaro);padding: .2em 0;">
                                                                    <input type="hidden" id="txt_id<?= $value->id ?>" value="<?= $value->id ?>">
                                                                    <input type="hidden" id="txt_precio<?= $value->id ?>" value="<?= $value->es_oferta == 1 ?$value->precio_oferta : $value->precio ?>">
                                                                    <input type="hidden" id="txt_nombre<?= $value->id ?>" value="<?= $value->descripcion ?>">
                                                                    <?php $rango =  ($value->rango==null) ? '1' : str_replace(",",".",$value->rango)  ?>
                                                                    <?php $rango = ($value->es_unidad==1) ? intval($rango) : $rango ?>

                                                                <div class="input-group d-flex justify-content-center">
                                                                    <div class="input-group-prepend">
                                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 0)">-</button>
                                                                    </div>
                                                                    <input id="txt_cant<?= $value->id ?>" class="" style=" width: 40%;" type="text" value="<?= $rango  ?>" step="<?= $rango  ?>" min="<?= $rango  ?>" readonly>
                                                                    
                                                                    <div class="input-group-append">
                                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 1)">+</button>

                                                                        <button id="btn_add<?= $value->id ?>" style="color: var(--verde);" type="button" class="googles-cart pgoogles-cart m-0 cart-xl " onclick="guardarEnCarro(<?= $value->id ?>, <?= $value->es_oferta ?>, <?= $rango ?>, <?= $value->es_unidad ?>)">
                                                                            <i class="fas fa-cart-plus"></i>
                                                                        </button>
                                                                        
                                                                    </div>
                                                                </div>

                                                                <button id="btn_add<?= $value->id ?>" style="color: var(--verde);" type="button" class="googles-cart pgoogles-cart m-0 cart-sm-md-lg " onclick="guardarEnCarro(<?= $value->id ?>, <?= $value->es_oferta ?>, <?= $rango ?>, <?= $value->es_unidad ?>)">
                                                                            <i class="fas fa-cart-plus"></i>
                                                                        </button>

                                                                

                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                        </div>

                    <?php }else { ?>

                        <h3 class="tittle-mbgreen my-lg-4 mt-3">Sin Productos para mostrar</h2>

                    <?php } ?>
                <?php } ?>
            <?php }else{ ?>
                <h3 class="tittle-mbgreen my-lg-4 mt-3">Sin Productos para mostrar</h2>
            <?php } ?>
        </div>
    </div>
</section>
<!-- end productos -->

<script>
        $(".js-range-slider").ionRangeSlider({
            type: "double",
            min: 0,
            max: 10000,
            from: <?= $minimo ?>,
            to: <?= $maximo ?>,
            grid: false
        });
        </script>

