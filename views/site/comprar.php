<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'MB Green';
$this->params['activeLink'] = "comprar";

$total = 0;
?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/compra.css">
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<section class="banner-bottom-wthreelayouts py-lg-5 py-3">
	<div class="px-4">
		<?php if (count($compra)>0) {  ?>
			<div class="row">
		
				<div class="col-md-12 col-lg-6">
					<div class="inner-sec-shop px-lg-5 px-md-0 px-sm-0 px-2 sticky-top">
						<h3 class="tittle-w3layouts my-lg-4 mt-3">Detalle de compra </h3>
						<div class="checkout-right">
							<h4>Su compra contiene:
								<span><?= count($compra) == 1 ? count($compra). " Producto" : count($compra). " Productos" ?> </span>
							</h4>
							<table class="table table-responsive">
								<thead>
									<tr>
										<th>Producto</th>
										<th>Cantidad</th>
										<th>Descripción</th>
										<th>Precio total</th>
										<th>Eliminar</th>
									</tr>
								</thead>
								<tbody>
								<?php  foreach ($compra as $key => $value) { ?>
								
									<tr class="rem1">
										<td class="invert">
											<a href="#">
												<img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "/productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt="" class="img-responsive">
											</a>
										</td>
										<td class="invert">
											<div class="quantity">
												<div class="quantity-select">

													<div class="input-group d-flex justify-content-center">

														<div class="input-group-prepend">
															<a class="btn btn-sm btn-outline-secondary btn-menos h-100 p-2" onclick="cambiar('<?=$value->id_producto ?>', '<?= $value->producto->es_oferta ?>', '<?= $value->cantidad_productos ?>', '<?=$value->producto->rango ?>', 0 )">-</a>

														</div>

														<input type="hidden" id="txt_id_hidden" value="<?=$value->id_producto ?>">
														<input class="input_cantidad_productos text-center" id="input_cantidad_productos" type="text" readonly="true" value="<?=$value->cantidad_productos ?>" step="<?=$value->producto->rango ?>" min="<?=$value->producto->rango ?>">


														<div class="input-group-append">
															<button class="btn btn-sm btn-outline-secondary btn-mas h-100 p-2" type="button" onclick="cambiar('<?=$value->id_producto ?>', '<?= $value->producto->es_oferta ?>', '<?= $value->cantidad_productos ?>', '<?=$value->producto->rango ?>', 1 )">+</button>
														</div>

													</div>
												</div>
											</div>
										</td>
										<td class="invert"><?=$value->producto->descripcion ?> </td>

										<td class="invert"><?= $value->precio_total ?> $</td>
										<td class="invert" style="width: 5%;">
											<i class="fas fa-times icon-eliminar" onclick="eliminarProductoo(<?=$value->id_producto ?>)"></i>
										</td>
									</tr>

								<?php } ?>

								</tbody>
							</table>
						</div>
						

					</div>
				</div>

				<div class="col-md-12 col-lg-6 ">
                    <div class="px-lg-0 px-md-0 px-sm-0 px-4">
                        <h3 class="tittle-w3layouts my-lg-4 mt-3 pb-3">Datos de envio </h3>
                        <?php $form = ActiveForm::begin([
                            'method' => 'post', 
                            'id'=> 'confirmarPedidoForm', 
							'action' => Url::to(['site/confirmacionpedido']),
                            'options'=> [
                                'class' => 'form-horizontal',
                            ], 
                            'enableClientValidation' => false,
                            'enableAjaxValidation' => true, 
                            ]); ?>


							<!-- id del pedido hidden para recuperar un pedido -->
							<input type="hidden" value="<?= isset($pedidoCabId) ? $pedidoCabId : null  ?>" name="pedidoCabId">
							<input type="hidden" id="cuponId" value="" name="cuponId">

                            <div class="row">
                                <div class="col-md-9 col-sm-12">
                                    <?= $form->field($model, 'direccion_despacho', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                            ])->textInput(['class'=>'form-control', 'placeholder' => 'Dirección de depacho', 'value' => $datosCliente->direccion_despacho]) ?>
                                </div>

								<div class="col-md-3 col-sm-12">
                                    <?= $form->field($model, 'nro_casa', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                            ])->textInput(['class'=>'form-control', 'placeholder' => 'Dpto/Casa', 'value' => $datosCliente->nro_casa]) ?>
                                </div>
		
                            </div>

							<div class="row">
                                

								<div class="col-md-6">
                                    <?= $form->field($model, 'comuna', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->dropDownList($comunas, ['prompt'=>'Seleccione', 'class' => 'form-control', "value" => isset($comunaElegidaCliente) ? $comunaElegidaCliente : ""]) ?>
                                </div>
								<div class="col-md-6">
                                    <?= $form->field($model, 'fechaDespacho', ['template' => '{label}
                                    <div class="form-group mb-0">
                                        <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                            {input}
                                            <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    {error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                    ])->textInput(['class'=>'form-control', 'placeholder' => 'dia / mes / año', 'readonly'=>'true']) ?>
                                </div>

							</div>

							<div class="checkout-left row justify-content-end">
								<div class="col-md-12 checkout-left-basket">
									<h3>Cupon promocional</h3>
									<hr>

									<ul >

										<li >Ingresa aquí tu cupón
											<span> 
											<div class="input-group mb-3">
												<input type="text" class="form-control" id="txt_cupon_promocional" aria-describedby="button-addon2">
												<div class="input-group-append">
													<button class="btn btn-outline-secondary" type="button" id="button-addon2" onclick="agregarCupon()">Validar</button>
												</div>
											</div>
											</span>
										</li>
										<br>

									</ul>
								</div>

								<div class="clearfix"> </div>

							</div>


							<div class="checkout-left row justify-content-end">
								<div class="col-md-12 checkout-left-basket">
									<h3>Resumen del pedido</h3>
									<hr>

									<ul>
										<?php  
											$total = 0;
											foreach ($compra as $key => $value) { ?>
											<li><?= $value->producto->descripcion ?>
												<span><?= $value->precio_total ?> $ </span>
											</li>
										<?php $total = $total + $value->precio_total; } ?>
										
										<hr>
										<li id="comuna">Sin Comuna Seleccionada: <span id="comunaPrecio"> 0 $</span></li>

										<li id="datosCupon" style="display:none">Descuento
											<span class="font-weight-bold"> </span>
											<button id="eliminarCupon" type="button" class="btn btn-small float-right pt-0">
												<i class="fas fa-trash text-danger"></i>
											</button>
										</li>

										<li><b>Total Pedido:<span id="totalPedido"><?= $total ?> $</span> </b></li>
										<hr>
										<h5 class="float-right">Total a pagar: <b id="totalConEnvio"></b> $</h5>

									</ul>
								</div>

								<div class="clearfix"> </div>

							</div>

							<hr>
						    <div class="row text-right mt-2">
								<div class="col-md-12">
									<a class="btn btn-primary mb-3 mt-md-4 mt-0 w-100" href="<?= Yii::getAlias('@web') ?>/site/index">Agregar mas productos <i class="fas fa-plus"></i></a>
								
								</div>

								
							</div>

							<div class="row mt-2 d-flex align-items-center">

								

							</div>

							
							<div class="row text-center mt-2 d-flex align-items-center">
								<div class="col-md-12">
									<h4>Elige un metodo de pago</h4>
								</div>
							</div>
							<div class="row text-center mt-2 ">
								<div class="col-md-4 col-sm-6">
									<label>
										<input type="radio" id="radio_metodo_pago" name="radio_metodo_pago" value="1" checked >
										<img src="<?= Yii::getAlias('@web') ?>/images/pagos-webpay-plus.png" class="img-fluid rounded-pill">
									</label>
								</div>


								<div class="col-md-4 col-sm-6">
									<label>
										<input type="radio" id="radio_metodo_pago" name="radio_metodo_pago" value="2" 	>
										<img src="<?= Yii::getAlias('@web') ?>/images/hites-pay.png" class="img-fluid rounded-pill">
									</label>
								</div>
								
								
								<div class="col-md-4 col-sm-6">
									<label>
										<input type="radio" id="radio_metodo_pago" name="radio_metodo_pago" value="3" 	>
										<img src="<?= Yii::getAlias('@web') ?>/images/otros-medios-pago.png" class="img-fluid rounded-pill">
									</label>
								</div>

								<div class="col-md-6 col-12 text-center d-none">
									<img src="<?= Yii::getAlias('@web') ?>/images/pagos-webpay-plus.png" width="200" alt="">
								</div>
								
								
							</div>

							<div class="row text-right mt-2">
								<div class="col-md-12 mb-0">
									<div class="form-group form-check">
										<input type="checkbox" class="form-check-input" id="check_condiciones">
										<label class="form-check-label" for="check_condiciones">
											He leido y acepto los términos de compras y las <a class="text-verde" href="<?= Yii::getAlias('@web') ?>/site/terminosycondiciones" target="_blank">Condiciones de reserva</a>
										</label>
									</div>
                                </div>

							</div>
							
							<div class="row text-right mt-2 d-flex align-items-center">
								<div class="col-md-12 col-12 mt-2 mt-sm-0">
										<?= Html::submitButton(Yii::t('app', 'Ir a pagar el pedido <i class="fas fa-credit-card"></i>'), ['class' => 'btn btn-verde waves-effect waves-light col-12', "id" => "btn_pagar", 'disabled' => 'disabled']) ?>
								</div>
							</div>


                        <?php $form->end(); ?>


																						  
											             
                    </div>
				</div>
			
			</div>
		<?php }else{ ?>
			<div class="col-md-12 text-center">
				<p class="pb-2" style="font-size:2em">Sin articulos para continuar con la compra</p>
			
					<a class="btn btn-primary mb-3 w-25" href="<?= Yii::getAlias('@web') ?>/site/index">Volver al inicio <i class="fas fa-home"></i></a>

			</div>
		<?php }  ?>
		

	</div>

	<div class="modal fade" id="modalRut" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Actualización de rut</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-12 mb-3">
						<label for="">Ingrese su Rut (Sin puntos y con guión. Ejm: 12345678-9)</label>
						<input type="text" class="form-control" id="txt_actualizacion_rut" value="">
					</div>

					<div class="col-12">
						<label for="">¿Por qué pedimos este dato ahora?</label>
						<p><b>Solicitamos este dato para poder enviar tu boleta electrónica por email.</b></p>
						<p>Actualiza tu rut para continuar con la compra.</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="actualizarRut()">Actualizar Rut</button>
			</div>
			</div>
		</div>
	</div>
</section>




	<script>
		$("#input_cantidad_productos").inputmask("[9]{1,4}.[9]{1,2}");
		$("#txt_actualizacion_rut").inputmask("[9]{1,8}-[9|k]{1}");

		

		function cambiar(id, oferta, cantidad, rango, tipoOperacion){

			if (tipoOperacion == 0) {
				resta = parseFloat(cantidad) - parseFloat(rango);
				if (parseFloat(resta) < parseFloat(rango)) {
					alertify.error(`La cantidad no puede ser menor que ${rango}`, 3);
					return false;
				}else{
					op = parseFloat(cantidad) - parseFloat(rango)
					cantidad = op.toFixed(2);
				}
			}
			//suma
			if (tipoOperacion == 1) {
				op = parseFloat(cantidad) + parseFloat(rango)
				cantidad = op.toFixed(2);
			}

			$("#modalTitulo").html("Actualizando cantidades");
			$("#modalCargando").modal("show");

			$.ajax({
				type: "POST",
				url: "<?= Yii::getAlias('@web') ?>/site/actualizarcarro",
				dataType: "html",
				data: {id_producto: id , cantidad_productos: cantidad},
				success: function (response) {
						
					$("#modalCargando").modal("hide");

				},
			});
		}

		function eliminarProductoo(id){
			$("#modalTitulo").html("Eliminando producto");
			$("#modalCargando").modal("show");
			$.ajax({
				type: "POST",
				url: "<?= Yii::getAlias('@web') ?>/site/eliminarproductocarrito",
				dataType: "html",
				success: function (response) {
						
					$("#modalCargando").modal("hide");
					 
				},
				data: {id_producto: id, bd: 1},
			});
		}

		banderaIngresoEnvioGratis = 0;
		banderaIngresoCupon = 0;
		_descuento = 0;
		_costoEnvioComuna = 0;

		$(document).ready(function () {

			preguntarRutCliente()
			
			$('#datetimepicker4').datetimepicker({
				locale: 'es',
				format: 'L',
				defaultDate: "",
				ignoreReadonly: true,
				useCurrent: false,
				// debug : true,
				disabledDates: [
					"05/24/2019",
					"<?= $mesActual ?>",
					"<?= $mesSiguiente ?>",
					"<?= $maximoPedidos ?>",
					"<?= $bloqueInmediato ?>"
				],
				minDate: "<?= date("m/d/Y", strtotime('+ 1 day')) ?>",
				maxDate: "<?= date('m/d/Y', strtotime('last day of next month')); ?>"
			});
			totalConEnvio =  parseFloat($("#totalPedido").html()) + parseFloat($("#comunaPrecio").html())
			$("#totalConEnvio").html(totalConEnvio.toFixed(2));
			$('#_envioform-fechadespacho').val("");
			<?php if (isset($fechaDespachoElegidaCliente)) {  ?>
				$('#_envioform-fechadespacho').val("<?= $fechaDespachoElegidaCliente ?>");
			<?php }else{} ?>
		});

		$( "#_envioform-comuna" ).change(function(e) {
			$("#modalTitulo").html("Actualizando comuna de envio");
			$("#modalCargando").modal("show");

			envioGratis($(this).val(), $("#_envioform-fechadespacho").val())
		});

		// al cambiar la fecha revisar la comuna seleccionada para revisar si el despacho es gratis para ese dia
		$('#datetimepicker4').on('change.datetimepicker', function() {
			console.log("asd");
			envioGratis($("#_envioform-comuna").val(), $("#_envioform-fechadespacho").val())
		});


		// revisar si la comuna tiene envio gratis para un dia especifico
		function envioGratis(id_comuna, fechaDespacho){
				banderaIngresoEnvioGratis = 0;
			// if (id_comuna > 0 && fechaDespacho != "") {
				$.ajax({
					type: "POST",
					url: "<?= Yii::getAlias('@web') ?>/site/buscarcomuna",
					dataType: "html",
					data: {idComuna: id_comuna},
					success: function (response) {
						setTimeout(() => {
							$("#modalCargando").modal("hide");
						}, 500);

						
						if(response != 0){
							comuna = JSON.parse(response);
							_costoEnvioComuna = comuna.precio;
							//si la comuna tiene dias gratis
							
							if (comuna.dias_envio_gratis != null) {
								//se verifica si el dia actual esta en los dias de envio gratis
								if ($("#_envioform-fechadespacho").val() != "") {

									// se parte la fecha por /
									fechaSplit = $("#_envioform-fechadespacho").val().split("/");

									// se crea una fecha con los datos de la fecha de despacho para buscar el dia
									fechaDespachoEnvioGratis = new Date(fechaSplit[2], fechaSplit[1]-1, fechaSplit[0]);

									// si existe se agrega un mensaje y no se cuanta el envio al pedido
									if (comuna.dias_envio_gratis.indexOf(fechaDespachoEnvioGratis.getDay()) >= 0) {
										// se pregunta si existe un descuento por cupon para poder agregar el descuento por envio gratis
										if (banderaIngresoCupon == 0) {	
											$("#comuna").html(`${comuna.nombre}: <span id="comunaPrecio"> Despacho gratis</span>`);
											totalConEnvio =  (parseFloat($("#totalPedido").html())) - parseFloat(_descuento)
											alertify.notify('Se ha cambiado el costo de envio.', "info", 3);
											banderaIngresoEnvioGratis = 1;
										}else{
											$("#comuna").html(`${comuna.nombre}: <span id="comunaPrecio"> ${(parseInt(comuna.precio) == 0) ? "Gratis" : comuna.precio + "$"} </span>`);
											totalConEnvio =  (parseFloat($("#totalPedido").html()) + parseFloat(comuna.precio)) - parseFloat(_descuento)
											alertify.warning('Ya existe un descuento por cupon aplicado.', 3);
										}
									}else{
										// si no existe se agrega el precio tal cual como antes
										$("#comuna").html(`${comuna.nombre}: <span id="comunaPrecio"> ${(parseInt(comuna.precio) == 0) ? "Gratis" : comuna.precio + "$"} </span>`);
										totalConEnvio =  (parseFloat($("#totalPedido").html()) + parseFloat(comuna.precio)) - parseFloat(_descuento)
									}

									$("#totalConEnvio").html(totalConEnvio.toFixed(2));
									

								}else{
									$("#comuna").html(`${comuna.nombre}: <span id="comunaPrecio"> ${(parseInt(comuna.precio) == 0) ? "Gratis" : comuna.precio + "$"} </span>`);
										totalConEnvio =  (parseFloat($("#totalPedido").html()) + parseFloat(comuna.precio)) - parseFloat(_descuento)
								}

							}else{

								// if ($("#_envioform-fechadespacho").val() != "") {
									$("#comuna").html(`${comuna.nombre}: <span id="comunaPrecio"> ${(parseInt(comuna.precio) == 0) ? "Gratis" : comuna.precio + "$"} </span>`);
									totalConEnvio =  (parseFloat($("#totalPedido").html()) + parseFloat(comuna.precio)) - parseFloat(_descuento)

									$("#totalConEnvio").html(totalConEnvio.toFixed(2));
									
								// }
								// else{
								// 	alertify.notify('Seleccione una fecha de despacho', "warning", 3);
								// 	$("#_envioform-comuna").val("")
								// }
							}
						}else{
							$("#comuna").html(`Sin Comuna Seleccionada: <span id="comunaPrecio"> 0 $</span>`);
							totalConEnvio =  parseFloat($("#totalPedido").html()) - parseFloat(_descuento);
						}

						

					},
					error: function (xhr, ajaxOptions, thrownError) {
						// alert("Error buscando los datos : \n" + xhr.responseText);
					}
				});	
			// }
			// return false;
		}



		$('#confirmarPedidoForm').on('beforeSubmit', function(){
			$("#btn_pagar").fadeOut();
			$('#modalTitulo').html("Estamos guardando tu pedido");
			$('#modalCargando').modal('show');
		});

		$("#check_condiciones").change(function() {
			if( this.checked ) {
				$("#btn_pagar").removeAttr("disabled")
			}else{
				$("#btn_pagar").prop("disabled","disabled")
				// $("#btn_pagar").attr("disabled","disabled")
			}
		});

		// $("#btn_pagar").click(function(){
		// 	if ($("#radio_metodo_pago").prop("checked") == false) {
		// 		alertify.notify(`Debe seleccionar un medio de pago`, "info", 5);
		// 		return false;
		// 	}
		// });

		async function agregarCupon(){

			banderaIngresoCupon = 1;
			

			if (banderaIngresoEnvioGratis == 1) {
				//se debe quitar el descuento por ccomuna gratis
				$("#comuna").html(`${comuna.nombre}: <span id="comunaPrecio"> ${(parseInt(_costoEnvioComuna) == 0) ? "Gratis" : _costoEnvioComuna + "$"} </span>`);
				totalConEnvio =  (parseFloat($("#totalPedido").html()) + parseFloat(_costoEnvioComuna)) - parseFloat(_descuento)
				$("#totalConEnvio").html(totalConEnvio.toFixed(2));
			}

			if ($("#txt_cupon_promocional").val() == "") {
				alertify.notify(`Parece que no has ingresado ningun cupón`, "info", 5);
				return false;
			}

			$('#modalTitulo').html("Estamos validando el cupon");
			$('#modalCargando').modal('show');

			let cupon = $("#txt_cupon_promocional").val();

            var formData = {
                cupon : cupon,
            };

            var datos = new FormData();
            datos.append('data', JSON.stringify( formData ));

			datos2 = await fetch("<?= Yii::getAlias('@web'); ?>/site/validarcupon",{
				method: "POST",
				body: datos
			})

			res = await datos2.json()


			setTimeout(() => {
				$('#modalCargando').modal('hide');
			}, 1000);

			if (res == 0) {
				alertify.notify(`El cupón ha caducado`, "info", 3);
			}else if(res == 1){
				alertify.notify(`El cupón ha superado la cantidad de veces para usar`, "info", 5);
			}else if(res == 2){
				alertify.notify(`El cupón ingresado no es valido`, "info", 5);
			}else{

				_descuento = res.descuento;
				signoPorcentaje = "";
				let totalConEnvioYDescuento
				let costoEnvio = parseFloat($("#comunaPrecio").html()) > 0 ? parseFloat($("#comunaPrecio").html()) : 0
				if (res.tipo_cupon == 0) {

					totalConEnvioYDescuento =  (parseFloat($("#totalPedido").html()) + parseFloat(costoEnvio)) - parseFloat(res.descuento);

				}else{

					monto = parseFloat($("#totalPedido").html()) + parseFloat(costoEnvio)
					porcentajeDescuento = res.descuento * 0.01;

					totalConEnvioYDescuento =  monto - (monto * porcentajeDescuento);
					signoPorcentaje = "%";
				}

				console.log(totalConEnvioYDescuento);


				if (totalConEnvioYDescuento > 10000) {
					alertify.notify(`Cupón agregado`, "success", 3);
					$("#datosCupon > span").html(`Cupón (${res.cupon})  ${res.descuento}${signoPorcentaje}`);
					$("#datosCupon").fadeIn();
					$("#cuponId").val(res.id)
					$("#totalConEnvio").html(totalConEnvioYDescuento.toFixed(2));

				}else{
					alertify.notify(`La compra con el descuento aplicado debe superar los $10.000`, "warning", 5);
				}


			}


		}

		$("#eliminarCupon").click(function(){

			banderaIngresoCupon = 0;
			_descuento = 0;
			envioGratis($("#_envioform-comuna").val(), $("#_envioform-fechadespacho").val())

			let totalConEnvioEliminandoDescuento =  parseFloat($("#totalPedido").html()) + parseFloat($("#comunaPrecio").html())

			alertify.notify(`Cupón eliminado`, "success", 3);
			$("#datosCupon").fadeOut();
			$("#cuponId").val("")
			$("#datosCupon > span").html("");
			$("#totalConEnvio").html(totalConEnvioEliminandoDescuento.toFixed(2));
			$("#txt_cupon_promocional").val("")
		});


		function preguntarRutCliente(){

			$.ajax({
				type: "POST",
				url: "<?= Yii::getAlias('@web') ?>/site/pregutarrutcliente",
				dataType: "json",
				data: {id:0},
				success: function (response) {
					if(response == "" || response == null){
						$("#modalRut").modal("show");
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					// alert("Error buscando los datos : \n" + xhr.responseText);
				}
			});	
		}

		function actualizarRut(){
			if ($("#txt_actualizacion_rut").val() != "") {	
				if (checkRut($("#txt_actualizacion_rut").val()) == false) {
					alertify.notify(`Debes ingresar un rut valido para continuar`, "error", 3);
				}else{
					$.ajax({
						type: "POST",
						url: "<?= Yii::getAlias('@web') ?>/site/actualizarturcliente",
						dataType: "json",
						data: {rut : $("#txt_actualizacion_rut").val()},
						success: function (response) {
							if (response == 0) {
								alertify.notify(`Actualizacio de rut exitosa, gracias`, "success", 3);
								$("#modalRut").modal("hide");
							}else if(response == 1){
								alertify.notify(`El rut ingresado ya existe para otro cliente`, "warning", 3);
							}else{
								alertify.notify(`Error inesperado al actualizar el rut`, "error", 3);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							// alert("Error buscando los datos : \n" + xhr.responseText);
						}
					});	
				}
			}else{
				alertify.notify(`Ingresa tu rut`, "error", 3);
			}
		}

		function checkRut(rut) {

			// Despejar Puntos
			var valor = rut.replace('.','');
			// Despejar Guión
			valor = valor.replace('-','');

			// Aislar Cuerpo y Dígito Verificador
			cuerpo = valor.slice(0,-1);
			dv = valor.slice(-1).toUpperCase();

			// Formatear RUN
			rut = cuerpo + '-'+ dv

			// Si no cumple con el mínimo ej. (n.nnn.nnn)
			if(cuerpo.length < 7) { 
				// rut.setCustomValidity("RUT Incompleto"); 
				return false;
			}

			// Calcular Dígito Verificador
			suma = 0;
			multiplo = 2;

			// Para cada dígito del Cuerpo
			for(i=1;i<=cuerpo.length;i++) {

				// Obtener su Producto con el Múltiplo Correspondiente
				index = multiplo * valor.charAt(cuerpo.length - i);
				
				// Sumar al Contador General
				suma = suma + index;
				
				// Consolidar Múltiplo dentro del rango [2,7]
				if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

			}

			// Calcular Dígito Verificador en base al Módulo 11
			dvEsperado = 11 - (suma % 11);

			// Casos Especiales (0 y K)
			dv = (dv == 'K')?10:dv;
			dv = (dv == 0)?11:dv;

			// Validar que el Cuerpo coincide con su Dígito Verificador
			if(dvEsperado != dv) { 
				// rut.setCustomValidity("RUT Inválido"); 
				return false; 
			}

			// Si todo sale bien, eliminar errores (decretar que es válido)
			// rut.setCustomValidity('');
		}
	</script>


