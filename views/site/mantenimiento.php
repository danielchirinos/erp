<?php

$this->title = 'Mantenimiento';
$this->params['activeLink'] = "mantenimiento";
?>

   

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12 text-center">
                <img src="<?= Yii::getAlias('@web') ?>/images/mbgreen_logo.png" width="200" alt="">

                <p style="font-size:3em; color: var(--verde)">Estamos en mantenimiento, regresaremos pronto</p>
            </div>
        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->