<?php

$this->title = 'Activacion de usuario';
$this->params['activeLink'] = "usuario-activar";

?>

   

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">

        <div class="inner-sec-shop px-lg-4 px-3">
            <div class="about-content py-lg-5 py-3">
                <div class="row">

                    <div class=" col-md-8 offset-md-2">

                        <div class="alert alert-info " role="alert">
                            <h4 class="alert-heading"><b>Activacion de usuario</b></h4>
                            <?php if($activo == 1) { ?>
                                <p style="color:#0c5460">Email activado con exito</p>
                            <?php } else { ?>
                                <p style="color:#0c5460">Su usuario se encuntra inactivo, haga click en el siguiente enlace para activarlo.</p>
                                <hr>
                                <a href="<?= Yii::getAlias('@web') ?>/site/usuarioactivar/<?=$_GET["id"]?>">ACTIVAR</a>
                            <?php } ?>     
                        </div>

                        <div class="col-md-12 d-flex px-0">

                            <div class="col-md-6">
                                <a class="btn btn-verde col-md-12" href="<?= Yii::getAlias('@web') ?>/site/index"> Volver al inicio 
                                    <i class="fas fa-home"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a class="btn btn-verde col-md-12" href="<?= Yii::getAlias('@web') ?>/site/login"> Iniciar sesión 
                                    <i class="fas fa-user"></i>
                                </a>
                            </div>
                        </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->