<?php

$this->title = 'Pago Rechazado';
$this->params['activeLink'] = "pago-rechazado";

$total = 0;
?>

<section class="banner-bottom-wthreelayouts py-lg-5 py-3">
	<div class="container">
		<div class="inner-sec-shop px-lg-0 px-md-0 px-sm-0 px-4">
			<h3 class="tittle-w3layouts my-lg-4 mt-3">Informacion sobre el pago </h3>
		</div>	
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="px-lg-0 px-md-0 px-sm-0 px-4">
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label"><b>Envio a la dirección:</b></label>
                        <label  class="col-sm-4 col-form-label"><?= $datosCliente->direccion ?></label>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label"><b>Comuna de despacho:</b></label>
                        <label  class="col-sm-4 col-form-label"><?= $pedidoCabRechazado["comuna"]["nombre"] ?></label>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label"><b>Fecha de entrega:</b></label>
                        <label  class="col-sm-4 col-form-label"><?= $pedidoCabRechazado["fecha_despacho"] ?></label>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="px-lg-0 px-md-0 px-sm-0 px-2">
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Su pago fue rechazado</h4>
                        <p>Su pedido esta guardado en su perfil</p>

                        
                    </div>
                    <div class="row text-center">
                        <div class="col-md-6">
                            <a class="btn btn-secondary btn-sm mr-3 w-100" href="<?= Yii::getAlias('@web'). '/site/perfil' ?>">
                                <i class="fas fa-user"></i> Ir a mi perfil
                            </a> 
                        </div>
                        <div class="col-md-6"><a class="btn btn-verde btn-sm mr-3 w-100" href="<?= Yii::getAlias('@web'). '/site/comprar/'.$pedidoCabRechazado["id"] ?>">
                            <i class="fas fa-credit-card"></i> Volver a intentar el pago
                        </a> </div>
                    </div>
                </div>
            </div>
        </div>


		<br>

		<div class="row">
			<div class="col-md-12">
                <div class="px-lg-0 px-md-0 px-sm-0 px-2">  
                    <table class="table table-striped table-sm">
                        <tr style="background-color:var(--verde); color:white">
                            <td>Producto</td>
                            <td>Descripción</td>
                            <td>Cantidad</td>
                            <td>Precio Total</td>
                        </tr>

                        <?php foreach ($pedidoLinRechazado as $key => $value) { ?>
                            <tr>
                                <td class="text-center"><img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt=""></td>
                                <td><?= $value->producto->descripcion ?></td>
                                <td><?= $value->cantidad ?> </td>
                                <td><?= $value->precio ?> $</td>
                                <?php $total = $total + $value->precio ?>
                            </tr>
                        <?php } ?>

                    </table>
                </div>

			</div>
		</div>

		<div class="row">
			<div class="col-md-12 text-right">
                <h5>Costo de Envio: 
                    <b><?= ($pedidoCabRechazado["comuna"]["precio"] == 0 ) ? "Gratis" : $pedidoCabRechazado["comuna"]["precio"] . " $" ?></b>
                </h5>
				<h4>Precio total del pedido: <b><?= $pedidoCabRechazado["comuna"]["precio"] + $total ?> $</b></h4>
			</div>
		</div>
        <br>
        <div class="row float-right">
			<div class="col-md-12 ">
				<a class="btn btn-verde waves-effect waves-light" href="<?= Yii::getAlias('@web') ?>/site/index">Volver al inicio <i class="fas fa-home"></i></a>
			</div>
		</div>

	</div>
</section>