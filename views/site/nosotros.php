<?php

$this->title = 'Nosotros';
$this->params['activeLink'] = "nosotros";
?>

   

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">

        <div class="inner-sec-shop px-lg-4 px-3">
            <div class="about-content py-lg-5 py-3">
                <div class="row">

                    <div class="col-lg-6 about-info" style="background:transparent">
                        <h3 class="tittle-mbgreen text-left mb-lg-3 mb-3">Empresa</h3>
                        <p class="mb-xl-4 mb-lg-3 mb-md-4 my-3">Mb Green es una empresa familiar que empezó a dar el servicio de venta y distribucion de frutas, verduras y hortalizas a Restaurantes y ventas de comida de diferentes gastronomias. Desde el 2018 está en el mercado apostando dia a dia por el comercio sustentable, con la intension de contribuir en el mercado chileno como emprendedores dispuestos a innovar y a aportar por el crecimiento de un mejor país, con la intensión de responder los requerimientos de empresas, restaurantes o comercios en general que requieran el servicio de venta y despacho de verduras y frutas de calidad. </p>

                        <p class="mb-3">Se ajusta a las realidades de los negocios y accede a las flexibilidades administrativas, es por ello que pasado un año y en vista a las necesidades de amigos y familiares se han diversificado estableciendo el canal de venta hacia los hogares, despachando a domicilio a quienes no cuentan con el tiempo para ir a La Vega y poderse abastecer, siguiendo asi dos lineas de negocios simultáneamente.
                        </p>

                        <p class="mb-3">Lo que no tienen y el cliente lo requiera lo buscan.</p>

                        <p>No dude en ponerse en <a href="https://api.whatsapp.com/send?phone=56961652958&text=&source=&data=" target="_bank">contacto.</a></p>


                    </div>
                    <div class="col-lg-6 p-0">
                        <img src="<?= Yii::getAlias('@web') ?>/images/frutas_empresas.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->