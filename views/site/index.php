<?php

$this->title = '&#127824; MB Green | Despacho de Verduras y frutas a domicilio &#x1F34E; &#x1F350; &#x1F34A; &#x1F34B; &#x1F34C; &#x1F347;';
$this->params['activeLink'] = "inicio";

//meta tags
// Yii::$app->meta->keywords = "Mb Green, Frutas y verduras, despacho, despacho en santiago de frutas y verduras, frutas en santiago de chile, verduras en santiago de chile, verduras y frutas en santiago de chile";

?>


<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">

    <div class="row mx-0 px-0">
        <div class="col-12 text-center">
            <h1>Frutas y Verduras a domicilio</h1>
        </div>
    </div>

    <div class="container-fluid">
        <div class="inner-sec-shop px-lg-4 px-3">

            <!-- /grids -->
            <div class="bottom-sub-grid-content py-lg-5 py-3">
                <div class="row">
                    <div class="col-lg-3 col-md-4 text-center tip1">
                        <div class="bt-icon">

                            <!-- <span class="far fa-hand-paper fa-2x"></span> -->
                            <img src="<?= Yii::getAlias('@web') ?>/images/satisfaccion.svg" alt="" srcset="">
                        </div>

                        <h4 class="sub-tittle-mbgreen my-lg-4 my-3">Satisfación garantizada</h4>
                        <p>Los productos que entregamos son de calidad.</p>
                        
                    </div>
    
                    <div class="col-lg-3 col-md-4 text-center tip3">
                        <div class="bt-icon">
                            <!-- <span class="fas fa-clock fa-2x"></span> -->
                            <img src="<?= Yii::getAlias('@web') ?>/images/tiempo.svg" alt="" srcset="">
                        </div>

                        <h4 class="sub-tittle-mbgreen my-lg-4 my-3">Tiempo de entrega</h4>
                        <p>Disponemos de un calendario en nuestra web para que agendes cuando desees tu despacho.</p>

                    </div>

                    <div class="col-lg-3 col-md-4 text-center tip4">
                        <div class="bt-icon">
                            <!-- <span class="fas fa-map-marker-alt fa-2x"></span> -->
                            <img src="<?= Yii::getAlias('@web') ?>/images/cobertura.svg" alt="" srcset="">
                        </div>

                        <h4 class="sub-tittle-mbgreen my-lg-4 my-3">Cobertura</h4>
                        <p>Providencia, Lo Barnechea, Quilicura, Recoleta, Huechuraba, Valle Grande, Conchalí, Independencia, San Miguel, Santiago Centro, Las Condes, Vitacura, Quinta Normal, Pudahuel, Ñuñoa ,Macul, Estación Central, Chicureo, San Joaquín, La Reina, Renca, Cerro Navia, Lo Prado, Cerrillos y Pedro Aguirre Cerda (PAC)</p>

                    </div>


                    <div class="col-lg-3 col-md-4 text-center tip2">
                        <div class="bt-icon">
                            <!-- <span class="fas fa-phone fa-2x"></span> -->
                            <img src="<?= Yii::getAlias('@web') ?>/images/atencion.svg" alt="" srcset="">
                        </div>

                        <h4 class="sub-tittle-mbgreen my-lg-4 my-3">Atencion de calidad</h4>
                        <p>Respondemos tus inquietudes en el menor tiempo posible. </p>
                    </div>
    
                </div>
            </div>
                <!-- //grids -->


            <!--/slide-->
            
            <h3 class="tittle-mbgreen my-lg-4 my-4 " >Ofertas </h3>
            <div class="slider-img mid-sec" >
                <!--//banner-sec-->
                <div class="mid-slider">
                    <div class="owl-carousel owl-theme row d-flex">

                    <?php if (count($ofertas)>0) {
                        foreach ($ofertas as $key => $value) {  ?>
                            <div class="item ">
                                <div class="gd-box-info text-center">
                                    <div class=" women_two bot-gd itemOferta" id="productoOferta_<?= $value->id ?>">
                                        <div class="product-googles-info slide-img googles">
                                            <div class="men-pro-item">
                                                <div class="men-thumb-item">
                                                    <?php if ($value->imagen == null) { ?>
                                                        <img data-src="<?= Yii::getAlias('@web') ?>/images/productos/producto.jpg" class="img-fluid lazy-img" alt="">
                                                    <?php }else{ ?>
                                                        <img data-src="<?= Yii::getAlias('@web') .'/images/thum/'.$value->imagen?>" class="img-fluid lazy-img" alt="">
                                                    <?php  } ?>

                                                    <?php if ($value->es_oferta == 1) { ?>
                                                        <span class="product-new-top">Oferta</span>
                                                    <?php } ?>
                                                </div>
                                                <div class="item-info-product">

                                                    <div class="info-product-price">
                                                        <div class="grid_meta">
                                                            <div class="product_price">
                                                                <h4>
                                                                    <a href="#"><?= $value->descripcion ?> </a>
                                                                </h4>
                                                                <div class="grid-price mt-2">
                                                                    <span class="money "><?= $value->precio_oferta ?></span>
                                                                    <?php if ($value->es_unidad == 1) { ?>
                                                                        <span class="badge badge-light float-right mt-1">Por Unidad</span>
                                                                    <?php }else{ ?>
                                                                        <span class="badge badge-light float-right mt-1">Por Kg</span>
                                                                    <?php } ?>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="googles single-item hvr-outline-out agregarCarro">
                                                            <!-- <form action="#" method="post"> -->


                                                            <input type="hidden" id="txt_id<?= $value->id ?>" value="<?= $value->id ?>">
                                                            <input type="hidden" id="txt_precio<?= $value->id ?>" value="<?= $value->es_oferta == 1 ? $value->precio_oferta : $value->precio ?>">

                                                            <input type="hidden" id="txt_nombre<?= $value->id ?>" value="<?= $value->descripcion ?>">
                                                                
                                                            <?php $rango =  ($value->rango==null) ? '1' : str_replace(",",".",$value->rango)  ?>
                                                            <?php $rango = ($value->es_unidad==1) ? intval($rango) : $rango ?>

                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 1, <?= $rango ?>, 0)">-</button>
                                                                </div>
                                                                <input id="txt_cantOferta<?= $value->id ?>" class="" style=" width: 30%;" type="text" value="<?= $rango  ?>" step="<?= $rango  ?>" min="<?= $rango  ?>" readonly>
                                                                <div class="input-group-append">
                                                                    <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 1, <?= $rango ?>, 1)">+</button>

                                                                    <button id="btn_add<?= $value->id ?>" style="" type="button" class="googles-cart pgoogles-cart m-0 " onclick="guardarEnCarro(<?= $value->id ?>, 1, <?= $rango ?>, <?= $value->es_unidad ?>)">
                                                                        <i class="fas fa-cart-plus"></i>
                                                                    </button>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php }
                    }else{ ?>
                        <div class="col" >
                            <span class="badge badge-info">Sin productos</span>
                        </div>
                    <?php } ?>

                    </div>
                </div>
            </div>


            <h3 class="tittle-mbgreen my-lg-4 my-4">Frutas </h3>
            <div class="row">

                <?php if (count($frutas)>0) {
                    foreach ($frutas as $key => $value)  {  ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 pt-2  women_two d-flex" id="producto_<?= $value->id ?>" >
                            <div class="product-googles-info googles">
                                <div class="men-pro-item">
                                    <div class="men-thumb-item">
                                        <?php if ($value->imagen == null) { ?>
                                            <img data-src="<?= Yii::getAlias('@web') ?>/images/productos/producto.jpg" class="img-fluid lazy-img" alt="">
                                        <?php }else{ ?>
                                            <img data-src="<?= Yii::getAlias('@web') .'/images/thum/'.$value->imagen?>" class="img-fluid lazy-img" alt="">
                                        <?php  } ?>
    
                                        <?php if ($value->es_oferta == 1) { ?>
                                            <span class="product-new-top">Oferta</span>
                                            <?php } ?>
                                    </div>
                                    <div class="item-info-product">
                                        <div class="info-product-price">
                                            <div class="grid_meta">
                                                <div class="product_price">
                                                    <h4>
                                                        <a href="#"><?= $value->descripcion ?></a>
                                                    </h4>
                                                    <div class="grid-price mt-2">
                                                        <?php if ($value->es_oferta == 1) { ?>
                                                            <del class="precioOferta"><span class="money "><?= $value->precio ?></span></del>
                                                            <br>
                                                            <span class="money  "><?= $value->precio_oferta ?></span>
                                                            
                                                        <?php }else{ ?>
                                                            <span class="money  "><?= $value->precio ?></span>
                                                        <?php }  ?>
                                                        <?php if ($value->es_unidad == 1) { ?>
                                                            <span class="badge badge-light float-right mt-1">Por Unidad</span>
                                                        <?php }else{ ?>
                                                            <span class="badge badge-light float-right mt-1">Por Kg</span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="googles single-item hvr-outline-out agregarCarro">
                                                
                                                <!-- <form action="#" method="post"> -->
                                                    <input type="hidden" id="txt_id<?= $value->id ?>" value="<?= $value->id ?>">
                                                    <input type="hidden" id="txt_precio<?= $value->id ?>" value="<?= $value->es_oferta == 1 ? $value->precio_oferta : $value->precio ?>">

                                                    <input type="hidden" id="txt_nombre<?= $value->id ?>" value="<?= $value->descripcion ?>">

                                                    <?php $rango =  ($value->rango==null) ? '1' : str_replace(",",".",$value->rango)  ?>
                                                    <?php $rango = ($value->es_unidad==1) ? intval($rango) : $rango ?>
                                                    

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 0)">-</button>
                                                        </div>
                                                        <input id="txt_cant<?= $value->id ?>" class="" style=" width: 30%;" type="text" value="<?= $rango  ?>" step="<?= $rango  ?>" min="<?= $rango  ?>" readonly>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 1)">+</button>

                                                            <button id="btn_add<?= $value->id ?>" style="" type="button" class="googles-cart pgoogles-cart m-0 " onclick="guardarEnCarro(<?= $value->id ?>, 0, <?= $rango ?>, <?= $value->es_unidad ?>)">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>

                                                
                                                
                                                <!-- </form> -->
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } 
                }else{  ?>
                    <div class="col">
                        <span class="badge badge-info">Sin productos</span>
                    </div>
                <?php }?>
                
                

            </div>

            <div class="row pt-4 d-flex justify-content-center">
                <a href="<?= Yii::getAlias('@web') ?>/site/productos/1" class="btn btn-verde"> Ver todas</a>
            </div>

            

            <h3 class="tittle-mbgreen my-lg-4 my-4">Verduras </h3>
            <div class="row">

                <?php if (count($verduras)>0) {
                    foreach ($verduras as $key => $value) {  ?>
                    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 pt-2  women_two d-flex" id="producto_<?= $value->id ?>">
                        <div class="product-googles-info googles">
                            <div class="men-pro-item">
                                <div class="men-thumb-item">
                                    <?php if ($value->imagen == null) { ?>
                                        <img data-src="<?= Yii::getAlias('@web') ?>/images/productos/producto.jpg" class="img-fluid lazy-img" alt="">
                                    <?php }else{ ?>
                                        <img data-src="<?= Yii::getAlias('@web') .'/images/thum/'.$value->imagen?>" class="img-fluid lazy-img" alt="">
                                    <?php  } ?>

                                    <?php if ($value->es_oferta == 1) { ?>
                                        <span class="product-new-top">Oferta</span>
                                     <?php } ?>
                                </div>
                                <div class="item-info-product">
                                    <div class="info-product-price">
                                        <div class="grid_meta">
                                            <div class="product_price">
                                                <h4>
                                                    <a href="#"><?= $value->descripcion ?></a>
                                                </h4>
                                                <div class="grid-price mt-2">
                                                    <?php if ($value->es_oferta == 1) { ?>
                                                        <del class="precioOferta"><span class="money "><?= $value->precio ?></span></del>
                                                        <br>
                                                        <span class="money  "><?= $value->precio_oferta ?></span>
                                                        
                                                    <?php }else{ ?>
                                                        <span class="money  "><?= $value->precio ?></span>
                                                    <?php }  ?>
                                                    <?php if ($value->es_unidad == 1) { ?>
                                                        <span class="badge badge-light float-right mt-1">Por Unidad</span>
                                                    <?php }else{ ?>
                                                        <span class="badge badge-light float-right mt-1">Por Kg</span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="googles single-item hvr-outline-out agregarCarro">
                                            <!-- <form action="#" method="post"> -->
                                                <input type="hidden" id="txt_id<?= $value->id ?>" value="<?= $value->id ?>">
                                                <input type="hidden" id="txt_precio<?= $value->id ?>" value="<?= $value->es_oferta == 1 ?$value->precio_oferta : $value->precio ?>">

                                                <input type="hidden" id="txt_nombre<?= $value->id ?>" value="<?= $value->descripcion ?>">

                                                <?php $rango =  ($value->rango==null) ? '1' : str_replace(",",".",$value->rango)  ?>
                                                <?php $rango = ($value->es_unidad==1) ? intval($rango) : $rango ?>

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 0)">-</button>
                                                    </div>
                                                    <input id="txt_cant<?= $value->id ?>" class="" style=" width: 30%;" type="text" value="<?= $rango  ?>" step="<?= $rango  ?>" min="<?= $rango  ?>" readonly>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 1)">+</button>

                                                        <button id="btn_add<?= $value->id ?>" style="" type="button" class="googles-cart pgoogles-cart m-0 " onclick="guardarEnCarro(<?= $value->id ?>, 0, <?= $rango ?>, <?= $value->es_unidad ?>)">
                                                            <i class="fas fa-cart-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                            <!-- </form> -->

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                } else{  ?>
                    <div class="col">
                        <span class="badge badge-info">Sin productos</span>
                    </div>
                <?php } ?>

            </div>

            <div class="row pt-4 d-flex justify-content-center">
                <a href="<?= Yii::getAlias('@web') ?>/site/productos/2" class="btn btn-verde"> Ver todas</a>
            </div>


            <h3 class="tittle-mbgreen my-lg-4 my-4">Veggie Love </h3>
            <div class="row">

                <?php if (count($veggie)>0) {
                    foreach ($veggie as $key => $value) {  ?>
                    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 pt-2  women_two d-flex" id="producto_<?= $value->id ?>">
                        <div class="product-googles-info googles">
                            <div class="men-pro-item">
                                <div class="men-thumb-item">
                                    <?php if ($value->imagen == null) { ?>
                                        <img data-src="<?= Yii::getAlias('@web') ?>/images/productos/producto.jpg" class="img-fluid lazy-img" alt="">
                                    <?php }else{ ?>
                                        <img data-src="<?= Yii::getAlias('@web') .'/images/thum/'.$value->imagen?>" class="img-fluid lazy-img" alt="">
                                    <?php  } ?>

                                    <?php if ($value->es_oferta == 1) { ?>
                                        <span class="product-new-top">Oferta</span>
                                     <?php } ?>
                                </div>
                                <div class="item-info-product">
                                    <div class="info-product-price">
                                        <div class="grid_meta">
                                            <div class="product_price">
                                                <h4>
                                                    <a href="#"><?= $value->descripcion ?></a>
                                                </h4>
                                                <div class="grid-price mt-2">
                                                    <?php if ($value->es_oferta == 1) { ?>
                                                        <del class="precioOferta"><span class="money "><?= $value->precio ?></span></del>
                                                        <br>
                                                        <span class="money  "><?= $value->precio_oferta ?></span>
                                                        
                                                    <?php }else{ ?>
                                                        <span class="money  "><?= $value->precio ?></span>
                                                    <?php }  ?>
                                                    <?php if ($value->es_unidad == 1) { ?>
                                                        <span class="badge badge-light float-right mt-1">Por Unidad</span>
                                                    <?php }else{ ?>
                                                        <span class="badge badge-light float-right mt-1">Por Kg</span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="googles single-item hvr-outline-out agregarCarro">
                                            <!-- <form action="#" method="post"> -->
                                                <input type="hidden" id="txt_id<?= $value->id ?>" value="<?= $value->id ?>">
                                                <input type="hidden" id="txt_precio<?= $value->id ?>" value="<?= $value->es_oferta == 1 ?$value->precio_oferta : $value->precio ?>">

                                                <input type="hidden" id="txt_nombre<?= $value->id ?>" value="<?= $value->descripcion ?>">

                                                <?php $rango =  ($value->rango==null) ? '1' : str_replace(",",".",$value->rango)  ?>
                                                <?php $rango = ($value->es_unidad==1) ? intval($rango) : $rango ?>

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 0)">-</button>
                                                    </div>
                                                    <input id="txt_cant<?= $value->id ?>" class="" style=" width: 30%;" type="text" value="<?= $rango  ?>" step="<?= $rango  ?>" min="<?= $rango  ?>" readonly>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 1)">+</button>

                                                        <button id="btn_add<?= $value->id ?>" style="" type="button" class="googles-cart pgoogles-cart m-0 " onclick="guardarEnCarro(<?= $value->id ?>, 0, <?= $rango ?>, <?= $value->es_unidad ?>)">
                                                            <i class="fas fa-cart-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            <!-- </form> -->

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                } else{  ?>
                    <div class="col">
                        <span class="badge badge-info">Sin productos</span>
                    </div>
                <?php } ?>

            </div>

            <div class="row pt-4 d-flex justify-content-center">
                <a href="<?= Yii::getAlias('@web') ?>/site/productos/3" class="btn btn-verde"> Ver todas</a>
            </div>

            <h3 class="tittle-mbgreen my-lg-4 my-4">Tostaduria </h3>
            <div class="row">

                <?php if (count($tostaduria)>0) {
                    foreach ($tostaduria as $key => $value) {  ?>
                    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 pt-2  women_two d-flex" id="producto_<?= $value->id ?>">
                        <div class="product-googles-info googles">
                            <div class="men-pro-item">
                                <div class="men-thumb-item">
                                    <?php if ($value->imagen == null) { ?>
                                        <img data-src="<?= Yii::getAlias('@web') ?>/images/productos/producto.jpg" class="img-fluid lazy-img" alt="">
                                    <?php }else{ ?>
                                        <img data-src="<?= Yii::getAlias('@web') .'/images/thum/'.$value->imagen?>" class="img-fluid lazy-img" alt="">
                                    <?php  } ?>

                                    <?php if ($value->es_oferta == 1) { ?>
                                        <span class="product-new-top">Oferta</span>
                                     <?php } ?>
                                </div>
                                <div class="item-info-product">
                                    <div class="info-product-price">
                                        <div class="grid_meta">
                                            <div class="product_price">
                                                <h4>
                                                    <a href="#"><?= $value->descripcion ?></a>
                                                </h4>
                                                <div class="grid-price mt-2">
                                                    <?php if ($value->es_oferta == 1) { ?>
                                                        <del class="precioOferta"><span class="money "><?= $value->precio ?></span></del>
                                                        <br>
                                                        <span class="money  "><?= $value->precio_oferta ?></span>
                                                        
                                                    <?php }else{ ?>
                                                        <span class="money  "><?= $value->precio ?></span>
                                                    <?php }  ?>
                                                    <?php if ($value->es_unidad == 1) { ?>
                                                        <span class="badge badge-light float-right mt-1">Por Unidad</span>
                                                    <?php }else{ ?>
                                                        <span class="badge badge-light float-right mt-1">Por Kg</span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="googles single-item hvr-outline-out agregarCarro">
                                            <!-- <form action="#" method="post"> -->
                                                <input type="hidden" id="txt_id<?= $value->id ?>" value="<?= $value->id ?>">
                                                <input type="hidden" id="txt_precio<?= $value->id ?>" value="<?= $value->es_oferta == 1 ?$value->precio_oferta : $value->precio ?>">

                                                <input type="hidden" id="txt_nombre<?= $value->id ?>" value="<?= $value->descripcion ?>">

                                                <?php $rango =  ($value->rango==null) ? '1' : str_replace(",",".",$value->rango)  ?>
                                                <?php $rango = ($value->es_unidad==1) ? intval($rango) : $rango ?>

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 0)">-</button>
                                                    </div>
                                                    <input id="txt_cant<?= $value->id ?>" class="" style=" width: 30%;" type="text" value="<?= $rango  ?>" step="<?= $rango  ?>" min="<?= $rango  ?>" readonly>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-sm btn-outline-secondary" type="button" onclick="cambiarCantidad(<?= $value->id ?>, 0, <?= $rango ?>, 1)">+</button>

                                                        <button id="btn_add<?= $value->id ?>" style="" type="button" class="googles-cart pgoogles-cart m-0 " onclick="guardarEnCarro(<?= $value->id ?>, 0, <?= $rango ?>, <?= $value->es_unidad ?>)">
                                                            <i class="fas fa-cart-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            <!-- </form> -->

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                } else{  ?>
                    <div class="col">
                        <span class="badge badge-info">Sin productos</span>
                    </div>
                <?php } ?>

            </div>

            <div class="row pt-4 d-flex justify-content-center">
                <a href="<?= Yii::getAlias('@web') ?>/site/productos/4" class="btn btn-verde"> Ver todas</a>
            </div>
            


        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->


