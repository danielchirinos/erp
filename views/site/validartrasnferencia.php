<?php

$this->title = 'Confirmación del pago';
$this->params['activeLink'] = "confirmacion-pago";

$total = 0;
?>

<section class="banner-bottom-wthreelayouts py-lg-5 py-3">
	<div class="container">
		<div class="inner-sec-shop px-lg-0 px-md-0 px-sm-0 px-4">
			<h3 class="tittle-w3layouts my-lg-4 mt-3">Validación de pago </h3>
		</div>	
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="px-lg-0 px-md-0 px-sm-0 px-4">
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label"><b>Envio a la dirección:</b></label>
                        <label  class="col-sm-4 col-form-label"><?= $datosCliente->direccion_despacho ?></label>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label"><b>Comuna de despacho:</b></label>
                        <label  class="col-sm-4 col-form-label"><?= $pedidoCabConfirmado["comuna"]["nombre"] ?></label>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-4 col-form-label"><b>Fecha de entrega:</b></label>
                        <label  class="col-sm-4 col-form-label"><?= $pedidoCabConfirmado["fecha_despacho"] ?></label>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="px-lg-0 px-md-0 px-sm-0 px-2">
                    <div class="alert alert-success " role="alert">
                        <h4 class="alert-heading">Validación del pago</h4>
                        <p>Gracias por su compra <?= $pedidoCabConfirmado->cliente->nombre . " " . $pedidoCabConfirmado->cliente->apellido ?> </p>
                        <hr>
                        <ul class="pl-3">
                            <li>Una vez sea verificado el pago le llegará la confirmación del pedido a su email. Si no lo recibe, revise en la carpeta de "no deseados" o "spam"</li>
                        </ul>


                    </div>
                </div>
            </div>
        </div>


		<br>

		<div class="row">
			<div class="col-md-12">
                <div class="px-lg-0 px-md-0 px-sm-0 px-2">  
                    <table class="table table-striped table-sm">
                        <tr style="background-color:var(--verde); color:white">
                            <td>Producto</td>
                            <td>Descripción</td>
                            <td>Cantidad</td>
                            <td>Precio Total</td>
                        </tr>

                        <?php foreach ($pedidoLinConfirmado as $key => $value) { ?>
                            <tr>
                                <td class="text-center"><img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt=""></td>
                                <td><?= $value->producto->descripcion ?></td>
                                <td><?= $value->cantidad ?> </td>
                                
                                <td><?= $value->precio ?> $</td>
                                <?php $total = $total + $value->precio ?>
                            </tr>
                        <?php } ?>

                    </table>
                </div>

			</div>
		</div>

		<div class="row">
			<div class="col-md-12 text-right">
            <!-- costo de envio -->
                <h5>Costo de Envio: 
                    <b><?= ($pedidoCabConfirmado["comuna"]["precio"] == 0 ) ? "Gratis" : $pedidoCabConfirmado["comuna"]["precio"] . " $" ?></b>
                </h5>
                <!-- si llega un cupon -->
                <?php if ($pedidoCabConfirmado->id_cupon != null) { ?>
                    <h5>Cupon de descuento: 
                        <b><?= "(".$pedidoCabConfirmado["cupon"]["cupon"] .") ". $pedidoCabConfirmado["cupon"]["descuento"] ."".($pedidoCabConfirmado["cupon"]["tipo_cupon"] == 0 ? "" : "%" )?></b>
                    </h5>
                <?php } ?>
				<!-- sub total del pedido - solo sumatorias de los productos -->
                <h4>Sub total del pedido: <b><?= $total ?> $</b></h4>

                
                <!-- si viene un cupon -->
                <?php if ($pedidoCabConfirmado->id_cupon != null) { ?>
                    <!-- se revisa que tipo de cupon es 0 para monto 1 para porcentaje-->
                    <?php if($pedidoCabConfirmado["cupon"]["tipo_cupon"] == 0){ 
                        $descuento = $pedidoCabConfirmado["cupon"]["descuento"];
                        $monto = (floatval($pedidoCabConfirmado["comuna"]["precio"] + $total)) - $descuento;
                    ?>
                        <h4>Precio total del pedido: <b><?=  $monto ?> $</b></h4>

                    <?php }else{ 
                        $porcentajeDescuento = $pedidoCabConfirmado["cupon"]["descuento"] * 0.01;
                        $monto = floatval($pedidoCabConfirmado["comuna"]["precio"] + $total) - (floatval($pedidoCabConfirmado["comuna"]["precio"] + $total) * $porcentajeDescuento);
                        
                        ?>
                        <h4>Precio total del pedido: <b><?=  $monto ?> $</b></h4>
                    <?php } ?>

                    <!-- si no llega un cupon, solo se suma comuna con total de articulos -->
                <?php }else{  ?>
                    <h4>Precio total del pedido: <b><?= $pedidoCabConfirmado["comuna"]["precio"] + $total ?> $</b></h4>
                <?php } ?>

                
			</div>
		</div>
        <br>
        <div class="row float-right">
			<div class="col-md-12 ">
				<a class="btn btn-secondary waves-effect waves-light" href="<?= Yii::getAlias('@web') ?>/site/perfilpedidos"><i class="fas fa-user"></i> Ir a mis pedidos </a>
				<a class="btn btn-verde waves-effect waves-light" href="<?= Yii::getAlias('@web') ?>/site/index"><i class="fas fa-home"></i> Volver al inicio </a>
			</div>
		</div>

	</div>
</section>