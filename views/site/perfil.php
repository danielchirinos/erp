<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'Perfil';
    $this->params['activeLink'] = "perfil";
?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/compra.css">

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-2 ">
                <div class="list-group ">
                    <a href="#" class="list-group-item list-group-item-action active">Datos</a>
                    <a href="<?= Yii::getAlias('@web') ?>/site/perfilpedidos" class="list-group-item list-group-item-action">Pedidos</a>
                </div> 
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Datos personales</h4>
                                <hr>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nombre</label>
                                            <p><?= strtoupper($cliente->nombre . " " . $cliente->apellido) ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <p><?= $cliente->email ?> <span class="badge badge-pill badge-success" title="Verificado"><i class="fas fa-check"></i ></p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="rut">Rut</label>
                                            <p>
                                                <?php if ( $cliente->rut == "" ||  $cliente->rut == null) { ?>
                                                    <button class="btn btn-link p-0 m-0" data-toggle="modal" data-target="#modalRut"> Actualice su rut aquí </button>
                                                    <span class="badge badge-pill badge-danger" title="Por Actualizar">
                                                    <i class="fas fa-times"></i >
                                                <?php }else{ ?>

                                                    <?= $cliente->rut ?> 
                                                    <span class="badge badge-pill badge-success" title="Verificado">
                                                    <i class="fas fa-check"></i >
                                                <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row ">
                                    <div class="col-md-4 text-center">
                                        <button id="btn_actualizar_calve" class="btn btn-verde" data-toggle="modal" data-target="#modalClave">Cambiar Clave <i class="fas fa-key"></i></button>
                                    </div>
                                    <div class="col-md-4 text-center">

                                        <button id="btn_actualizar_calve" class="btn btn-verde " data-toggle="modal" data-target="#modalDireccion">Cambiar Direccion <i class="fas fa-address-card"></i></button>

                                    </div>
                                    <div class="col-md-4 text-center">
                                        <button id="btn_actualizar_calve" class="btn btn-verde " data-toggle="modal" data-target="#modalTelefono">Cambiar Numero de teléfono <i class="fas fa-phone"></i></button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
<!-- end tips frutas verduras ofertas -->


<!-- Modal clave -->
<div class="modal fade" id="modalRut" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Actualizar rut</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php $form = ActiveForm::begin([
            'method' => 'post', 
            'id'=> 'CambioRut', 
            'action' => Url::to(['site/cambiorut']),
            'enableClientValidation' => false,
            'enableAjaxValidation' => true, 
            ]); ?>
                <div class="modal-body ">

                <?php if ($cliente->rut == "" || $cliente->rut == null) { ?>
                    <?= $form->field($cambioRut, 'rut', [
                    'template' => '{label}<div class="form-group mb-0">
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger'],
                        'labelOptions' => [ 'style' => 'font-weight:bold' ]
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'Rut', 'type' => 'text']) ?>
                <?php } ?>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-verde" onclick="actualizarRut()" >Actualizar</button>

                </div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>

<!-- Modal clave -->
<div class="modal fade" id="modalClave" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cambio de clave</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php $form = ActiveForm::begin([
            'method' => 'post', 
            'id'=> 'CambioClave', 
            'action' => Url::to(['site/cambioclave']),
            'enableClientValidation' => false,
            'enableAjaxValidation' => true, 
            ]); ?>
                <div class="modal-body ">

                    <?= $form->field($cambioClave, 'claveAnterior', [
                    'template' => '{label}<div class="form-group mb-0">
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger'],
                        'labelOptions' => [ 'style' => 'font-weight:bold' ]
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'Clave anterior', 'type' => 'password']) ?>

                    <?= $form->field($cambioClave, 'clave', [
                    'template' => '{label}<div class="form-group mb-0">
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger'],
                        'labelOptions' => [ 'style' => 'font-weight:bold' ]
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'Nueva Clave', 'type' => 'password']) ?>

                    <?= $form->field($cambioClave, 'claveRepetir', [
                    'template' => '{label}<div class="form-group mb-0">
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger'],
                        'labelOptions' => [ 'style' => 'font-weight:bold' ]
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'Repetir Nueva Clave', 'type' => 'password']) ?>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-verde']) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>

<!-- Modal direccion -->
<div class="modal fade" id="modalDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cambio de dirección</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php $form = ActiveForm::begin([
            'method' => 'post', 
            'id'=> 'CambioDireccion', 
            'action' => Url::to(['site/cambiodireccion']),
            'enableClientValidation' => false,
            'enableAjaxValidation' => true, 
            ]); ?>
                <div class="modal-body ">
                    <?= $form->field($cambioDireccion, 'direccion', [
                    'template' => '{label}<div class="form-group mb-0">
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger'],
                        'labelOptions' => [ 'style' => 'font-weight:bold' ]
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'Clave anterior', 'type' => 'text', 'value'=> $cliente->direccion]) ?>

                    <?= $form->field($cambioDireccion, 'direccion_despacho', [
                    'template' => '{label}<div class="form-group mb-0">
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger'],
                        'labelOptions' => [ 'style' => 'font-weight:bold' ]
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'Clave anterior', 'type' => 'text', 'value'=> $cliente->direccion_despacho]) ?>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-verde']) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>

<!-- Modal telefono -->
<div class="modal fade" id="modalTelefono" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cambio de telefono</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php $form = ActiveForm::begin([
            'method' => 'post', 
            'id'=> 'CambioTelefono', 
            'action' => Url::to(['site/cambiotelefono']),
            'enableClientValidation' => false,
            'enableAjaxValidation' => true, 
            ]); ?>
                <div class="modal-body ">
                    <?= $form->field($cambiotelefono, 'telefono', [
                    'template' => '{label}<div class="form-group mb-0">
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger'],
                        'labelOptions' => [ 'style' => 'font-weight:bold' ]
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'Clave anterior', 'type' => 'text', 'value'=> $cliente->telefono]) ?>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-verde']) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>


<script>

    $("#_cambiorutform-rut").inputmask("[9]{1,8}-[9|k]{1}");

    function actualizarRut(){
        if ($("#_cambiorutform-rut").val() != "") {	
            if (checkRut($("#_cambiorutform-rut").val()) == false) {
                alertify.notify(`Debes ingresar un rut valido para continuar`, "error", 3);
            }else{
                $("#CambioRut").submit()
            }
        }else{
            alertify.notify(`Ingresa tu rut`, "error", 3);
        }
    }

    function checkRut(rut) {

        // Despejar Puntos
        var valor = rut.replace('.','');
        // Despejar Guión
        valor = valor.replace('-','');

        // Aislar Cuerpo y Dígito Verificador
        cuerpo = valor.slice(0,-1);
        dv = valor.slice(-1).toUpperCase();

        // Formatear RUN
        rut = cuerpo + '-'+ dv

        // Si no cumple con el mínimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7) { 
            // rut.setCustomValidity("RUT Incompleto"); 
            return false;
        }

        // Calcular Dígito Verificador
        suma = 0;
        multiplo = 2;

        // Para cada dígito del Cuerpo
        for(i=1;i<=cuerpo.length;i++) {

            // Obtener su Producto con el Múltiplo Correspondiente
            index = multiplo * valor.charAt(cuerpo.length - i);
            
            // Sumar al Contador General
            suma = suma + index;
            
            // Consolidar Múltiplo dentro del rango [2,7]
            if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

        }

        // Calcular Dígito Verificador en base al Módulo 11
        dvEsperado = 11 - (suma % 11);

        // Casos Especiales (0 y K)
        dv = (dv == 'K')?10:dv;
        dv = (dv == 0)?11:dv;

        // Validar que el Cuerpo coincide con su Dígito Verificador
        if(dvEsperado != dv) { 
            // rut.setCustomValidity("RUT Inválido"); 
            return false; 
        }

        // Si todo sale bien, eliminar errores (decretar que es válido)
        // rut.setCustomValidity('');
    }
</script>
