<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Editar pedido';
$this->params['activeLink'] = "editar-pedido";
?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/compra.css">


<style>
.field-pedidoscab-direccion_despacho{
    width:100%;
}
</style>

<?php $form = ActiveForm::begin([
            'method' => 'post', 
            'id'=> 'editarpedido', 
            'action' => Url::to(['site/editarpedidoconfirmar']),
            'enableClientValidation' => false,
            'enableAjaxValidation' => true, 
            ]); ?>
            

    <!-- tips frutas verduras ofertas -->	
    <section class="banner-bottom-mbgreen py-lg-5 py-3">
        <div class="container">

            <?php if (count($pedidoslin) > 0) { ?>
                <div class="row">
                    <div class="col-md-1">
                        <label for=""><b>Estado</b></label><br>
                        <?= ($pedidoscab->id_estadopedido == 1) ? '<span class="badge badge-info">'.$pedidoscab->estadopedido->estado.'</span>' : '<span class="badge badge-success">'.$pedidoscab->estadopedido->estado.'</span>'  ?>
                    </div>
                    <div class="col-md-2">                
                        <?= $form->field($pedidoscab, 'id_comuna', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->dropDownList($comunas, ['prompt'=>'Seleccione', 'class' => 'form-control']) ?>
                    </div>
                    
                    <div class="col-md-2">
                        <div class="input-group mb-3 ">
                            <?= $form->field($pedidoscab, 'direccion_despacho', [
                            'template' => '{label}<div class="form-group mb-0 ">
                                                {input}
                                            </div>
                                            {error}{hint}',
                                'errorOptions'=>['class'=>'badge badge-danger'],
                                'labelOptions' => [ 'style' => 'font-weight:bold' ]
                            ])->textInput(['class'=>'form-control ', 'placeholder' => 'Direccion de despacho', 'type' => 'text', 'value'=> $pedidoscab->direccion_despacho]) ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($pedidoscab, 'fecha_despacho', ['template' => '{label}
                            <div class="form-group">
                                <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                    {input}
                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            {error}{hint}','errorOptions'=>['class'=>'badge badge-danger'],
                            'labelOptions' => [ 'style' => 'font-weight:bold' ]
                            ])->textInput(['class'=>'form-control', 'placeholder' => 'dia / mes / año', 'required' => 'required', 'required'=> 'required']) ?>
                    </div>
                    <div class="col-md-2">
                        <label for=""><b>Cantidad de productos</b></label><br>
                        <span><?= $pedidoscab->cantidad_productos ?></span>
                    </div>
                    <div class="col-md-2">
                        <label for=""><b>Precio Total</b></label><br>
                        <span><?= $pedidoscab->precio_total ?> $</span>
                    </div>
                </div>

                <hr>
                
                <div class="row float-right">
                    <a class="btn btn-primary btn-sm mr-3" href="<?= Yii::getAlias('@web'). '/site/perfil/' ?>"><i class="fas fa-arrow-left"></i> Volver</a>
                    <?= Html::submitButton(Yii::t('app', '<i class="fas fa-save"></i> Guardar '), ['class' => 'btn btn-verde btn-sm']) ?>
                </div>

                <div class="clearfix"></div>
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <div class="inner-sec-shop">
                            <div class="checkout-right">
                                <table class="timetable_sub">
                                    <thead>
                                        <tr>
                                            <th>Imagen</th>
                                            <th>Descripción</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Precio total</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php  foreach ($pedidoslin as $key => $value) { ?>

                                        <tr class="rem1">
                                            <td><img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "/productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt="" class="img-responsive"></td>
                                            <td><?= $value->producto->descripcion?></td>
                                            <td>
                                                <input type="hidden" class="txt_id_hidden" value="<?=$value->id_producto ?>">
                                                <input type="hidden" class="txt_id_pedidoLin_hidden" value="<?=$value->id ?> ">
                                                <input class="txt_cantidad_productos" id="txt_cantidad_productos" name="txt_cantidad_productos<?=$value->id ?>" type="number" value="<?=$value->cantidad ?>" step="<?=$value->producto->rango ?>" min="<?=$value->producto->rango ?>">
                                            </td>
                                            <td><?= $value->producto->precio ?> $</td>
                                            <td><?= $value->precio ?> $</td>
                                            <td><i class="fas fa-times icon-eliminar" onclick="eliminarProductoo(<?=$value->id ?>, '<?= $value->producto->descripcion?>', <?= $pedidoscab->id ?>)"></i></td>
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }else{ ?>
                <div class="col-md-12 text-center">
                    <p class="pb-2" style="font-size:2em">Sin articulos para continuar con la edición</p>
                
                        <a class="btn btn-primary mb-3 w-25" href="<?= Yii::getAlias('@web') ?>/site/perfil">Volver al perfil <i class="fas fa-user"></i></a>
                    
                </div>
            <?php }  ?>

            
            

        </div>
    </section>
    <!-- end tips frutas verduras ofertas -->

    <?= Html::hiddenInput('txt_pedidocab', $pedidoscab->id); ?>

<?php $form->end(); ?>

<script>

	function eliminarProductoo(id, descripcion, idpedidoCab){

        id_pedidocab = idpedidoCab;

        alertify.confirm(`Eliminar producto ${descripcion}`, '¿Desea eliminar el producto?',
            function(){ 
                $.ajax({
                    type: "POST",
                    url: "<?= Yii::getAlias('@web') ?>/site/eliminarproductopedido",
                    dataType: "html",
                    data: {id_pedidolin: id, id_pedidocab: id_pedidocab},
                });
            }, 
            function(){ 
                alertify.error('Accion cancelada por cliente')
            });
	
	}
</script>





<script>

$(function () {

    $('#datetimepicker4').datetimepicker({
        locale: 'es',
        format: 'L',
        // debug : true,
        disabledDates: [
            "<?= $mesActual ?>",
            "<?= $mesSiguiente ?>"
        ],
        minDate: "<?= date("m/d/Y", strtotime("+1 day")) ?>",
        maxDate: "<?= date('m/d/Y', strtotime('last day of next month')); ?>"
    });

    $("#pedidoscab-fecha_despacho").val("<?= $pedidoscab->fecha_despacho ?>");

});
</script>