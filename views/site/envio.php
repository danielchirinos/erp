<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Pedido';
$this->params['activeLink'] = "envio";

$total = 0;
?>

<style>
.form-group{
    margin-bottom: unset !important;
}
</style>

<section class="banner-bottom-wthreelayouts py-lg-5 py-3">
	<div class="container">
		<div class="inner-sec-shop ">
			<h3 class="tittle-w3layouts my-lg-4 mt-3">Detalles de la compra </h3>
		</div>	

		<br>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-sm">
					<tr style="background-color:var(--verde); color:white">
						<td>Producto</td>
						<td>Descripción</td>
						<td>Cantidad</td>
						<td>Precio</td>
					</tr>

					<?php foreach ($envio as $key => $value) { ?>
						
						<tr>
							<td class="text-center"><img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt=""></td>
							<td><?= $value->producto->descripcion ?></td>
							<td><?= $value->cantidad_productos ?> </td>
							<td><?= $value->precio_total ?> $</td>
							<?php $total = $total + $value->precio_total ?>
						</tr>
					<?php }  ?>
				</table>

			</div>
		</div>

		<div class="row">
			<div class="col-md-12 text-right ">
				<ul style="list-style: none">
					<li> Total Pedido: <span id="totalPedido"><?= $total ?> $</span></li>
					<li id="comuna">Sin Comuna Seleccionada: <span id="comunaPrecio"> 0 $</span></li>
				</ul>
				<br>
				<h4 class="float-right">Total a pagar: <b id="totalConEnvio"></b> $</h4>
			</div>
			<div class="col-md-12 mt-2">

				<?php $form = ActiveForm::begin([
                        'method' => 'post', 
						'id'=> 'guardarEnvio', 
						'action' => 'confirmacionpedido',
                        'options'=> [
                            'class' => 'form-horizontal',
                        ], 
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>



                        <div class="row">
                            <div class="col-md-4">
                                <?= $form->field($model, 'direccion', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Dirección', 'value' => $datosCliente->direccion]) ?>
							</div>
							
							<div class="col-md-4">
								<?= $form->field($model, 'comuna', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->dropDownList($comunas, ['prompt'=>'Seleccione', 'class' => 'form-control']) ?>
							</div>
							
                            <div class="col-md-4">
								<?= $form->field($model, 'fechaDespacho', ['template' => '{label}
								<div class="form-group">
									<div class="input-group date" id="datetimepicker4" data-target-input="nearest">
										{input}
										<div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="fa fa-calendar"></i></div>
										</div>
									</div>
								</div>
								{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                             	])->textInput(['class'=>'form-control', 'placeholder' => 'dia / mes / año']) ?>
                            </div>
                        </div>


                        <div class="row float-right">

							<div class="form-actions">
								<div class="card-body ">
									<?= Html::submitButton(Yii::t('app', 'Confirmar Pedido <i class="fas fa-check"></i>'), ['class' => 'btn btn-verde waves-effect waves-light']) ?>
									
								</div>
							</div>
							
						</div>


                    <?php $form->end(); ?>
			</div>
		</div>

	</div>
</section>

	<script type="text/javascript">
		$(function () {
			$('#datetimepicker4').datetimepicker({
				locale: 'es',
				format: 'L',
                defaultDate: "",
				// debug : true,
				disabledDates: [
					"<?= $mesActual ?>",
                    "<?= $mesSiguiente ?>"
				],
				minDate: "<?= date("m/d/Y", strtotime("-1 day")) ?>",
				maxDate: "<?= date('m/d/Y', strtotime('last day of next month')); ?>"
			});
			totalConEnvio =  parseFloat($("#totalPedido").html()) + parseFloat($("#comunaPrecio").html())
			$("#totalConEnvio").html(totalConEnvio.toFixed(2));
		});

		$( "#_envioform-comuna" ).change(function(e) {
			$.ajax({
                type: "POST",
                url: "<?= Yii::getAlias('@web') ?>/site/buscarcomuna",
                dataType: "html",
                data: {idComuna: $(this).val()},
                success: function (response) {
					
					if(response != 0){
						comuna = JSON.parse(response);
						$("#comuna").html(`${comuna.nombre}: <span id="comunaPrecio"> ${(parseInt(comuna.precio) == 0) ? "Gratis" : comuna.precio + "$"} </span>`);
					}else{
						$("#comuna").html(`Sin Comuna Seleccionada: <span id="comunaPrecio"> 0 $</span>`);
					}

					totalConEnvio =  parseFloat($("#totalPedido").html()) + parseFloat(comuna.precio)
						$("#totalConEnvio").html(totalConEnvio.toFixed(2));
 
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error buscando los datos : \n" + xhr.responseText);
                }
            });
		});

	</script>
