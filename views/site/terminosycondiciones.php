<?php

$this->title = 'Terminos y condiciones';
$this->params['activeLink'] = "terminos-y-condiciones";
?>



<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">

        <div class="inner-sec-shop px-lg-4 px-3">
            <div class="about-content py-lg-5 py-3">
                <div class="row">

                    <div class="col-lg-12 about-info">
                        <h3 class="tittle-mbgreen text-center mb-lg-5 mb-3">Terminos y condiciones</h3>
                        <p class="my-xl-4 my-lg-3 my-md-4 my-3">En <b>MBGreen.cl</b> se publican los días de disponibilidad para el despacho de entrega de las frutas y verduras, el servicio se efectúa en un rango de hora que va desde las 9:00hrs hasta las 18:00hrs, en horario matutino y medio día las comunas de la zona oriente y centro de Santiago. En caso de no poderse efectuar el servicio, la empresa se comunicará con el cliente y hará saber el retraso o reagendamiento del pedido. </p>
                        <p>El cliente debe considerar que de acuerdo a la Ley N° 19.496 de Protección al Consumidor los pedidos de pago On line no sufren modificaciones ni anulaciones, por lo que no se gestiona la devolución. En caso de la no existencia del producto por cambio de temporada éste será sustituido por uno similar o de mejor calidad del mismo monto ya pagado. </p>

                    </div>
                    
                </div>
            </div>


        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->