<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Detalle pedido';
$this->params['activeLink'] = "detalle-pedido";

$total = 0;
?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/compra.css">
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<section class="banner-bottom-wthreelayouts py-lg-5 py-3">
	<div class="container">
		<?php if (count($pedidoConfirmadoLin)>0) {  ?>
			<div class="row">
		
				<div class="col-md-6">
					<div class="inner-sec-shop px-lg-5 px-md-0 px-sm-0 px-2">
						<h3 class="tittle-w3layouts my-lg-4 mt-3">Detalle del pedido de pago </h3>
						<div class="checkout-right">
							<h4>Su pedido contiene:
								<span><?= count($pedidoConfirmadoLin) ?> Productos</span>
							</h4>
							<table class="timetable_sub">
								<thead>
									<tr>
										<th>Producto</th>
										<th>Cantidad</th>
										<th>Descripción</th>
										<th>Precio total</th>
									</tr>
								</thead>
								<tbody>
								<?php  foreach ($pedidoConfirmadoLin as $key => $value) { ?>
								
									<tr class="rem1">
										<td class="invert">
											<a href="#">
												<img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "/productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt="" class="img-responsive">
											</a>
										</td>
										<td class="invert">
											<div class="quantity">
												<div class="quantity-select">
													<input type="hidden" id="txt_id_hidden" value="<?=$value->id_producto ?>">
													<input class="input_cantidad" id="input_cantidad" type="number" readonly value="<?=$value->cantidad ?>" step="<?=$value->producto->rango ?>" min="<?=$value->producto->rango ?>">
												</div>
											</div>
										</td>
										<td class="invert"><?=$value->producto->descripcion ?> </td>

										<td class="invert"><?= $value->precio ?> $</td>
									</tr>

								<?php } ?>

								</tbody>
							</table>
						</div>
						

					</div>
				</div>

				<div class="col-md-6 ">
                    <div class="px-lg-0 px-md-0 px-sm-0 px-4">
                        <h3 class="tittle-w3layouts my-lg-4 mt-3 pb-3">Datos de envio </h3>
							<div class="row">
								<div class="col-md-6">
									<ul>
										<li><b>Comuna: </b><?= $pedidoConfirmadoCab->comuna->nombre ?></li>
										<li><b>Fecha de despacho: </b><?= $pedidoConfirmadoCab->fecha_despacho ?></li>
										<li><b>Comuna: </b><?= $pedidoConfirmadoCab->direccion_despacho ?></li>
									</ul>
								</div>
								<div class="col-md-6 text-center">
									<img src="<?= Yii::getAlias('@web') ?>/images/pagos-webpay-plus.png" width="200" alt="">
								</div>
							</div>
							

                        <hr>
                        <?php $form = ActiveForm::begin([
                            'method' => 'post', 
                            'id'=> 'PagarPedidoForm', 
                            'action' => $formAction, 
                            'options'=> [
                                'class' => 'form-horizontal',
                            ], 
                            'enableClientValidation' => false,
                            'enableAjaxValidation' => true, 
                            ]); ?>

							<input type="hidden" name="token_ws" value="<?= $token_ws ?>">

                            <div class="row">
                                <div class="col-md-12">
									<?= Html::submitButton(Yii::t('app', 'Pagar Pedido <i class="fas fa-credit-card"></i>'), ['class' => 'btn btn-verde waves-effect waves-light col-12', 'disabled' => 'disabled' ,"id"=> "btn_pagar"]) ?>
                                </div>
								<div class="col-md-12 mb-0">
									<div class="form-group form-check">
										<input type="checkbox" class="form-check-input" id="check_condiciones">
										<label class="form-check-label" for="exampleCheck1">
											He leido y acepto los términos de compras: las <a class="text-verde" href="<?= Yii::getAlias('@web') ?>/site/terminosycondiciones" target="_blank">Condiciones de reserva</a>
										</label>
									</div>
                                </div>
                            </div>

                        <?php $form->end(); ?>
                        
                        <div class="checkout-left row justify-content-end">
                            <div class="col-md-12 checkout-left-basket">
                                <h3>Resumen del pedido</h3>
                                <hr>

                                <ul>
                                    <?php  
                                        $total = 0;
                                        foreach ($pedidoConfirmadoLin as $key => $value) { ?>
                                        <li><?= $value->producto->descripcion ?>
                                            <span><?= $value->precio ?> $ </span>
                                        </li>
                                    <?php $total = $total + $value->precio; } ?>
                                    
                                    <hr>
                                    <li id="comuna"><?= $pedidoConfirmadoCab->comuna->nombre ?>: <span id="comunaPrecio"><?= $pedidoConfirmadoCab->comuna->precio ?> $</span></li>

                                    <li><b>Total Pedido:<span id="totalPedido"><?= $total ?> $</span> </b></li>
                                    <hr>
                                    <h3 class="float-right">Total a pagar: <b id="totalConEnvio"></b> $</h3>

                                </ul>
                            </div>

                            <div class="clearfix"> </div>

                        </div>
                    </div>
				</div>
			
			</div>
		<?php }else{ ?>
			<div class="col-md-12 text-center">
				<p class="pb-2" style="font-size:2em">Sin articulos para continuar con la compra</p>
			
					<a class="btn btn-primary mb-3 w-25" href="<?= Yii::getAlias('@web') ?>/site/index">Volver al inicio <i class="fas fa-home"></i></a>
				
			</div>
		<?php }  ?>
		

	</div>
</section>

<script>
	$("#totalConEnvio").html(`${(parseFloat($("#comunaPrecio").html()) + parseFloat($("#totalPedido").html())).toFixed(2)}`);
	
	$("#txt_precioPagar").val(`${$("#totalConEnvio").html()}`);

	$('#PagarPedidoForm').on('beforeSubmit', function(){
		$('#modalTitulo').html("Estamos enviando un correo con el detalle del pedido");
		$('#modalCargando').modal('show');
	});


	$("#check_condiciones").change(function() {
		if( this.checked ) {
			$("#btn_pagar").removeAttr("disabled")
		}else{
			$("#btn_pagar").prop("disabled","disabled")	
			// $("#btn_pagar").attr("disabled","disabled")	
		}
	});
</script>




