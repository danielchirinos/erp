<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Procesar pago';
$this->params['activeLink'] = "procesar-pago";
?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/compra.css">

<!-- dropify -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/dropify-master/css/dropify.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/dropify-master/js/dropify.js"></script>

<!-- tips frutas verduras ofertas -->
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">

        <div class="inner-sec-shop px-lg-4 px-3">
            <div class="about-content py-lg-5 py-3">
                <div class="row">

                    <div class="col-lg-6  ">

                        <div class="sticky-top">

                            <h4 class="mb-3">Su compra contiene:
                                <span><?= count($pedidoConfirmadoLin) == 1 ? count($pedidoConfirmadoLin). " Producto" : count($pedidoConfirmadoLin). " Productos" ?>
                                </span>
                            </h4>

                            <table class="timetable_sub ">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Descripción</th>
                                        <th>Precio total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php  
                                    $total = 0;
                                    foreach ($pedidoConfirmadoLin as $key => $value) { ?>
                                    <tr class="rem1">
                                        <td class="invert">
                                            <a href="#">
                                                <img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "/productos/producto.jpg" : "thum/".$value->producto->imagen ?>"
                                                    width="50" alt="" class="img-responsive">
                                            </a>
                                        </td>
                                        <td class="invert">
                                            <?= $value->cantidad ?>
                                        </td>
                                        <td class="invert"><?=$value->producto->descripcion ?> </td>

                                        <td class="invert"><?= $value->precio ?> $</td>

                                    </tr>

                                    <?php } ?>

                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="col-lg-6 p-0">

                        <div class="checkout-left row justify-content-end">
                            <div class="col-md-12 checkout-left-basket">
                                <h3>Resumen del pedido</h3>
                                <hr>

                                <ul>

                                    <li id="comuna">
                                        Fecha despacho: <span><?= $pedidoConfirmadoCab->fecha_despacho ?></span>
                                    </li>
                                    <li id="comuna">
                                        Dirección despacho: <span><?= $pedidoConfirmadoCab->direccion_despacho ?></span>
                                    </li>
                                    <li id="comuna">
                                        
                                        <?php if ($banderaGratis != 0) { ?>
                                            Comuna: <span><?= $pedidoConfirmadoCab->comuna->nombre. " -  Despacho Gratis" ?></span>
                                        <?php }else{ ?>
                                            Comuna: <span><?= $pedidoConfirmadoCab->comuna->nombre. " - ". $pedidoConfirmadoCab->comuna->precio ?></span>
                                        <?php } ?>
                                        
                                    </li>

                                    <?php if ($banderaCupon != "") { ?>
                                        <li id="comuna">Cupon:
                                            <span><?= $banderaCupon ?></span>
                                        </li>
                                    <?php } ?>

                                    <li>
                                        <b>
                                            Total Pedido: <span
                                                id="totalPedido"><?= $pedidoConfirmadoCab->precio_total ?> $</span>
                                        </b>
                                    </li>

                                    

                                    <h5 class="float-right">Total a pagar: <b id="totalConEnvio">
                                            <?= $monto ?></b> $
                                    </h5>

                                </ul>
                            </div>

                            <div class="clearfix"> </div>

                        </div>

                        <hr>

                        <?php $form = ActiveForm::begin([
                                'method' => 'post', 
                                'id'=> 'confirmarOtroMedioPago', 
                                'action' => Url::to(['site/confirmartrasnferencia']),
                                'options'=> [
                                    'class' => 'form-horizontal',
                                    'enctype'=>"multipart/form-data"
                                ], 
                                'enableClientValidation' => false,
                                'enableAjaxValidation' => true, 
                                ]); ?>


                            <!-- id del pedido hidden para recuperar un pedido -->
                            <input type="hidden"
                                value="<?= isset($pedidoConfirmadoCab->id) ? $pedidoConfirmadoCab->id : null  ?>"
                                name="pedidoCabId">

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'medio_pago', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->dropDownList($medioPago, ['prompt'=>'Seleccione', 'class' => 'form-control']) ?>

                                    <?= $form->field($model, 'nro_referencia', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'Nro referencia transacción']) ?>

                                    <?= $form->field($model, 'monto', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'value' => $monto, "readonly" => true ]) ?>

                                </div>
                                <div class="col-md-6">
                                    <label>Agregar imagen</label>

                                    <input type="file" name="file_imagen" id="file_imagen" class="dropify" data-default-file="<?= Yii::getAlias('@web') .'/images/admin/crear_producto.jpg' ?>" data-show-errors="true" data-errors-position="outside" data-allowed-file-extensions="jpg jpeg" accept="image/jpeg" />
                                </div>
                                
                            </div>

                            <div class="row text-right mt-2 d-flex align-items-center">
                                <div class="col-md-12 col-12 mt-2 mt-sm-0">
                                    <?= Html::submitButton(Yii::t('app', 'Enviar pago <i class="fas fa-credit-card"></i>'), ['class' => 'btn btn-verde waves-effect waves-light col-12', "id" => "btn_pagar"]) ?>
                                </div>
                            </div>


                        <?php $form->end(); ?>

                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->


<div class="modal fade" id="modalDatosTransferencia" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalDatosTitulo"></h5>
            </div>
            <div class="modal-body text-center" id="modalDatosBody">
            
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-verde d-none" onclick="copiar()">Copiar datos</button>
            </div>
        </div>
    </div>
</div>



<script>

    $('.dropify').dropify({
        messages: {
            'default': 'Arrastra una imagen o click aqui para agregar',
            'replace': 'Arrastra una imagen o click aqui para reemplazar',
            'remove':  'Eliminar',
            'error':   'Un error ha ocurrido'
        },
        error: {
            'fileSize': 'El archivo es muy grande ({{ value }} max).',
            'minWidth': 'El ancho de imagen es muy pequeña ({{ value }}}px min).',
            'maxWidth': 'El ancho de la imagen es muy grande ({{ value }}}px max).',
            'minHeight': 'El alto de la imagen es muy pequeño ({{ value }}}px min).',
            'maxHeight': 'El alto de la imagen es muy grande ({{ value }}px max).',
            'imageFormat': 'El formato de la imagen no esta disponible, solo ({{ value }}).',
            'fileExtension': 'El archivo no esta disponible, solo ({{ value }}).'
        }
    });

    $("#_reportarpagoform-medio_pago").change(function(){
        if ($(this).val() == 3) {
            $("#modalDatosTitulo").html("Datos Bancarios");
            $("#modalDatosBody").html(`
                <p><b>Nombre:</b> MB Green spa</p>
                <p><b>Banco:</b> Banco BCI</p>
                <p><b>Tipo Cuenta:</b> Corriente</p>
                <p><b>Nro cuenta:</b> 21875189</p>
                <p><b>Rut:</b> 76.854.269-4</p>
                <p><b>Email:</b> mbgreenspan@gmail.com</p>
            `)

            $("#modalDatosTransferencia").modal("show")
        }
        if ($(this).val() == 4) {
            $("#modalDatosTitulo").html("Datos Amipass");
            $("#modalDatosBody").html(`
                <p><b>Código de comercio:</b> 2635302</p>
                <p><b>Nombre del comercio:</b> MB Green 1</p>
                <p><b>Rut:</b> 76.854.269-4</p>
                <p>En descripción coloca tu nombre y apellido</p>
            `)

            $("#modalDatosTransferencia").modal("show")
        }
        // return false;
    })

    $("#btn_pagar").click(function(){
        if ($('#file_imagen').val() != "" && $('#file_imagen').val() != null) {
            return true;
        }else{
            alertify.error(`Debe adjuntar una imagen del pago`, 5);
            return false;
        }
    })
    


    // function copiar() {
    //     var elm = document.getElementById("modalDatosBody");
    //     // for Internet Explorer

    //     if(document.body.createTextRange) {
    //         var range = document.body.createTextRange();
    //         range.moveToElementText(elm);
    //         range.select();
    //         document.execCommand("Copy");
    //         alert("Copied div content to clipboard");
    //     }
    //     else if(window.getSelection) {
    //         // other browsers

    //         var selection = window.getSelection();
    //         var range = document.createRange();
    //         range.selectNodeContents(elm);
    //         selection.removeAllRanges();
    //         selection.addRange(range);
    //         document.execCommand("Copy");
    //         alert("Copied div content to clipboard");
    //     }
    // }



</script>