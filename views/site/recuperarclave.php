<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Recuperar Clave';
$this->params['activeLink'] = "recuperar-clave";
?>

   

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12 text-center">
                <h1>Recupeara clave</h1>
            </div>

            <div class="col-12 offset-sm-4 col-sm-4 mt-3">

                <?php $form = ActiveForm::begin([
                'method' => 'post', 
                'id'=> 'RecuperarClave', 
                'enableClientValidation' => false,
                'enableAjaxValidation' => true, 
                ]); ?>

                    <?= $form->field($recupearClave, 'email', [
                    'template' => '<div class="form-group mb-0">
                                        {label}
                                        {input}
                                        <small id="emailHelp" class="form-text text-muted">Debe ingresar el correo con el cual se registro para poder restaurar su contraseña.</small>
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger']
                    ])->textInput(['class'=>'form-control', 'placeholder' => 'ejemplo@ejemplo.com'])->label() ?>

                    

                    <?= Html::submitButton(Yii::t('app', 'Enviar email'), ['class' => 'btn btn-verde col-12 submit mb-4']) ?>

                <?php $form->end(); ?>

        
            </div>
        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->