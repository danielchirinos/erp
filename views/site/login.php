<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'Clientes';
    $this->params['activeLink'] = "inicio-sesion";
?>

<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<style>
.field-logincliente-clave{
    margin-bottom: 0px;
}
</style>

<div class="container-fluid p-5">


<div class="row">

    <!-- <div class="col-md-6 text-center">

        ?php $form = ActiveForm::begin([
            'method' => 'post', 
            'id'=> 'InicioSesion', 
            'enableClientValidation' => false,
            'enableAjaxValidation' => true, 
            ]); ?>

            <h3 class="box-title text-center">Inicio de Sesión</h3>
            <div class="text-center mb-3"><ins >Solo para clientes registrados</ins></div>

                <label for="#">Email</label>
                <input type="email" class="form-control inputLogin" style="display:inline;">

                <p class="mb-3"><a href="<?= Yii::getAlias('@web') ?>/site/recuperarclave" class="text-verde ">¿Olvido su clave de acceso?</a></p>


            ?= Html::submitButton(Yii::t('app', 'Entrar'), ['class' => 'btn btn-verde col-6 submit mb-4']) ?>

        ?php $form->end(); ?>

    </div> -->

    <!-- <div class="col-md-6 text-center">

        ?php $form = ActiveForm::begin([
            'method' => 'post', 
            'action' => 'registro',
            'id'=> 'Regitro', 
            'enableClientValidation' => false,
            'enableAjaxValidation' => true, 
            ]); ?>

            <h3 class="box-title text-center">Registro</h3>
            <div class="text-center mb-3"><ins >Registrate para poder realizar tus pedidos</ins></div>

            ?= $form->field($cliente, 'nombre', [
                'template' => '<div class="form-group mb-0">
                                    {input}
                                </div>
                                {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Nombre', 'style'=>'display:inline;'])
                ->label(false) ?>
            ?= $form->field($cliente, 'apellido', [
                    'template' => '<div class="form-group mb-0">
                                        <div class="col-xs-12">
                                            {input}
                                        </div>
                                    </div>
                                    {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Apellido', 'style'=>'display:inline;'])
                ->label(false) ?>

                ?= $form->field($cliente, 'rut', [
                    'template' => '<div class="form-group mb-0">
                                        <div class="col-xs-12">
                                            {input}
                                        </div>
                                    </div>
                                    {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Rut', 'style'=>'display:inline;'])
                ->label(false) ?>

                ?= $form->field($cliente, 'email', [
                    'template' => '<div class="form-group mb-0">
                                        <div class="col-xs-12">
                                            {input}
                                        </div>
                                    </div>
                                    {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Email', 'style'=>'display:inline;'])
                ->label(false) ?>

                ?= $form->field($cliente, 'clave', [
                    'template' => '<div class="form-group mb-0">
                                        <div class="col-xs-12">
                                            {input}
                                        </div>
                                    </div>
                                    {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Clave', 'type' => 'password', 'style'=>'display:inline;'])
                ->label(false) ?>

                ?= $form->field($cliente, 'claveRepetir', [
                    'template' => '<div class="form-group mb-0">
                                        <div class="col-xs-12">
                                            {input}
                                        </div>
                                    </div>
                                    {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Repetir Clave', 'type' => 'password', 'style'=>'display:inline;'])
                ->label(false) ?>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="password" placeholder="Repetir Clave" class="form-control" name="txt_clave_repetir" id="txt_clave_repetir" style='display:inline; width: 50%'>
                    </div>
                </div>


                ?= $form->field($cliente, 'direccion', [
                    'template' => '<div class="form-group mb-0">
                                        <div class="col-xs-12">
                                            {input}
                                        </div>
                                    </div>
                                    {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Dirección', 'style'=>'display:inline;'])
                ->label(false) ?>

                ?= $form->field($cliente, 'telefono', [
                    'template' => '<div class="form-group mb-0">
                                        <div class="col-xs-12">
                                            {input}
                                        </div>
                                    </div>
                                    {error}{hint}',
                    'errorOptions'=>['class'=>'badge badge-danger']
                ])->textInput(['class'=>'form-control inputLogin', 'placeholder' => 'Teléfono', 'style'=>'display:inline;'])
                ->label(false) ?>


            <button type="button" id="btn_registrar" class="btn btn-verde col-6 submit mb-4">Registrar</button>
            <= Html::submitButton(Yii::t('app', 'Registrar'), ['class' => 'btn btn-verde col-6 submit mb-4']) ?>

            

        ?php $form->end(); ?>

    </div> -->

</div>

</div>


<script>
    $("#clientes-rut").inputmask("[9]{1,8}-[9|k]{1}");

    $("#btn_registrar").click(function(){
        if (checkRut($("#clientes-rut").val()) == false) {
            alertify.notify(`Debes ingresar un rut valido para continuar`, "error", 3);
        }else{
            $("#Regitro").submit();
        }
    })

    function checkRut(rut) {

        // Despejar Puntos
        var valor = rut.replace('.','');
        // Despejar Guión
        valor = valor.replace('-','');

        // Aislar Cuerpo y Dígito Verificador
        cuerpo = valor.slice(0,-1);
        dv = valor.slice(-1).toUpperCase();

        // Formatear RUN
        rut = cuerpo + '-'+ dv

        // Si no cumple con el mínimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7) { 
            // rut.setCustomValidity("RUT Incompleto"); 
            return false;
        }

        // Calcular Dígito Verificador
        suma = 0;
        multiplo = 2;

        // Para cada dígito del Cuerpo
        for(i=1;i<=cuerpo.length;i++) {

            // Obtener su Producto con el Múltiplo Correspondiente
            index = multiplo * valor.charAt(cuerpo.length - i);
            
            // Sumar al Contador General
            suma = suma + index;
            
            // Consolidar Múltiplo dentro del rango [2,7]
            if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

        }

        // Calcular Dígito Verificador en base al Módulo 11
        dvEsperado = 11 - (suma % 11);

        // Casos Especiales (0 y K)
        dv = (dv == 'K')?10:dv;
        dv = (dv == 0)?11:dv;

        // Validar que el Cuerpo coincide con su Dígito Verificador
        if(dvEsperado != dv) { 
            // rut.setCustomValidity("RUT Inválido"); 
            return false; 
        }

        // Si todo sale bien, eliminar errores (decretar que es válido)
        // rut.setCustomValidity('');
    }
</script>
