<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->params['activeLink'] = "error";
?>
<div class="site-error">

    <div class="container">
        <?php Html::encode($this->title) ?>
        <h1>Error</h1>

        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>

        <p>
            Se ha producido un error cuando se ha hecho la solicitud.
        </p>

        <div class="row">

            <div class="col-12 text-center">
                <h2>
                    ¿Que quieres hacer?
                </h2>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-6 col-12 text-center">
                <a class="btn btn-primary btn-verde mb-3 float-md-right " href="<?= Yii::getAlias('@web') ?>/admin/index">Volver al inicio <i class="fas fa-home"></i></a>
            </div>
        </div>

    
    </div>


</div>
