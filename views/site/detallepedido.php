<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Detalle del pedido';
$this->params['activeLink'] = "detalle-pedido";
?>

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/compra.css">

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container">

        <div class="row">
            <div class="col-md-2">
                <label for=""><b>Estado</b></label><br>
                <?= ($pedidoscab->id_estadopedido == 1) ? '<span class="badge badge-info">'.$pedidoscab->estadopedido->estado.'</span>' : '<span class="badge badge-success">'.$pedidoscab->estadopedido->estado.'</span>'  ?>
            </div>
            <div class="col-md-2">
                <label for=""><b>Comuna de despacho</b></label><br>
                <span><?= $pedidoscab->comuna->nombre ?></span>
            </div>
            <div class="col-md-2">
                <label for=""><b>Cantidad de productos</b></label><br>
                <span><?= $pedidoscab->cantidad_productos ?></span>
            </div>
            <div class="col-md-2">
                <label for=""><b>Precio Total</b></label><br>
                <span><?= $pedidoscab->precio_total ?></span>
            </div>
            <div class="col-md-2">
                <label for=""><b>Dirección de despacho</b></label><br>
                <span><?= $pedidoscab->direccion_despacho ?></span>
            </div>
            <div class="col-md-2">
                <label for=""><b>Fecha de despacho</b></label><br>
                <div class="d-flex justify-content-between">
                    <span><?= $pedidoscab->fecha_despacho ?></span>
                    <?php if ($pedidoscab->id_estadopedido != 1) { ?>
                        <span title="Editar Fecha"><button class="btn btn-verde btn-sm" onclick="editarFecha()"><i class="fas fa-edit"></i></button> </span>
                    <?php } ?>
                </div>
            </div>
        </div>

        <hr>

        <div class="row float-right">
            <?php if (count($pedidoslin) > 0) { ?>
                <?php if ($pedidoscab->id_estadopedido == 3 || $pedidoscab->id_estadopedido == 4) { ?>
                    <!-- si la fecha de despacho es menor a la fecha actual se envia un aviso que debe cambiar la fecha -->
                    <?php if(strtotime($pedidoscab->fecha_despacho) < strtotime(date("Y-m-d"))){ ?>
                        <button class="btn btn-verde btn-sm mr-3" onclick="alertify.warning('La fecha de despacho es menor a la fecha actual, edite la fecha');">
                            <i class="fas fa-credit-card"></i> Pagar
                        </button>
                    <?php }else{ ?>

                        <!-- si el pedido es menor a 10mil se envio a compra y de alli se enviara directamente a index para que siga agregando productos -->
                        <?php if ($precioTotalPedido < 10000) { ?>
                            <a class="btn btn-info btn-sm mr-3" href="<?= Yii::getAlias('@web'). '/site/comprar/'.$pedidoscab->id ?>"><i class="fas fa-lemon"></i> Seguir Comprando</a>

                            <!-- boton pagar deshabilitado, solo muestra información -->
                            <a class="btn btn-secondary btn-sm mr-3" href="javascript:void(0)" onclick="notificacion()">
                                <i class="fas fa-credit-card"></i> Pagar
                            </a>
                        <?php  }else{ ?>
                            <!-- no se usa este boton, envia a un resumen de pedido para una rapida compra -->
                            <a class="btn btn-verde btn-sm mr-3 d-none" href="<?= Yii::getAlias('@web'). '/site/confirmacionpedido/'.$pedidoscab->id ?>"><i class="fas fa-credit-card"></i> Pagar</a>

                            <?php if ($banderaFechaBloqueada == 1) { ?>
                                <a class="btn btn-verde btn-sm mr-3" href="#" data-toggle="modal" data-target="#modalAvisoFechaBloqueada">
                                    <i class="fas fa-credit-card"></i> Pagar
                                </a>
                            <?php }else{ ?>
                                <a class="btn btn-verde btn-sm mr-3" href="<?= Yii::getAlias('@web'). '/site/comprar/'.$pedidoscab->id ?> "><i class="fas fa-credit-card"></i> Pagar</a>
                            <?php } ?>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            
            <a class="btn btn-primary btn-sm mr-3" href="<?= Yii::getAlias('@web'). '/site/perfilpedidos/' ?>"><i class="fas fa-arrow-left"></i> Volver</a>
        </div>

        <div class="clearfix"></div>
        <br>

        <div class="row">
            <div class="col-md-12">
            <div class="inner-sec-shop">
                <div class="checkout-right">
                    <table class="timetable_sub">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Descripción</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Disponible</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php  foreach ($pedidoslin as $key => $value) { ?>

                            <tr class="rem1">
                                <td><img src="<?= Yii::getAlias('@web') ?>/images/<?=  ($value->producto->imagen == null) ? "/productos/producto.jpg" : "thum/".$value->producto->imagen ?>" width="50" alt="" class="img-responsive"></td>
                                <td><?= $value->producto->descripcion?></td>
                                <td><?= $value->cantidad ?></td>
                                <td><?= $value->precio ?></td>
                                
                                <td>
                                <span class="badge badge-<?= $value->producto->activo == 1 ? 'success' : 'danger' ?>">
                                    <?= $value->producto->activo == 1 ? 'Disponible' : 'No disponible' ?>
                                </span></td>
                            </tr>

                        <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>

        

    </div>
</section>
<!-- end detalle pedido -->


<!-- Modal -->
<div class="modal fade" id="modalEditarFecha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Fecha de pedido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php $form = ActiveForm::begin([
                    'method' => 'post', 
                    'id'=> 'detallePedidoCambiarFecha', 
                    'action' => Url::to(['site/detallepedidocambiarfecha']),
                    'options'=> [
                        'class' => 'form-horizontal',
                    ], 
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => false, 
                    ]); ?>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-8">
                            <?= $form->field($modelFechaDespacho, 'fechaDespacho', ['template' => '{label}
                            <div class="form-group mb-0">
                                <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                    {input}
                                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            {error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                            ])->textInput(['class'=>'form-control', 'placeholder' => 'dia / mes / año', 'readonly'=>'true', "type" => "text", "value" => ""]) ?>
                        </div>

                        <input type="hidden" name="txt_id_pedido" value="<?= $pedidoscab->id ?>">

                        
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-verde waves-effect waves-light" onclick="enviarFormulario()">Actualizar Fecha</button>
                    <?= Html::submitButton(Yii::t('app', 'Actualizar Fecha'), ['class' => 'd-none btn btn-verde waves-effect waves-light']) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalAvisoFechaBloqueada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Aviso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    La fecha del pedido ya no esta disponible, pruebe con una nueva fecha de despacho
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<script>

    $(function () {
        $('#datetimepicker4').datetimepicker({
            locale: 'es',
            format: 'L',
            defaultDate: "",
            ignoreReadonly: true,
            // debug : true,
            disabledDates: [
                "<?= $mesActual ?>",
                "<?= $mesSiguiente ?>",
                "<?= $maximoPedidos ?>",
                "<?= $bloqueInmediato ?>"
            ],
            minDate: "<?= date("m/d/Y", strtotime('+ 1 day')) ?>",
            maxDate: "<?= date('m/d/Y', strtotime('last day of next month')); ?>"
        });

        $('#_actualizarfechadespachoform-fechadespacho').val("")
        debugger;
    });

    function editarFecha(){
        $("#modalEditarFecha").modal("show");
    }

    function notificacion(){
        alertify.warning('No puede ir a pagar porque el monto total del pedido es menor a 10.000, intente seguir comprando', 5); 
    }


    function enviarFormulario(){
        if ($('#_actualizarfechadespachoform-fechadespacho').val() != "") {
            $("#detallePedidoCambiarFecha").submit();
        }else{
            alertify.warning('Debe elegir una fecha del calendario para actualizar la fecha de despacho', 5);
        }
    }
</script>

