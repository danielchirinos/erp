<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Recuperar Clave';
$this->params['activeLink'] = "recuperar-clave";
?>


<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>
   

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12 text-center">
                <h1>Recuperar clave</h1>
            </div>

            <div class="col-12 offset-sm-4 col-sm-4 mt-3">

                <?php $form = ActiveForm::begin([
                'method' => 'post', 
                'id'=> 'ActualizarClave', 
                'action' => Url::to(['site/actualizarclavecliente']),
                'enableClientValidation' => false,
                'enableAjaxValidation' => true, 
                ]); ?>

                    <?php
                    $dNoneCliente = "";
                    if($cliente->rut != "" || $cliente->rut != null){
                        $dNoneCliente = "d-none";
                    } ?>

                        <?= $form->field($actualizarClave, 'rut', [
                        'template' => '<div class="form-group mb-0 '.$dNoneCliente.'">
                                            {label}
                                            {input}
                                        </div>
                                        {error}{hint}',
                            'errorOptions'=>['class'=>'badge badge-danger']
                        ])->textInput(['class'=>'form-control', 'placeholder' => '', 'value' => $cliente->rut])->label() ?>


                    <?= $form->field($actualizarClave, 'clave', [
                    'template' => '<div class="form-group mb-0">
                                        {label}
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger']
                    ])->textInput(['class'=>'form-control', 'placeholder' => ''])->label() ?>

                    <?= $form->field($actualizarClave, 'claveRepetir', [
                    'template' => '<div class="form-group mb-0">
                                        {label}
                                        {input}
                                    </div>
                                    {error}{hint}',
                        'errorOptions'=>['class'=>'badge badge-danger']
                    ])->textInput(['class'=>'form-control', 'placeholder' => ''])->label() ?>
                    
                    <input type="hidden" name="txt_token" value="<?= $id ?>">
                    <input type="hidden" name="txt_id_cliente" value="<?= $cliente->id ?>">

                    <button class="btn btn-verde col-12 submit mb-4" type="button" onclick="actualizarRut()"> Actualizar clave</button>
                    

                <?php $form->end(); ?>

        
            </div>
        </div>
    </div>
</section>
<!-- end tips frutas verduras ofertas -->


<script>

    $("#_actualizarclaveform-rut").inputmask("[9]{1,8}-[9|k]{1}");

    function actualizarRut(){
        if ($("#_actualizarclaveform-rut").val() != "") {	
            if (checkRut($("#_actualizarclaveform-rut").val()) == false) {
                alertify.notify(`Debes ingresar un rut valido para continuar`, "error", 3);
            }else{
                $("#ActualizarClave").submit()
            }
        }else{
            alertify.notify(`Ingresa tu rut`, "error", 3);
        }
    }

    function checkRut(rut) {

        // Despejar Puntos
        var valor = rut.replace('.','');
        // Despejar Guión
        valor = valor.replace('-','');

        // Aislar Cuerpo y Dígito Verificador
        cuerpo = valor.slice(0,-1);
        dv = valor.slice(-1).toUpperCase();

        // Formatear RUN
        rut = cuerpo + '-'+ dv

        // Si no cumple con el mínimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7) { 
            // rut.setCustomValidity("RUT Incompleto"); 
            return false;
        }

        // Calcular Dígito Verificador
        suma = 0;
        multiplo = 2;

        // Para cada dígito del Cuerpo
        for(i=1;i<=cuerpo.length;i++) {

            // Obtener su Producto con el Múltiplo Correspondiente
            index = multiplo * valor.charAt(cuerpo.length - i);
            
            // Sumar al Contador General
            suma = suma + index;
            
            // Consolidar Múltiplo dentro del rango [2,7]
            if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

        }

        // Calcular Dígito Verificador en base al Módulo 11
        dvEsperado = 11 - (suma % 11);

        // Casos Especiales (0 y K)
        dv = (dv == 'K')?10:dv;
        dv = (dv == 0)?11:dv;

        // Validar que el Cuerpo coincide con su Dígito Verificador
        if(dvEsperado != dv) { 
            // rut.setCustomValidity("RUT Inválido"); 
            return false; 
        }

        // Si todo sale bien, eliminar errores (decretar que es válido)
        // rut.setCustomValidity('');
    }
</script>