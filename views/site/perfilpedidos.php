<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Perfil';
$this->params['activeLink'] = "perfil-pedidos";
?>



<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/compra.css">

<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/css/responsive.bootstrap4.min.css">

<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datatable/Responsive-2.2.3/js/responsive.bootstrap4.min.js"></script>


<style>
.pagination{
    justify-content: center !important;
}
.paginate_button{
    cursor:pointer;

    color:var(--verde);
    border: 1px solid var(--verde);
}
.paginate_button.current{
    background-color: var(--rojo);
    border: 1px solid var(--rojo);
    color:white;
}
table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before {
    background-color: var(--verde);
}
</style>

<!-- tips frutas verduras ofertas -->	
<section class="banner-bottom-mbgreen py-lg-5 py-3">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-secondary btn-verde btn-sm float-right" href="<?= Yii::getAlias('@web') ?>/site/perfil">
                    <i class="fas fa-user"></i> Ir a mi perfil
                </a>
            </div>
        </div>
        <br>
        <div class="inner-sec-shop">
            <div class="checkout-right">
                <table id="example23" class="display table table-hover table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha de despacho</th>
                            <th>Dirección despacho</th>
                            <th>Comuna</th>
                            <th>Cantidad de productos</th>
                            <th>Precio total</th>
                            <th>Estado</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php  foreach ($pedidos as $key => $value) { ?>
                        
                        <?php 
                            //se le resta 1 dia  a la fecha de despacho y se le agregan las 9 pm
                            $fecha_anterior = date("Y-m-d 21:00:00", strtotime($value->fecha_despacho. "-1 day"));

                            //se busca el entero de la fecha anterior y la fecha actual
                            $fant = strtotime($fecha_anterior);
                            $fact = strtotime(date("Y-m-d H:i:s"));
                            
                            $hora_editar = 0;
                            //si fecha anterior es menos a fecha actual, se asigna hora_editar a 1 y no se mustra el boton
                            if ($fant < $fact) {
                                $hora_editar = 1;
                            }

                            if ($value->id_estadopedido == 1) {
                                // pagado
                                $badge = "success";
                            }elseif ($value->id_estadopedido == 2) {
                                // entregado
                                $badge = "primary";
                            }elseif ($value->id_estadopedido == 3) {
                                // rechazado
                                $badge = "danger";
                            }elseif ($value->id_estadopedido == 4) {
                                //guardado
                                $badge = "info";
                            }

                        ?>
                        <tr class="rem1">
                            <td><?= $value->fecha_despacho ?></td>
                            <td><?= $value->direccion_despacho ?></td>
                            <td><?= $value->comuna->nombre?></td>
                            <td><?= $value->cantidad_productos ?></td>
                            <td><?= $value->precio_total ?></td>
                            <td><?= '<span class="badge badge-'.$badge.'">'.$value->estadopedido->estado.'</span>' ?></td>
                            
                            
                            <td>
                                <a href="<?= Yii::getAlias('@web'). '/site/detallepedido/'.$value->id ?>" class="btn btn-info btn-sm" title="Ver pedido"><i class="fa fa-list"></i></a> 

                                <?php //if ($value->id_estadopedido == 1 && $hora_editar == 0) { ?>
                                <?php if (1==2) { ?>
                                    <a href="<?= Yii::getAlias('@web'). '/site/editarpedido/'.$value->id ?>" class="btn btn-success btn-sm" ><i class="fa fa-edit" title="Editar pedido"></i></a> 
                                <?php } ?>

                                <?php if ($value->id_estadopedido == 2) { ?>
                                    <a href="<?= Yii::getAlias('@web'). '/site/hacerpedidosimilar/'.$value->id ?>" class="d-none btn btn-primary btn-sm"  title="Pedir similar"><i class="fa fa-redo"></i></a> 
                                <?php } ?>
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

            </div>
        </div>

    </div>
</section>
<!-- end tips frutas verduras ofertas -->



<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtp',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "order": [[ 0, "desc" ]]
        // buttons: [
        //     'copy', 'csv', 'excel', 'pdf', 'print'
        // ]
    });
});
</script>
