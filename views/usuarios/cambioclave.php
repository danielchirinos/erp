<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Editar Cliente'; 
    $this->params['activeLink'] = "clientes-lista";
?>

<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>



<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/clientes/lista">Listado de clientes</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'editarCliente', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ], 
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'clave', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Nueva Clave', "value" => ""]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'rut', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Rut']) ?>
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light']) ?>
                                    
                                </div>
                            </div>
                            
                        </div>

                <?php $form->end(); ?>
            
        </div>
    </div>
</div>




<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: "<'row'<'col-6' B><'col-6' f>>rtip",
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            'excel',
        ]
    });
});
</script>