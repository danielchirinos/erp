<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Listado de Usuarios'; 
    $this->params['activeLink'] = "usuarios-lista";
?>

<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- datatables -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jszip.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/buttons.html5.min.js"></script>


<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/usuarios/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
        

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($usuarios)?></span></span>
            <div  class="d-flex justify-content-end mt-5">
                <a href="<?= Yii::getAlias('@web'); ?>/usuarios/crear">
                    <button class="btn btn-success text-light">
                        <i class="fa fa-plus"></i> Crear Usuario
                    </button>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <?php if (count($usuarios) == 0) { ?>
            <span>Sin usurios</span>
        <?php }else { ?>
        
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Lista de usuarios</h4>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Usuario</th>
                                        <th>Tipo de Usuario</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($usuarios as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value->id ?></td>
                                        <td><?= $value->usuario ?></td>
                                        <td><?= $value->tipoUsuario->tipo ?></td>
                                        <td>
                                            <span class="badge badge-<?= $value->activo == 1 ? "success" : "danger" ?>">
                                                <?= $value->activo == 1 ? "Activo" : "Inactivo" ?>
                                            </span>
                                        </td>   
                                        <td class="text-center">
                                            <a  href="<?= Yii::getAlias('@web') ?>/usuarios/editar/<?= $value->id?>" title="Editar">
                                                <i class="fal fa-edit"></i>
                                            </a>
                                        </td>                                     
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.14.0-beta3/js/bootstrap-select.min.js" integrity="sha512-yrOmjPdp8qH8hgLfWpSFhC/+R9Cj9USL8uJxYIveJZGAiedxyIxwNw4RsLDlcjNlIRR4kkHaDHSmNHAkxFTmgg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    
<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            { extend: 'excel', className: 'btn' }
        ]

    });
});
</script>