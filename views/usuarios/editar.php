<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Editar Usuarios'; 
    $this->params['activeLink'] = "usuarios-lista";
?>
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/usuarios/lista">Listado de Usuarios</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'editarCliente', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ], 
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'usuario', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Usuario']) ?>
                            </div>
                            <div class="col-md-6">
                                <label for="password">Nueva Contraseña</label>
                                <input type="password" class="form-control" name="password" placeholder="Nueva Contraseña">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'id_tipo_usuario', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->label('Tipo de Usuario')->dropDownList($tipoUsuario, ['class'=>'form-control', 'placeholder' => 'Descripción']) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'activo', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->label('Estado')->dropDownList([1 => "Activo", 0 => "Inactivo"], ['class'=> 'form-control', 'placeholder' => 'Descripción']) ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <?= Html::submitButton(Yii::t('app', '<i class="fa fa-check"></i> Guardar'), ['class' => 'btn btn-success waves-effect waves-light']) ?>
                                    <a href="<?= Yii::getAlias('@web'); ?>/usuarios/lista">
                                        <button type="button" class="btn btn-info text-light">
                                            <i class="fa fa-arrow-left"></i> Volver
                                        </button>
                                    </a>
                                </div>
                            </div>
                            
                        </div>

                <?php $form->end(); ?>
            
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    /* $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            'excel',
        ]
    }); */

});
</script>