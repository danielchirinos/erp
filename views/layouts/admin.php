<?php


use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
$Pagina = $this->params['activeLink'];
$version = "0.1";
$session = Yii::$app->session;
$Usuario = $session['nombre']; 

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" type="image/png" sizes="16x16" href="">

    <!-- Bootstrap Core CSS -->
    <link href="<?= Yii::getAlias('@web') ?>/content/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= Yii::getAlias('@web') ?>/content/admin/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?= Yii::getAlias('@web') ?>/content/admin/css/colors/blue.css" id="theme" rel="stylesheet">

    
    <link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/alertifyjs/css/alertify.min.css" />
    <link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/alertifyjs/css/themes/bootstrap.css" />
    
    <script src="<?= Yii::getAlias('@web') ?>/content/alertifyjs/alertify.min.js"></script>
    <script>
        alertify.set('notifier','delay', 8);
        alertify.set('notifier','position', 'top-right');     
    </script>
    


</head>
<body class="fix-header fix-sidebar card-no-border">
<?php $this->beginBody() ?>
    <div id="main-wrapper">
        
        <!-- Topbar header - style you can find in pages.scss -->
        
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                
                <!-- Logo -->
                
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= Yii::getAlias('@web') ?>/admin/index">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                             <img src="?= Yii::getAlias('@web') ?>/images/imageserp.jpg" width="50" alt="" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="" alt="" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>
                         <!-- dark Logo text -->
                         <!-- <img src="" alt="" class="dark-logo" /> -->
                         <!-- Light Logo text -->    
                         <!-- <img src="" class="light-logo" alt="" />-->
                         ERP 
                        </span> </a> 
                </div>
                
                <!-- End Logo -->
                
                <div class="navbar-collapse">
                    
                    <!-- toggle and nav items -->
                    
                    <ul class="navbar-nav mr-auto mt-md-0 ">

                        <li class="nav-item"> <a class="nav-link sidebartoggler text-muted waves-effect waves-dark" href="javascript:void(0)">
                            <i class="fas fa-bars"></i>
                            <!-- <i class="icon-arrow-left-circle"></i> -->
                        </a> 
                        </li>
                    </ul>
                    
                    <!-- User profile and search -->
                    
                    <ul class="navbar-nav my-lg-0">
  
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?= Yii::getAlias('@web') ?>/images/admin/user.png" alt="usuario" class="profile-pic" />
                                <?= $Usuario; ?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4>Hola, <?= $Usuario ?></h4>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="<?= Yii::getAlias('@web') ?>/login/logout"><i class="fa fa-power-off"></i> Cerrar sesión</a></li>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>
        
        <!-- End Topbar header -->
        
        
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        

        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">

                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li>
                            <a class="" href="<?= Yii::getAlias('@web') ?>/admin/index ">
                                <i class="fas fa-home"></i>
                                <span class="hide-menu active">Inicio</span>
                            </a>
                        </li>
                  
                        <!-- ?= $Pagina=='pedidos-lista' ? 'active' : $Pagina=='pedidos-compras' ? 'active' : $Pagina=='pedidos-detalle' ? 'active' : '' ?> -->
                        
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false">
                                <i class="fas fa-plus"></i>
                                <span class="hide-menu">Productos</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li class="<?= $Pagina=='producto-crear' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='producto-crear' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/productos/crear">Crear</a>
                                </li>
                                <li class="<?= $Pagina=='producto-crear' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='producto-crear' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/productos/lista">Lista</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false">
                                <i class="fas fa-flag-alt"></i>
                                <span class="hide-menu">Pedidos</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li class="<?= $Pagina=='pedidos-crear' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='pedidos-crear' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/pedidos/compra">Crear</a>
                                </li>
                                <li class="<?= $Pagina=='pedidos-lista' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='pedidos-lista' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/pedidos/lista">Lista</a>
                                </li>
                            </ul>
                        </li>

                        <!-- <li class="?= $Pagina=='producto-crear' ? 'active' : $Pagina=='producto-lista' ? 'active': '' ?>"> -->
                        <li> 
                            <a class="has-arrow" href="#" aria-expanded="false">
                                <i class="fa fa-cash-register"></i>
                                <span class="hide-menu">Cierre Caja</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li class="<?= $Pagina=='producto-crear' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='producto-crear' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/cierre/lista">Listado</a>
                                </li>
                            </ul>
                        </li>
                        <li> 
                            <a class="has-arrow" href="#" aria-expanded="false">
                                <i class="fas fa-users"></i>
                                <span class="hide-menu">Clientes</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li class="<?= $Pagina=='clientes-crear' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='clientes-crear' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/clientes/crear">Crear</a>
                                </li>
                                <li class="<?= $Pagina=='clientes-lista' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='clientes-lista' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/clientes/lista">Listado</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li> 
                            <a class="has-arrow" href="#" aria-expanded="false">
                                <i class="fas fa-user"></i>
                                <span class="hide-menu">Usuarios</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li class="<?= $Pagina=='usuarios-lista' ? 'active' : '' ?>">
                                    <a class="<?= $Pagina=='usuarios-lista' ? 'active' : '' ?>" href="<?= Yii::getAlias('@web') ?>/usuarios/lista">Listado</a>
                                </li>
                            </ul>
                        </li>


                        <!-- ?= $Pagina=='diasbloqueados-crear' ? 'active' : $Pagina=='diasbloqueados-lista' ? 'active': '' ?> -->

                        <!-- ?= $Pagina=='cupon-crear' ? 'active' : $Pagina == 'cupon-lista' ? 'active': '' ?> -->

                        <!-- ?= $Pagina=='dte-listado' ? 'active' :  '' ?> -->

                        <!-- <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Inicio <span class="label label-rounded label-success">5</span></span></a>

                        </li> -->
                        <!-- <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Inicio <span class="label label-rounded label-success">5</span></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="index.html">Modern Dashboard</a></li>
                                <li><a href="index2.html">Awesome Dashboard</a></li>
                                <li><a href="index3.html">Classy Dashboard</a></li>
                                <li><a href="index4.html">Analytical Dashboard</a></li>
                                <li><a href="index5.html">Minimal Dashboard</a></li>
                            </ul>
                        </li> -->

                        <!-- <li>
                            <a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-arrange-send-backward"></i><span class="hide-menu">Multi level dd</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="#">item 1.1</a></li>
                                <li><a href="#">item 1.2</a></li>
                                <li>
                                    <a class="has-arrow" href="#" aria-expanded="false">Menu 1.3</a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="#">item 1.3.1</a></li>
                                        <li><a href="#">item 1.3.2</a></li>
                                        <li><a href="#">item 1.3.3</a></li>
                                        <li><a href="#">item 1.3.4</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">item 1.4</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->

        </aside>
        
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        

        
        <!-- Page wrapper  -->
        
        <div class="page-wrapper">
            
            <?= $content ?>

            <!-- footer -->
            <footer class="footer" style="position:fixed"> <?= date("Y"); ?> - ERP Oscar </footer>
            <!-- End footer -->
           
        </div>
        
        <!-- End Page wrapper  -->
        
    </div>

    

<?php $this->endBody() ?>
<?php $this->endPage() ?>


    <script src="<?= Yii::getAlias('@web') ?>/content/admin/bootstrap/js/popper.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/js/custom.min.js"></script>
    
    <!-- This page plugins -->
    

    
    <!-- Style switcher -->
    
    <script src="<?= Yii::getAlias('@web') ?>/content/admin/styleswitcher/jQuery.style.switcher.js"></script>

    <script>
        setTimeout(() => {
            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                alertify.success('<?= Yii::$app->session->getFlash('success'); ?>');
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('warning')) { ?>
                alertify.warning('<?= Yii::$app->session->getFlash('warning'); ?>');
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('error')) { ?>
                alertify.error('<?= Yii::$app->session->getFlash('error'); ?>');
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('notify')) { ?>
                alertify.notify('<?= Yii::$app->session->getFlash('notify'); ?>', "info");
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('message')) { ?>
                alertify.message('<?= Yii::$app->session->getFlash('message'); ?>');
            <?php } ?>
        }, 1000);
    </script>


</body>
</html>


