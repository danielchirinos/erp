<?php


use yii\helpers\Html;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
$Pagina = $this->params['activeLink'];
$campo = isset($this->params['campo']) ? $this->params['campo'] : "" ;
$session = Yii::$app->session;
$idCliente = $session['IdCliente']; 
$nombreCliente = $session['nombreCliente']; 
$version = "1.11";

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144159111-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-144159111-1');
    </script>

    <title><?= $this->title ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="2Xf0NGIcXBQdnUhP3wMveC5a3EAG151FnB_i3xEHxi4" />
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>

 
    <!-- meta para cambiar el color del navegado en los telefonos -->
    <meta name="theme-color" content="#22824B" />

    <!-- Favicon -->
    <link rel="icon" href="<?= Yii::getAlias('@web') ?>/images/favicon-96x96.png" type="image/x-icon" />


    <script>
        $.get("<?= Yii::getAlias('@web') ?>/css/bootstrap.min.css?v<?= $version ?>",function(data){
            $("head").prepend("<style>"+ data +"</style>");
        }); 
    </script>

    

    <!-- <link href="/css/bootstrap.min.css" rel='stylesheet' type="text/css" media="screen"/> -->
	<link href="<?= Yii::getAlias('@web') ?>/css/login_overlay.css" rel='stylesheet' type="text/css" media="none"/>
	<link href="<?= Yii::getAlias('@web') ?>/css/style6.css" rel='stylesheet' type="text/css" media="none"/>
	<link href="<?= Yii::getAlias('@web') ?>/css/shop.css?v<?= $version ?>" rel="stylesheet" type="text/css" media="none"/>
	<link href="<?= Yii::getAlias('@web') ?>/css/owl.carousel.css" rel="stylesheet" type="text/css" media="none" />
	<link href="<?= Yii::getAlias('@web') ?>/css/owl.theme.css" rel="stylesheet" type="text/css" media="none" />
	<link href="<?= Yii::getAlias('@web') ?>/css/style.css?v<?= $version ?>" rel='stylesheet' type="text/css" media="none"/>
	<link href="<?= Yii::getAlias('@web') ?>/css/fontawesome-all.css" rel="stylesheet" type="text/css" media="none"/>
    <link href="<?= Yii::getAlias('@web') ?>/css/social.css?v<?= $version ?>" rel="stylesheet" type="text/css"media="none"/>
    <link href="https://cdn.datatables.net/1.12.0/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"media="none" />
    <!-- <link href="/css/preloader.css" rel="stylesheet" type="text/css" /> -->
    

    <link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/content/datetimepicker/tempusdominus-bootstrap-4.min.css" media="none" />

    <script type="text/javascript" src=" https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    
    <script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datetimepicker/moment.min.js"></script>
    <script type="text/javascript" src="<?= Yii::getAlias('@web') ?>/content/datetimepicker/tempusdominus-bootstrap-4.min.js"></script>
    

</head>
<body >
<?php $this->beginBody() ?>


    <?= $content ?>



</body>
</html>


