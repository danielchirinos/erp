<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de Boletas'; 
    $this->params['activeLink'] = "dte-lista";
?>

<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- datatables -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jszip.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/buttons.html5.min.js"></script>


<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($boletas)?></span></span>
        </div>
    </div>

    <div class="row">
        <?php if (count($boletas) == 0) { ?>
            <span>Sin boletas</span>
        <?php }else { ?>
        
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Lista de Boletas</h4>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Pedido</th>
                                        <th>Fecha generacion temporal</th>
                                        <th>Fecha generacion real</th>
                                        <th>Fecha envio email</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($boletas as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value->id_pedido ?></td>
                                        <td><?= $value->fecha_generacion_temporal ?></td>
                                        <td><?= $value->fecha_generacion_real ?></td>
                                        <td><?= $value->fecha_envio_email ?></td>
                                        <!-- <td>
                                            <span class="badge badge-<= $value->activo == 1 ? "success" : "danger" ?>">
                                                <= $value->activo == 1 ? "Activo" : "Inactivo" ?>
                                            </span>
                                        </td>    -->
                                        <td class="text-center">
                                            <?php if ($value->envio_email == 0) { ?>
                                                <a  href="<?= Yii::getAlias('@web') ?>/libredte/reenviar/<?= $value->id_pedido ?>" title="Reprocesar boleta">
                                                    <i class="fal fa-share"></i>
                                                </a>
                                            <?php }else{ ?>
                                                <a class="text-danger" target="_blank"  href="<?= Yii::getAlias('@web') ?>/libredte/verboleta/<?= $value->id_pedido ?>" title="Ver boleta">
                                                <i class="fal fa-file-pdf"></i>
                                                </a>
                                            <?php } ?>
                                        </td>                                     
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>




<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            { extend: 'excel', className: 'btn' }
        ],
        "order": [[ 0, "desc" ]]

    });
});
</script>