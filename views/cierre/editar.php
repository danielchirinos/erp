<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Crear Cierre'; 
    $this->params['activeLink'] = "cierre-crear";
?>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/clientes/lista">Listado de clientes</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'crearCierre', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ]
                        ]); ?>

                        <div class="row">
                            <div class="col-md-3">
                                <label for=""> Fecha </label><br>
                                <input class="form-control" type="text" name="txt_fecha" id="" value="<?= date("Y-m-d H:i:s") ?>" readonly>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'total_cierre', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'total cierre', "type" => "number", "value" => $totalCierre]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'nombre_persona', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'Nombre Persona']) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'efectivo', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'total cierre', "type" => "number", "value" => 0]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'debito', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'total cierre', "type" => "number", "value" => 0]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'credito', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'total cierre', "type" => "number", "value" => 0]) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'transferencia', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']])->textInput(['class'=>'form-control', 'placeholder' => 'total cierre', "type" => "number", "value" => 0]) ?>
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success waves-effect waves-light" onclick="guardar()"><i class="fa fa-check"></i> Guardar </button>
                                    
                                </div>
                            </div>
                            
                        </div>

                <?php $form->end(); ?>
            
        </div>
    </div>
</div>


<script>

function guardar(){
    // < foreach ($medioPago as $key => $value) { ?>
    //     if(isNaN($("#txt_<= strtoupper($value->descripcion) ?>").val()) )
    //         alertify.error("El campo <= $value->descripcion ?> debe ser un numero ");
    // < } ?>

    $("#crearCierre").submit()
}

</script>