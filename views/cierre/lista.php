<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de Cierres de caja'; 
    $this->params['activeLink'] = "cierre-lista";
?>

<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- datatables -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jszip.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/buttons.html5.min.js"></script>


<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/cierre/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($cierreCajaCab)?></span></span>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <a class="btn btn-custom" href="<?= Yii::getAlias('@web'); ?>/cierre/crear"> <i class="fas fa-plus"> </i> Crear Cierre</a>
                    <hr>
                    <h4 class="card-title">Lista de Cierres de caja</h4>
                    <div class="table-responsive m-t-40">
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Usuario</th>
                                    <th>Efectivo</th>
                                    <th>Debito</th>
                                    <th>Crédito</th>
                                    <th>Transferencia</th>
                                    <th>Total Cierre</th>
                                    <th>Fecha de cierre</th>
                                    <th>Nombre Persona</th>
                                    <th class="d-none"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($cierreCajaCab as $key => $value) { ?>
                                <tr>
                                    <td><?= $value->id ?></td>
                                    <td><?= $value->usuario->usuario ?></td>
                                    <td><?= $value->efectivo ?></td>
                                    <td><?= $value->debito ?></td>
                                    <td><?= $value->credito ?></td>
                                    <td><?= $value->transferencia ?></td>
                                    <td><?= $value->total_cierre ?></td>
                                    <td><?= $value->fecha_cierre ?></td>
                                    <td><?= $value->nombre_persona ?></td>
                                    <td class="text-center d-none">
                                        <a  href="<?= Yii::getAlias('@web') ?>/cierre/editar/<?= $value->id?>" title="Editar">
                                            <i class="fal fa-edit"></i>
                                        </a>
                                    </td>                                     
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            { extend: 'excel', className: 'btn' }
        ]

    });
});

</script>