<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;

?>
<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/clientes/lista">Listado de clientes</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card card-body">
                <?php $form = ActiveForm::begin([
                        'method' => 'post', 
                        'id'=> 'crearCliente', 
                        'options'=> [
                            'class' => 'form-horizontal',
                            'enctype'=>"multipart/form-data",
                        ], 
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true, 
                        ]); ?>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model, 'rut', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Rut']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'nombre', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Nombre']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'apellido', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Apellido']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'email', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'email']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'direccion', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'dirección']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'telefono', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Teléfono']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'giro', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Giro']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'comuna', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Comuna']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model, 'ciudad', ['template' => '{label}{input}{error}{hint}','errorOptions'=>['class'=>'badge badge-danger']
                                        ])->textInput(['class'=>'form-control', 'placeholder' => 'Ciudad']) ?>
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-actions">
                                <div class="card-body">
                                    <button type="button" class="btn btn-success waves-effect waves-light" onclick="guardar()"><i class="fa fa-check"></i> Guardar </button>
                                    
                                </div>
                            </div>
                            
                        </div>

                <?php $form->end(); ?>
            
        </div>
    </div>
</div>


<script src="<?= Yii::getAlias('@web'); ?>/content/jsrut/jquery.rut.js"></script>

<script>
$(document).ready(function(){

    $("#clientes-rut").rut({formatOn: 'keyup'});
});


function checkRut(rut) {

    // Despejar Puntos
    var valor = rut.replaceAll('.', '');
    // Despejar Guión
    valor = valor.replaceAll('-', '');

    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();

    // Formatear RUN
    rut = cuerpo + '-' + dv

    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if (cuerpo.length < 7) {
        // rut.setCustomValidity("RUT Incompleto"); 
        return false;
    }

    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;

    // Para cada dígito del Cuerpo
    for (i = 1; i <= cuerpo.length; i++) {

        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);

        // Sumar al Contador General
        suma = suma + index;

        // Consolidar Múltiplo dentro del rango [2,7]
        if (multiplo < 7) {
            multiplo = multiplo + 1;
        } else {
            multiplo = 2;
        }

    }

    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;

    // Validar que el Cuerpo coincide con su Dígito Verificador
    if (dvEsperado != dv) {
        // rut.setCustomValidity("RUT Inválido"); 
        return false;
    } else {
        return true
    }

    // Si todo sale bien, eliminar errores (decretar que es válido)
    // rut.setCustomValidity('');
}

function guardar() {
        rut = $("#clientes-rut").val()
        if (checkRut(rut)) {
            $("#crearCliente").submit()
        } else {
            alertify.notify('El rut es invalido', "warning");
        }
    }
</script>