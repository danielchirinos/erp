<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Editar Cliente'; 
    $this->params['activeLink'] = "clientes-crear";
?>


<?php echo $this->render('form', ["model" => $model]); ?>