<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
    $this->title = 'Lista de Clientes'; 
    $this->params['activeLink'] = "clientes-lista";
?>

<!-- input mask -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/inputmask/inputmask.css" rel="stylesheet" />
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/inputmask/jquery.inputmask.bundle.js"></script>

<!-- datatables -->
<link type="text/css" href="<?= Yii::getAlias('@web'); ?>/content/datatable/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/jszip.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/content/datatable/js/buttons.html5.min.js"></script>


<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0"><?= $this->title ?></h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Yii::getAlias('@web') ?>/productos/crear">Inicio</a></li>
                <li class="breadcrumb-item active"><?= $this->title ?></li>
            </ol>
        </div>

        <div class="col-md-6">
            <span class="d-flex justify-content-end">Cantidad: &nbsp; <span class="badge badge-success d-flex align-items-center"><?= count($clientes)?></span></span>
        </div>
    </div>

    <div class="row">
        <?php if (count($clientes) == 0) { ?>
            <span>Sin clientes</span>
        <?php }else { ?>
        
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Lista de Clientes</h4>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Rut</th>
                                        <th>Email</th>
                                        <th>Dirección</th>
                                        <th>Teléfono</th>
                                        <th>Activo</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($clientes as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value->nombre ?></td>
                                        <td><?= $value->apellido ?></td>
                                        <td><?= $value->rut ?></td>
                                        <td><?= $value->email ?></td>
                                        <td><?= $value->direccion ?></td>
                                        <td><?= $value->telefono ?></td>
                                        <td>
                                            <span class="badge badge-<?= $value->activo == 1 ? "success" : "danger" ?>">
                                                <?= $value->activo == 1 ? "Activo" : "Inactivo" ?>
                                            </span>
                                        </td>   
                                        <td class="text-center">
                                            <a  href="<?= Yii::getAlias('@web') ?>/clientes/editar/<?= $value->id?>" title="Editar">
                                                <i class="fal fa-edit"></i>
                                            </a>

                                            <?php if(1==2){ ?>
                                                <a  href="<?= Yii::getAlias('@web') ?>/clientes/cambioclave/<?= $value->id?>" title="Cambio de clave" class="">
                                                <i class="fal fa-unlock"></i>
                                                </a>

                                                <a  href="<?= Yii::getAlias('@web') ?>/clientes/activar/<?= $value->id?>" title="Activar cliente" onclick="return confirm('¿Desea activar el cliente?')">
                                                    <i class="fal fa-user-check"></i>
                                                </a>

                                            <?php } ?>

                                            <a  href="<?= Yii::getAlias('@web') ?>/clientes/eliminar/<?= $value->id?>" title="Eliminar cliente" onclick="return confirm('¿Desea eliminar el cliente?')">
                                                <i class="fal fa-trash"></i>
                                            </a>
                                        </td>                                     
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>




<script>
$(document).ready(function(){
    $('#example23').DataTable({
        dom: 'Bfrtip',
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            { extend: 'excel', className: 'btn' }
        ]

    });
});
</script>